package se.searchofskills.service;

import org.junit.jupiter.api.Test;
import se.searchofskills.service.utils.Utils;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UtilsTest {

    /**
     * Check email is valid format or not
     * An email is valid is email has been format ****@**.**** . Email must have at least 2 characters after the dot
     * */
    @Test
    public void testIsValidEmail() {
        assertThat(Utils.isValidEmail("khachuu")).isFalse();
        assertThat(Utils.isValidEmail("khachuu.")).isFalse();
        assertThat(Utils.isValidEmail("khachuu@c")).isFalse();
        assertThat(Utils.isValidEmail("khachuupti@.com")).isFalse();
        assertThat(Utils.isValidEmail("khachuu..")).isFalse();
        assertThat(Utils.isValidEmail("khachuu@se")).isFalse();
        assertThat(Utils.isValidEmail("khachuu@@com.se")).isFalse();
        assertThat(Utils.isValidEmail("khachuu.com@se")).isFalse();
        assertThat(Utils.isValidEmail("khachuu@se.s")).isFalse();

        assertThat(Utils.isValidEmail("khachuu@se.seses")).isTrue();
        assertThat(Utils.isValidEmail("khachuu.com@se.com")).isTrue();
        assertThat(Utils.isValidEmail("khachuu@com.se")).isTrue();
        assertThat(Utils.isValidEmail("khachuu@se.sese")).isTrue();
        assertThat(Utils.isValidEmail("k3hachuu@.com.vn")).isFalse();
        assertThat(Utils.isValidEmail("Khachuu@.com.vng")).isFalse();
        assertThat(Utils.isValidEmail("khachuu@.edu.se")).isFalse();
    }
}
