package se.searchofskills.service.mock.phitraining;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import se.searchofskills.domain.EmailTemplate;
import se.searchofskills.domain.enumeration.EmailTemplateType;
import se.searchofskills.repository.EmailTemplateRepository;
import se.searchofskills.service.EmailTemplateService;
import se.searchofskills.service.UserACL;

import org.mockito.Mock;
import se.searchofskills.service.dto.EmailTemplateDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.ResourceNotFoundException;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
public class EmailTemplateServiceTest {
    @Mock
    UserACL userACL;
    @Mock
    EmailTemplateRepository emailTemplateRepository;
    @InjectMocks
    EmailTemplateService emailTemplateService;

    @Test
    public void testCreateOrUpdateEmailTemplateWithNonPermission() {
        given(userACL.isAdmin()).willReturn(false);
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class
            , () -> emailTemplateService.createOrUpdate(new EmailTemplateDTO()));

        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testDeleteEmailTemplateWithNonPermission() {
        given(userACL.isAdmin()).willReturn(false);
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class
            , () -> emailTemplateService.deleteEmailTemplate(1L));

        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testCreateEmailTemplateSuccess() {
        given(userACL.isAdmin()).willReturn(true);

        EmailTemplateDTO emailTemplateDTO = new EmailTemplateDTO();
        emailTemplateDTO.setContent("&lt;reference_number&gt;");
        emailTemplateDTO.setName("Email Template 1");
        emailTemplateDTO.setType(EmailTemplateType.MAIN);
        EmailTemplateDTO result = emailTemplateService.createOrUpdate(emailTemplateDTO);

        //then
        assertThat(result.getContent()).isEqualTo("&lt;reference_number&gt;");
        assertThat(result.getName()).isEqualTo("Email Template 1");
        assertThat(result.getType()).isEqualTo(EmailTemplateType.MAIN);
    }

    @Test
    public void testFindEmailTemplatesByName() {
        // given
        given(userACL.isAdmin()).willReturn(true);
        EmailTemplate emailTemplate = new EmailTemplate();
        emailTemplate.setName("Name");
        emailTemplate.setContent("Content");
        emailTemplate.setType(EmailTemplateType.MAIN);

        given(emailTemplateRepository.findAllByDeletedAndNameContainingIgnoreCase(PageRequest.of(1, 20), false, "a"))
            .willReturn(new PageImpl<>(Collections.singletonList(emailTemplate)));

        // when
        Page<EmailTemplateDTO> page = emailTemplateService.findEmailTemplatesByName(PageRequest.of(1, 20), "a", null);

        // then
        assertThat(page.getContent().size()).isEqualTo(1);
        assertThat(page.getContent().get(0).getName().contains("a")).isTrue();
        assertThat(page.getContent().get(0).getContent().equals("Content")).isTrue();
    }

    @Test
    public void testUpdateEmailTemplateSuccess() {
        given(userACL.isAdmin()).willReturn(true);

        //exist email template
        EmailTemplate emailTemplate = new EmailTemplate();
        emailTemplate.setId(1L);
        emailTemplate.setContent("&lt,request_date&gt;");
        emailTemplate.setName("template1");
        emailTemplate.setType(EmailTemplateType.MAIN);

        // data transfer
        EmailTemplateDTO emailTemplateDTO = new EmailTemplateDTO();
        emailTemplateDTO.setId(1L);
        emailTemplateDTO.setContent("&lt;reference_number&gt;");
        emailTemplateDTO.setName("template 11");
        emailTemplateDTO.setType(EmailTemplateType.MAIN);

        given(emailTemplateRepository.findById(eq(1L)))
            .willReturn(Optional.of(emailTemplate));
        EmailTemplateDTO result = emailTemplateService.createOrUpdate(emailTemplateDTO);

        assertThat(result.getContent()).isEqualTo("&lt;reference_number&gt;");
        assertThat(result.getName()).isEqualTo("template 11");
        assertThat(result.getType()).isEqualTo(EmailTemplateType.MAIN);
    }

    @Test
    public void testDeleteEmailTemplateSuccess() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        EmailTemplate emailTemplate = new EmailTemplate();
        emailTemplate.setId(1L);
        emailTemplate.setContent("&lt;reference_number&gt;");
        emailTemplate.setType(EmailTemplateType.REMINDER_1);
        emailTemplate.setName("emailtemplate1");
        emailTemplate.setDeleted(false);

        given(emailTemplateRepository.findByIdAndDeletedIsFalse(eq(1L)))
            .willReturn(Optional.of(emailTemplate));
        // when
        emailTemplateService.deleteEmailTemplate(1L);
        // then
        assertThat(emailTemplate.isDeleted()).isEqualTo(true);
    }

    @Test
    public void testDeleteEmailTemplateCaseNonExistData() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class
            ,() -> emailTemplateService.deleteEmailTemplate(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    // end of CRUD feature
}