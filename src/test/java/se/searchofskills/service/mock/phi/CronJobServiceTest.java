package se.searchofskills.service.mock.phi;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import se.searchofskills.domain.*;
import se.searchofskills.domain.enumeration.*;
import se.searchofskills.repository.*;
import se.searchofskills.service.*;
import se.searchofskills.service.dto.CronJobScheduler;
import se.searchofskills.service.dto.CronjobDTO;
import se.searchofskills.service.dto.CronjobDataDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.ResourceNotFoundException;
import se.searchofskills.service.utils.Utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class CronJobServiceTest {

    @InjectMocks
    CronJobService cronJobService;

    @Mock
    RecipientListRepository recipientListRepository;

    @Mock
    RecipientRepository recipientRepository;

    @Mock
    CronJobRepository cronJobRepository;

    @Mock
    EmailTemplateRepository emailTemplateRepository;

    @Mock
    UserACL userACL;

    @Mock
    CronJobStatisticRepository cronJobStatisticRepository;

    @Mock
    ApplicationEventPublisher eventPublisher;

    @Mock
    EmailTrackingRepository emailTrackingRepository;

    @Mock
    CronJobEmailTemplateRepository cronJobEmailTemplateRepository;

    @Mock
    TimeService timeService;

    private EmailTemplate mainTemplate;
    private List<Recipient> recipients = new ArrayList<>();

    public CronJob initCronJob() {
        // init recipient list
        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        recipientList.setName("Recipient list 1");

        // init 10 recipients
        for (int i = 0; i < 10; i++) {
            Recipient recipient = new Recipient();
            recipient.setId((long) i);
            recipient.setRecipientList(recipientList);
            recipients.add(recipient);
        }

        // init cronjob
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setRecipientList(recipientList);
        cronJob.setDeleted(false);
        cronJob.setArchived(false);
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setChunkNumber(2);

        // init 3 Email template: MAIN, REMINDER_1, REMINDER_2
        mainTemplate = new EmailTemplate();
        mainTemplate.setId(1L);
        mainTemplate.setName("Main");
        mainTemplate.setType(EmailTemplateType.MAIN);

        EmailTemplate reminderTemplate1 = new EmailTemplate();
        reminderTemplate1.setId(2L);
        reminderTemplate1.setName("Reminder template 1");
        reminderTemplate1.setType(EmailTemplateType.REMINDER_1);

        EmailTemplate reminderTemplate2 = new EmailTemplate();
        reminderTemplate2.setId(3L);
        reminderTemplate2.setName("Reminder tempate 2");
        reminderTemplate2.setType(EmailTemplateType.REMINDER_2);

        // init entity of relationship for cronjob and email template
        CronJobEmailTemplate cronJobMainTemplate = new CronJobEmailTemplate();
        cronJobMainTemplate.setCronJob(cronJob);
        cronJobMainTemplate.setEmailTemplate(mainTemplate);
        cronJobMainTemplate.setType(EmailTemplateType.MAIN);

        CronJobEmailTemplate cronJobReminder1Template = new CronJobEmailTemplate();
        cronJobReminder1Template.setCronJob(cronJob);
        cronJobReminder1Template.setEmailTemplate(mainTemplate);
        cronJobReminder1Template.setType(EmailTemplateType.REMINDER_1);

        CronJobEmailTemplate cronJobReminder2Template = new CronJobEmailTemplate();
        cronJobReminder2Template.setCronJob(cronJob);
        cronJobReminder2Template.setEmailTemplate(mainTemplate);
        cronJobReminder2Template.setType(EmailTemplateType.REMINDER_2);

        // init cronjob statistic
        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setId(1L);
        cronJobStatistic.setCronJob(cronJob);

        return cronJob;
    }

    /**
     * Able to send next chunk case time of current date and next send date equal
     */
    @Test
    public void testCronJobSentNextChunkCaseTimeEqual() {
        // given
        // current date and next send date at Sat 9 Oct 2021 14:00
        Instant mockCurrentDate = LocalDateTime.of(2021, 10, 9, 14, 0, 0, 0).toInstant(ZoneOffset.UTC);
        Instant nextSendDate = LocalDateTime.of(2021, 10, 9, 14, 0, 0, 0).toInstant(ZoneOffset.UTC);

        // when
        boolean result = Utils.isDateBefore(mockCurrentDate, nextSendDate);

        // then
        // The time is equal -> it's time to send next chunk
        assertThat(result).isEqualTo(true);
        assertThat(nextSendDate.equals(mockCurrentDate)).isEqualTo(true);
    }

    /**
     * Able to send next chunk case time of current date and next send date: hour equal but different minute
     */
    @Test
    public void testCronJobSentNextChunkCaseSameHourDifferentMinute() {
        // given
        // current date  Sat 9 Oct 2021 14:00
        Instant mockCurrentDate = LocalDateTime.of(2021, 10, 9, 14, 0, 0, 0).toInstant(ZoneOffset.UTC);
        // next send date Sat 9 Oct 2021 14:35:33
        Instant nextSendDate = LocalDateTime.of(2021, 10, 9, 14, 35, 33, 0).toInstant(ZoneOffset.UTC);

        // when
        // both date was truncate to hour due to business requirement only compare from hour unit
        boolean result = Utils.isDateBefore(Utils.truncateInstant(mockCurrentDate), Utils.truncateInstant(nextSendDate));

        // then
        // The time is still even having different minute at 14PM -> it's time to send next chunk
        assertThat(result).isEqualTo(true);
        assertThat(nextSendDate.isAfter(mockCurrentDate)).isEqualTo(true);
    }

    /**
     * NOT be able to send next chunk case time of current date and next send date: next send date is before current date
     */
    @Test
    public void testCronJobSentNextChunkCaseNextSendDateAfterCurrentDate() {
        // given
        // current date  Sat 9 Oct 2021 14:00
        Instant mockCurrentDate = LocalDateTime.of(2021, 10, 9, 14, 0, 0, 0).toInstant(ZoneOffset.UTC);
        // next send date Sat 9 Oct 2021 15:00
        Instant nextSendDate = LocalDateTime.of(2021, 10, 9, 15, 0, 0, 0).toInstant(ZoneOffset.UTC);

        // when
        // both date was truncate to hour due to business requirement only compare from hour unit
        boolean result = Utils.isDateBefore(Utils.truncateInstant(mockCurrentDate), Utils.truncateInstant(nextSendDate));

        // then
        // The time is not valid due to current date 14h - next send date truncated: 15h -> it's not time to send next chunk
        assertThat(result).isEqualTo(false);
        // compare next send date and mock current date
        assertThat(nextSendDate.isAfter(mockCurrentDate)).isEqualTo(true);
    }

    /**
     * Be able to send next chunk case time of current date and next send date: next send date is after a lot compare to current date
     */
    @Test
    public void testCronJobSentNextChunkCaseServerDieFor2Hours() {
        // given
        // current date  Sat 9 Oct 2021 16:00
        Instant mockCurrentDate = LocalDateTime.of(2021, 10, 9, 16, 0, 0, 0).toInstant(ZoneOffset.UTC);
        // next send date Sat 9 Oct 2021 14:00
        Instant nextSendDate = LocalDateTime.of(2021, 10, 9, 14, 0, 0, 0).toInstant(ZoneOffset.UTC);

        // when
        // both date was truncate to hour due to business requirement only compare from hour unit
        boolean result = Utils.isDateBefore(Utils.truncateInstant(mockCurrentDate), Utils.truncateInstant(nextSendDate));

        // then
        assertThat(result).isEqualTo(true);
        // compare next send date and mock current date
        assertThat(nextSendDate.isBefore(mockCurrentDate)).isEqualTo(true);
    }

    /**
     * Calculate next send date after once week
     */
    @Test
    public void testCalculateNextSendDateOfCronJobOnceWeekLater() {
        // given
        // old next send date Sat 9 Oct 2021 13:59:59
        Instant oldSentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 9, 13, 59, 59, 0).toInstant(ZoneOffset.UTC));

        // intend next send date is 1 week later
        Instant nextSendPretend = Utils.truncateInstant(LocalDateTime.of(2021, 10, 16, 13, 59, 59, 0).toInstant(ZoneOffset.UTC));

        // cronjob created
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.MON);
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setSendHour(14);
        cronJob.setNextSendDate(oldSentDate);

        // when
        // old send date is also current date
        Instant result = Utils.calculateNextSendDate(oldSentDate, cronJob);

        // then
        // intend next send date will be one week later at that hour
        assertThat(result.compareTo(nextSendPretend)).isEqualTo(0);
    }

    /**
     * Calculate next send date after two week
     */
    @Test
    public void testCalculateNextSendDateOfCronJobTwoWeekLater() {
        // given
        // old next send date Sat 9 Oct 2021 13:59:59
        Instant oldSentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 9, 13, 59, 59, 0).toInstant(ZoneOffset.UTC));

        // intend next send date is 2 week later
        Instant nextSendPretend = Utils.truncateInstant(LocalDateTime.of(2021, 10, 23, 13, 59, 59, 0).toInstant(ZoneOffset.UTC));

        // cronjob created
        CronJobScheduler cronJobScheduler = new CronJobScheduler();
        cronJobScheduler.setWeekday(WeekDay.MON);
        cronJobScheduler.setFrequency(Frequency.ONCE_PER_TWO_WEEK);
        cronJobScheduler.setSendHour(14);
        cronJobScheduler.setNextSendDate(oldSentDate);

        // when
        Instant result = Utils.calculateNextSendDate(oldSentDate, cronJobScheduler);

        // then
        // intend next send date will be two week later at that hour
        assertThat(result.equals(nextSendPretend)).isEqualTo(true);
    }

    /**
     * Calculate next send date frequency two week after 1 month server die
     */
    @Test
    public void testCalculateNextSendDateOfCronJobTwoWeekLaterCaseServerDieForAMonth() {
        // given
        // old next send date Sat 9 Oct 2021 13:00
        Instant oldSentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 9, 13, 59, 59, 0).toInstant(ZoneOffset.UTC));

        // intend next send date at 20/11/2021 13:00
        Instant nextSendPretend = Utils.truncateInstant(LocalDateTime.of(2021, 11, 20, 13, 59, 59, 0).toInstant(ZoneOffset.UTC));

        // the day server run back again 09/11/2021 13:00
        Instant dayServerRestart = Utils.truncateInstant(LocalDateTime.of(2021, 11, 9, 13, 59, 59, 0).toInstant(ZoneOffset.UTC));

        // cronjob created
        CronJobScheduler cronJobScheduler = new CronJobScheduler();
        cronJobScheduler.setWeekday(WeekDay.MON);
        cronJobScheduler.setFrequency(Frequency.ONCE_PER_TWO_WEEK);
        cronJobScheduler.setSendHour(14);
        cronJobScheduler.setNextSendDate(oldSentDate);


        // when
        Instant result = Utils.calculateNextSendDate(Utils.truncateInstant(dayServerRestart), cronJobScheduler);

        // then
        // intend next send date will at 20/11/2021 13:00
        assertThat(result.equals(nextSendPretend)).isEqualTo(true);
    }

    /**
     * Calculate next send date frequency once month later
     */
    @Test
    public void testCalculateNextSendDateOfCronJobOnceMonthAt2ndWeekLater() {
        // given
        // old next send date at Sat 2nd week of Oct at 14:00
        Instant oldSentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 9, 14, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // intend next send date Sat 2nd week of Nov at 14:00
        Instant nextSendPretend = Utils.truncateInstant(LocalDateTime.of(2021, 11, 13, 14, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // cronjob created
        CronJobScheduler cronJobScheduler = new CronJobScheduler();
        cronJobScheduler.setWeekday(WeekDay.SAT);
        cronJobScheduler.setFrequency(Frequency.ONCE_PER_MONTH);
        cronJobScheduler.setWeekOfMonth(2);
        cronJobScheduler.setSendHour(14);
        cronJobScheduler.setNextSendDate(oldSentDate);


        // when
        Instant result = Utils.calculateNextSendDate(oldSentDate, cronJobScheduler);

        // then
        // intend next send date will at 13/11/2021 14:00
        assertThat(result.equals(nextSendPretend)).isEqualTo(true);
    }

    /**
     * calculate first send date on same date and send hour is on future
     */
    @Test
    public void testCalculateFirstSendDateCaseFirstChunkOnSameDateSendHourInFuture() {
        // given
        // create cronjob settings
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.SAT);
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setWeekOfMonth(2);
        cronJob.setSendHour(15);
        cronJob.setNextSendDate(null);

        // mock a current date Sat 09/10/2021 14:00
        Instant currentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 9, 14, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // when
        Instant result = Utils.calculateFirstSendDate(currentDate, cronJob);

        // then
        // next send date will be the same day at 15:00
        assertThat(result.compareTo(Utils.truncateInstant(currentDate.plus(1, ChronoUnit.HOURS)))).isEqualTo(0);
    }

    /**
     * calculate first send date on same date and send hour passed
     */
    @Test
    public void testCalculateFirstSendDateCaseFirstChunkOnSameDateButSendHourPassed() {
        // given
        // create cronjob
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.SAT);
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setWeekOfMonth(2);
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);

        // mock a current date Sat 09/10/2021 14:00
        Instant currentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 9, 14, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // when
        Instant result = Utils.calculateFirstSendDate(currentDate, cronJob);

        // then
        // next send date will be at the next SAT at 13:00
        assertThat(result.compareTo(Utils.truncateInstant(currentDate.plus(7, ChronoUnit.DAYS)).minus(1, ChronoUnit.HOURS))).isEqualTo(0);
    }

    /**
     * calculate first send date on different weekdays
     */
    @Test
    public void testCalculateFirstSendDateCaseFirstChunkOnDifferentDate() {
        // given
        // create cronjob
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.THU); // on thursday
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setWeekOfMonth(2);
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);

        // mock a current date Sat 09/10/2021 14:00
        Instant currentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 9, 14, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // when
        Instant result = Utils.calculateFirstSendDate(currentDate, cronJob);

        // then
        // next send date will be at the next THU at 13:00
        assertThat(result.compareTo(Utils.truncateInstant(currentDate.plus(5, ChronoUnit.DAYS)).minus(1, ChronoUnit.HOURS))).isEqualTo(0);
    }

    /**
     * calculate first send date on same week of month
     */
    @Test
    public void testCalculateFirstSendDateCaseOncePerMonthOnSameWeekOfMonth() {
        // given
        // create cronjob
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.THU); // on thursday
        cronJob.setFrequency(Frequency.ONCE_PER_MONTH);
        cronJob.setWeekOfMonth(2);
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);

        // mock a current date Sat 09/10/2021 14:00 (second week of month)
        Instant currentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 9, 14, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next intend date is on Thursday 11/11/2021 13:00
        Instant nextSendDateIntend = Utils.truncateInstant(LocalDateTime.of(2021, 11, 11, 13, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // when
        Instant result = Utils.calculateFirstSendDate(currentDate, cronJob);

        // then
        // next send date will be at the next month on Thursday 11/11/2021 at 13:00
        assertThat(result.compareTo(nextSendDateIntend)).isEqualTo(0);
    }

    /**
     * calculate first send date on smaller week of month
     */
    @Test
    public void testCalculateFirstSendDateCaseOncePerMonthOnSmallerWeekOfMonth() {
        // given
        // create cronjob
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.THU); // on thursday
        cronJob.setFrequency(Frequency.ONCE_PER_MONTH);
        cronJob.setWeekOfMonth(3); // intend on 3rd week
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);

        // mock a current date Sat 09/10/2021 14:00 (second week of month)
        Instant currentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 9, 14, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next intend date is on Thursday 21/10/2021 13:00
        Instant nextSendDateIntend = Utils.truncateInstant(LocalDateTime.of(2021, 10, 21, 13, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // when
        Instant result = Utils.calculateFirstSendDate(currentDate, cronJob);

        // then
        // next send date will be at the next month on Thursday 14/10/2021 at 13:00
        assertThat(result.compareTo(nextSendDateIntend)).isEqualTo(0);
    }

    /**
     * calculate first send date on 23/10/2021 from current date 02/10/2021
     */
    @Test
    public void testCalculateFirstSendDateCaseOncePerMonthOnSmallerWeekOfMonthAndSameMonth() {
        // given
        // create cronjob
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.SAT); // on thursday
        cronJob.setFrequency(Frequency.ONCE_PER_MONTH);
        cronJob.setWeekOfMonth(4); // intend on 4th week
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);

        // mock a current date Sat 02/10/2021 12:00 (second week of month)
        Instant currentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 2, 12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next intend date is on SAT 23/10/2021 13:00
        Instant nextSendDateIntend = Utils.truncateInstant(LocalDateTime.of(2021, 10, 23, 13, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // when
        Instant result = Utils.calculateFirstSendDate(currentDate, cronJob);

        // then
        // next send date will be at the next month on SAT 23/10/2021 at 13:00
        assertThat(result.compareTo(nextSendDateIntend)).isEqualTo(0);
    }

    /**
     * calculate first send date on from current date 31/10/2021 with settings 1st week of month
     */
    @Test
    public void testCalculateFirstSendDateCaseOncePerMonthOnTheFirstWeekOfMonth() {
        // given
        // create cronjob
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.MON); // on mon
        cronJob.setFrequency(Frequency.ONCE_PER_MONTH);
        cronJob.setWeekOfMonth(1); // intend on 1st week
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);

        // mock a current date Sat 31/10/2021 12:00 (fourth week of month)
        Instant currentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 31, 12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next intend date is on MON 1/11/2021 13:00
        Instant nextSendDateIntend = Utils.truncateInstant(LocalDateTime.of(2021, 11, 1, 13, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // when
        Instant result = Utils.calculateFirstSendDate(currentDate, cronJob);

        // then
        // next send date will be at the next month on SAT 1/11/2021 at 13:00
        assertThat(result.compareTo(nextSendDateIntend)).isEqualTo(0);
    }

    /**
     * calculate first send date on from current date 10/10/2021 with settings 2nd week of month but current hour is bigger
     */
    @Test
    public void testCalculateFirstSendDateCaseOncePerMonthOnTheSameDateButHourIsBigger() {
        // given
        // create cronjob
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.SUN); // on mon
        cronJob.setFrequency(Frequency.ONCE_PER_MONTH);
        cronJob.setWeekOfMonth(2); // intend on 1st week
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);

        // mock a current date Sun 10/10/2021 14:00 (2nd week of month)
        Instant currentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 10, 14, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next intend date is on Sun 14/11/2021 13:00
        Instant nextSendDateIntend = Utils.truncateInstant(LocalDateTime.of(2021, 11, 14, 13, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // when
        Instant result = Utils.calculateFirstSendDate(currentDate, cronJob);

        // then
        // next send date will be at the next month on SAT 14/11/2021 at 13:00
        assertThat(result.compareTo(nextSendDateIntend)).isEqualTo(0);
    }

    /**
     * calculate first send date on from current date 10/10/2021 with settings 2nd week of month but current hour is bigger
     */
    @Test
    public void testCalculateFirstSendDateCaseOncePerMonthOnTheSameDateButHourIsSmaller() {
        // given
        // create cronjob
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.SUN); // on mon
        cronJob.setFrequency(Frequency.ONCE_PER_MONTH);
        cronJob.setWeekOfMonth(2); // intend on 1st week
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);

        // mock a current date Sun 10/10/2021 12:00 (2nd week of month)
        Instant currentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 10, 12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next intend date is on Sun 10/10/2021 13:00
        Instant nextSendDateIntend = Utils.truncateInstant(LocalDateTime.of(2021, 10, 10, 13, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // when
        Instant result = Utils.calculateFirstSendDate(currentDate, cronJob);

        // then
        // next send date will be at the next month on SAT 10/10/2021 at 13:00
        assertThat(result.compareTo(nextSendDateIntend)).isEqualTo(0);
    }

    /**
     * calculate first send date on from current date 10/10/2021 with settings 3rd week of month
     */
    @Test
    public void testCalculateFirstSendDateCaseOncePerMonthOn3rdWeekOfSameMonth() {
        // given
        // create cronjob
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.FRI); // on mon
        cronJob.setFrequency(Frequency.ONCE_PER_MONTH);
        cronJob.setWeekOfMonth(3); // intend on 1st week
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);

        // mock a current date Sun 10/10/2021 12:00 (2nd week of month)
        Instant currentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 10, 12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next intend date is on Fri 15/10/2021 13:00
        Instant nextSendDateIntend = Utils.truncateInstant(LocalDateTime.of(2021, 10, 15, 13, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // when
        Instant result = Utils.calculateFirstSendDate(currentDate, cronJob);

        // then
        // next send date will be at the next month on SAT 15/10/2021 at 13:00
        assertThat(result.compareTo(nextSendDateIntend)).isEqualTo(0);
    }

    /**
     * calculate first send date on from current date 10/10/2021 with settings 1st week of month
     */
    @Test
    public void testCalculateFirstSendDateCaseOncePerMonthOn1stdWeekOfNextMonth() {
        // given
        // create cronjob
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.WED);
        cronJob.setFrequency(Frequency.ONCE_PER_MONTH);
        cronJob.setWeekOfMonth(1); // intend on 1st week
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);

        // mock a current date Sun 10/10/2021 12:00 (2nd week of month)
        Instant currentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 10, 12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next intend date is on Fri 3/11/2021 13:00
        Instant nextSendDateIntend = Utils.truncateInstant(LocalDateTime.of(2021, 11, 3, 13, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // when
        Instant result = Utils.calculateFirstSendDate(currentDate, cronJob);

        // then
        // next send date will be at the next month on SAT 3/11/2021 at 13:00
        assertThat(result.compareTo(nextSendDateIntend)).isEqualTo(0);
    }

    /**
     * calculate first send date on from current date 10/10/2021 with settings 1st week of month
     */
    @Test
    public void testCalculateFirstSendDateCaseOncePerMonthOn4thdWeekOfSameMonth() {
        // given
        // create cronjob
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.SUN);
        cronJob.setFrequency(Frequency.ONCE_PER_MONTH);
        cronJob.setWeekOfMonth(4); // intend on 1st week
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);

        // mock a current date Sun 10/10/2021 12:00 (2nd week of month)
        Instant currentDate = Utils.truncateInstant(LocalDateTime.of(2021, 10, 10, 12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next intend date is on Sun 24/10/2021 13:00
        Instant nextSendDateIntend = Utils.truncateInstant(LocalDateTime.of(2021, 10, 24, 13, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // when
        Instant result = Utils.calculateFirstSendDate(currentDate, cronJob);

        // then
        // next send date will be at the next month on Sun 24/10/2021 at 13:00
        assertThat(result.compareTo(nextSendDateIntend)).isEqualTo(0);
    }

    /**
     * restore cronjob from non permission user
     */
    @Test
    public void testRestoreCronJobCaseUserDontHavePermission() {
        // given
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> cronJobService.restore(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * Restore non exist cronjob
     */
    @Test
    public void testRestoreCronJobFromNonExistData() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> cronJobService.restore(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    /**
     * restore cronjob failed due to cronjob finished
     */
    @Test
    public void testRestoreCronJobCaseCronJobFinished() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        CronJob cronJob = new CronJob();
        cronJob.setStatus(CronJobStatus.FINISHED);

        given(cronJobRepository.findById(eq(1L))).willReturn(Optional.of(cronJob));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> cronJobService.restore(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.cronjobStatusInvalid");
    }

    /**
     * restore cronjob failed due to cronjob not archived
     */
    @Test
    public void testRestoreCronJobCaseCronJobNotArchived() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        CronJob cronJob = new CronJob();
        cronJob.setStatus(CronJobStatus.CANCELLED);
        cronJob.setDeleted(false);
        cronJob.setArchived(false);

        given(cronJobRepository.findById(eq(1L))).willReturn(Optional.of(cronJob));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> cronJobService.restore(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.cronjobStatusInvalid");
    }

    /**
     * restore cronjob success
     */
    @Test
    public void testRestoreCronJobSuccess() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        CronJob cronJob = new CronJob();
        cronJob.setStatus(CronJobStatus.CANCELLED);
        cronJob.setDeleted(false);
        cronJob.setArchived(true);

        given(cronJobRepository.findById(eq(1L))).willReturn(Optional.of(cronJob));

        // when
        cronJobService.restore(1L);

        // then
        assertThat(cronJob.isArchived()).isEqualTo(false);
    }

    /**
     * Resume a cronjob from non admin permission user
     */
    @Test
    public void testResumeCronJobCaseNotAdmin() {
        // given
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> cronJobService.resume(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * resume non exist cronjob
     */
    @Test
    public void testResumeCronJobCaseCronJobNotFound() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> cronJobService.resume(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    /**
     * resume non exist cronjob statistic
     */
    @Test
    public void testResumeCronJobCaseCronJobStatisticNotFound() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        given(cronJobRepository.findById(eq(1L))).willReturn(Optional.of(cronJob));

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> cronJobService.resume(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    /**
     * resume cronjob with invalid status
     */
    @Test
    public void testResumeCronJobCaseCronJobStatusInvalid() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setStatus(CronJobStatus.FINISHED);
        cronJob.setArchived(true);
        cronJob.setDeleted(true);

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setCronJob(cronJob);
        cronJobStatistic.setId(1L);

        given(cronJobRepository.findById(eq(1L))).willReturn(Optional.of(cronJob));
        given(cronJobStatisticRepository.findByCronJobId(eq(1L))).willReturn(Optional.of(cronJobStatistic));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> cronJobService.resume(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.cronjobStatusInvalid");
    }

    /**
     * resume cronjob success once per week option
     */
    @Test
    public void testResumeCronJobSuccessOnePerWeek() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setStatus(CronJobStatus.CANCELLED);
        cronJob.setArchived(false);
        cronJob.setDeleted(false);
        cronJob.setWeekday(WeekDay.THU); // on thursday
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setWeekOfMonth(2);
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);
        cronJob.setChunkNumber(100);

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setCronJob(cronJob);
        cronJobStatistic.setId(1L);
        cronJobStatistic.setEmailSent(1000);
        cronJobStatistic.setTotalEmailToSend(2000);

        given(cronJobRepository.findById(eq(1L))).willReturn(Optional.of(cronJob));
        given(cronJobStatisticRepository.findByCronJobId(eq(1L))).willReturn(Optional.of(cronJobStatistic));

        // when
        cronJobService.resume(1L);

        // then
        // The day of next send date will be in the future
        assertThat(cronJob.getNextSendDate().compareTo(Instant.now())).isEqualTo(1);
    }

    /**
     * Calculate end date of cronjob with once per week
     */
    @Test
    public void testCalculateEndDateOfCronJobCaseOnePerWeek() {
        // given
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.THU); // on thursday
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setWeekOfMonth(2);
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);
        cronJob.setChunkNumber(100);
        cronJob.setTotalEmailToSend(1200); // total
        cronJob.setEmailSent(1000); // sent

        // now: thursday 14/10/2021 at 12:00
        Instant now = Utils.truncateInstant(LocalDateTime.of(2021, 10,14,12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next send date: thursday 21/10/2021 at 13:00
        Instant nextSendDate = Utils.calculateFirstSendDate(now, cronJob);
        cronJob.setNextSendDate(nextSendDate);

        // when
        Instant endDate = Utils.calculateEndDate(cronJob);

        // then
        // end date will be thursday 23-12-2021 13:00
        assertThat(endDate.toString()).isEqualTo("2021-10-21T13:00:00Z");
    }

    /**
     * Calculate end date of cronjob with once per week with odd chunk number
     */
    @Test
    public void testCalculateEndDateOfCronJobCaseOnePerWeekWithOddNumber() {
        // given
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.THU); // on thursday
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setWeekOfMonth(2);
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);
        cronJob.setChunkNumber(111);
        cronJob.setTotalEmailToSend(2000); // total
        cronJob.setEmailSent(1110); // sent - remain 890

        // now: thursday 14/10/2021 at 12:00
        Instant now = Utils.truncateInstant(LocalDateTime.of(2021, 10,14,12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next send date: thursday 21/10/2021 at 13:00
        Instant nextSendDate = Utils.calculateFirstSendDate(now, cronJob);
        cronJob.setNextSendDate(nextSendDate);

        // when
        Instant endDate = Utils.calculateEndDate(cronJob);

        // then
        // end date will be thursday 16-12-2021 13:00
        assertThat(endDate.toString()).isEqualTo("2021-12-09T13:00:00Z");
    }

    /**
     * Calculate end date of cronjob with once per 2 week
     */
    @Test
    public void testCalculateEndDateOfCronJobCaseOncePerTwoWeek() {
        // given
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.THU); // on thursday
        cronJob.setFrequency(Frequency.ONCE_PER_TWO_WEEK);
        cronJob.setWeekOfMonth(2);
        cronJob.setSendHour(11); // Run on 13h
        cronJob.setNextSendDate(null);
        cronJob.setChunkNumber(100);
        cronJob.setTotalEmailToSend(1200); // total
        cronJob.setEmailSent(1000); // sent

        // now: thursday 14/10/2021 at 12:00
        Instant now = Utils.truncateInstant(LocalDateTime.of(2021, 10,14,12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next send date: thursday 21/10/2021 at 13:00
        Instant nextSendDate = Utils.calculateFirstSendDate(now, cronJob);
        cronJob.setNextSendDate(nextSendDate);

        // when
        Instant endDate = Utils.calculateEndDate(cronJob);

        // then
        // end date will be thursday 3-3-2022 13:00
        assertThat(endDate.toString()).isEqualTo("2021-11-04T11:00:00Z");
    }

    /**
     * Calculate end date of cronjob with once per month
     */
    @Test
    public void testCalculateEndDateOfCronJobCaseOncePerMonth() {
        // given
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.THU); // on thursday
        cronJob.setFrequency(Frequency.ONCE_PER_MONTH);
        cronJob.setWeekOfMonth(2);
        cronJob.setSendHour(13); // Run on 13h
        cronJob.setNextSendDate(null);
        cronJob.setChunkNumber(100);
        cronJob.setTotalEmailToSend(1200); // total
        cronJob.setEmailSent(1000); // sent

        // now: thursday 7/10/2021 at 12:00
        Instant now = Utils.truncateInstant(LocalDateTime.of(2021, 10,7,12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next send date: thursday 21/10/2021 at 13:00
        Instant nextSendDate = Utils.calculateFirstSendDate(now, cronJob);
        cronJob.setNextSendDate(nextSendDate);

        // when
        Instant endDate = Utils.calculateEndDate(cronJob);

        // then
        // end date will be thursday 11-8-2022 13:00
        assertThat(endDate.toString()).isEqualTo("2021-11-11T13:00:00Z");
    }

    /**
     * delete cronjob from non permission user
     */
    @Test
    public void testDeleteCronJobCaseNonPermissionUser() {
        // given
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> cronJobService.delete(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * create cronjob from non permission user
     */
    @Test
    public void testCreateCronJobCaseNonPermissionUser() {
        // given
        given(userACL.isAdmin()).willReturn(false);
        CronjobDTO cronjobDTO = new CronjobDTO();
        cronjobDTO.setRecipientListId(1L);
        cronjobDTO.setTemplateMainId(1L);
        cronjobDTO.setTemplateReminder1Id(2L);
        cronjobDTO.setTemplateReminder2Id(3L);
        cronjobDTO.setEndDate(Instant.now());
        cronjobDTO.setFrequency(Frequency.ONCE_PER_WEEK);
        cronjobDTO.setWeekday(WeekDay.FRI);
        cronjobDTO.setSendHour(0);
        cronjobDTO.setChunkNumber(100);
        cronjobDTO.setTemplateReminderTime1(7);
        cronjobDTO.setTemplateReminderTime2(14);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> cronJobService.saveCronjob(cronjobDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * delete cronjob success
     */
    @Test
    public void testDeleteCronJobSuccess() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setDeleted(false);
        cronJob.setArchived(true);
        cronJob.setStatus(CronJobStatus.CANCELLED);

        given(cronJobRepository.findById(eq(1L))).willReturn(Optional.of(cronJob));

        // when
        cronJobService.delete(1L);

        // then
        assertThat(cronJob.isDeleted()).isEqualTo(true);
    }

    /**
     * delete cronjob failed
     */
    @Test
    public void testDeleteCronJobInvalidStatus() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setDeleted(false);
        cronJob.setArchived(true);

        given(cronJobRepository.findById(eq(1L))).willReturn(Optional.of(cronJob));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> cronJobService.delete(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.cronjobStatusInvalid");
    }

    /**
     * Calculate end date of cronjob with once per 2 week
     */
    @Test
    public void testCalculateEndDateOfCronJobCaseOncePerTwoWeekSendToday() {
        // given
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.MON);
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setSendHour(3);
        cronJob.setNextSendDate(null);
        cronJob.setChunkNumber(100);
        cronJob.setTotalEmailToSend(301); // total

        // now: monday 18/10/2021 at 12:00
        Instant now = Utils.truncateInstant(LocalDateTime.of(2021, 10,18,2, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next send date: thursday 21/10/2021 at 13:00
        Instant nextSendDate = Utils.calculateFirstSendDate(now, cronJob);
        cronJob.setNextSendDate(nextSendDate);

        // when
        Instant endDate = Utils.calculateEndDate(cronJob);

        // then
        // end date will be thursday 3-3-2022 13:00
        assertThat(endDate.toString()).isEqualTo("2021-11-08T03:00:00Z");
    }

    /**
     * Calculate end date of cronjob with once per 2 week
     */
    @Test
    public void testCalculateEndDateOfCronJobCaseOncePerTwoWeekNotSendToday() {
        // given
        CronJobScheduler cronJob = new CronJobScheduler();
        cronJob.setWeekday(WeekDay.MON);
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setSendHour(1);
        cronJob.setNextSendDate(null);
        cronJob.setChunkNumber(100);
        cronJob.setTotalEmailToSend(301); // total

        // now: monday 18/10/2021 at 12:00
        Instant now = Utils.truncateInstant(LocalDateTime.of(2021, 10, 18, 2, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // next send date: thursday 21/10/2021 at 13:00
        Instant nextSendDate = Utils.calculateFirstSendDate(now, cronJob);
        cronJob.setNextSendDate(nextSendDate);

        // when
        Instant endDate = Utils.calculateEndDate(cronJob);

        // then
        assertThat(endDate.toString()).isEqualTo("2021-11-15T01:00:00Z");
    }

    /*
     * auto sending email case cronjob still have chunk to send
     */
    @Test
    public void testAutoSendingEmailCaseCronJobStillHave() {
        // given
        // 18/10/2021 at 12:00
        Instant nextSendDate = Utils.truncateInstant(LocalDateTime.of(2021, 10,18,12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        CronJob cronJob = initCronJob();
        cronJob.setChunkNumber(2);
        cronJob.setWeekday(WeekDay.MON);
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setSendHour(12);
        cronJob.setNextSendDate(nextSendDate);

        Instant now = Utils.truncateInstant(LocalDateTime.of(2021, 10,18,12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setEmailSent(0);
        cronJobStatistic.setTotalEmailToSend(10);

        // get cronjob statistic
        given(cronJobStatisticRepository.findByCronJobId(eq(1L))).willReturn(Optional.of(cronJobStatistic));
        // get recipients to send
        given(recipientRepository.findAllByRecipientListId(any(Pageable.class), eq(1L))).willReturn(new PageImpl<>(recipients.subList(0,2)));
        // get email template main of cronjob
        given(emailTemplateRepository.findMainTemplateOfCronJob(eq(EmailTemplateType.MAIN), eq(1L))).willReturn(Collections.singletonList(mainTemplate));

        // when
        cronJobService.sendEmailWithCronJob(cronJob, now);

        // then
        // call to event to send email
        then(eventPublisher).should(times(2)).publishEvent(any(AutoSendEmailEvent.class));

        // update cronjob statistic with 2 more email sent
        assertThat(cronJobStatistic.getEmailSent()).isEqualTo(2);
        // update cronjob next send date
        assertThat(cronJob.getNextSendDate().toString()).isEqualTo("2021-10-25T12:00:00Z");
    }

    /*
     * auto sending email case cronjob case send hour of cronjob greater than current hour
     */
    @Test
    public void testAutoSendingEmailCaseCronJobSendHourGreaterThanCurrentHour() {
        // given
        // 18/10/2021 at 12:00
        Instant nextSendDate = Utils.truncateInstant(LocalDateTime.of(2021, 10,18,13, 0, 0, 0).toInstant(ZoneOffset.UTC));

        CronJob cronJob = initCronJob();
        cronJob.setChunkNumber(2);
        cronJob.setWeekday(WeekDay.MON);
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setSendHour(13);
        cronJob.setNextSendDate(nextSendDate);

        Instant now = Utils.truncateInstant(LocalDateTime.of(2021, 10,18,12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        // when
        cronJobService.sendEmailWithCronJob(cronJob, now);

        // then
        // not call to event to send email
        then(eventPublisher).should(times(0)).publishEvent(any(AutoSendEmailEvent.class));

    }

    /**
     * auto sending email case cronjob sent last chunk
     */
    @Test
    public void testAutoSendingEmailCaseSentLastChunk() {
        // given
        // 18/10/2021 at 12:00
        Instant nextSendDate = Utils.truncateInstant(LocalDateTime.of(2021, 10,18,12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        CronJob cronJob = initCronJob();
        cronJob.setChunkNumber(2);
        cronJob.setWeekday(WeekDay.MON);
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setSendHour(12);
        cronJob.setNextSendDate(nextSendDate);

        Instant now = Utils.truncateInstant(LocalDateTime.of(2021, 10,18,12, 0, 0, 0).toInstant(ZoneOffset.UTC));

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setEmailSent(8);
        cronJobStatistic.setTotalEmailToSend(10);

        // get cronjob statistic
        given(cronJobStatisticRepository.findByCronJobId(eq(1L))).willReturn(Optional.of(cronJobStatistic));
        // get recipients to send
        given(recipientRepository.findAllByRecipientListId(any(Pageable.class), eq(1L))).willReturn(new PageImpl<>(recipients.subList(0,2)));
        // get email template main of cronjob
        given(emailTemplateRepository.findMainTemplateOfCronJob(eq(EmailTemplateType.MAIN), eq(1L))).willReturn(Collections.singletonList(mainTemplate));

        // when
        cronJobService.sendEmailWithCronJob(cronJob, now);

        // then
        // call to event to send email
        then(eventPublisher).should(times(2)).publishEvent(any(AutoSendEmailEvent.class));
        // update cronjob statistic with 2 more email sent
        assertThat(cronJobStatistic.getEmailSent()).isEqualTo(10);
    }

    /**
     * create Cronjob successful
     */
    @Test
    public void testCreateCronjobSuccessful() {
        // given
        Instant endDate
            = Instant.parse("2021-11-12T00:00:00Z");
        Instant startDate
            = Instant.parse("2021-10-22T00:00:00Z");
        CronjobDTO cronjobDTO = new CronjobDTO();
        cronjobDTO.setName("Cronjob name");
        cronjobDTO.setRecipientListId(1L);
        cronjobDTO.setTemplateMainId(1L);
        cronjobDTO.setTemplateReminder1Id(2L);
        cronjobDTO.setTemplateReminder2Id(3L);
        cronjobDTO.setStartDate(startDate);
        cronjobDTO.setEndDate(endDate);
        cronjobDTO.setFrequency(Frequency.ONCE_PER_WEEK);
        cronjobDTO.setWeekday(WeekDay.FRI);
        cronjobDTO.setSendHour(0);
        cronjobDTO.setChunkNumber(100);
        cronjobDTO.setTemplateReminderTime1(7);
        cronjobDTO.setTemplateReminderTime2(14);

        EmailTemplate emailTemplate = new EmailTemplate();
        emailTemplate.setType(EmailTemplateType.MAIN);
        emailTemplate.setId(1L);
        emailTemplate.setContent("Content");
        emailTemplate.setName("template main");

        EmailTemplate emailTemplateReminder1 = new EmailTemplate();
        emailTemplateReminder1.setType(EmailTemplateType.REMINDER_1);
        emailTemplate.setId(2L);
        emailTemplateReminder1.setContent("Content reminder 1");
        emailTemplateReminder1.setName("template reminder");

        EmailTemplate emailTemplateReminder2 = new EmailTemplate();
        emailTemplateReminder2.setType(EmailTemplateType.REMINDER_2);
        emailTemplate.setId(3L);
        emailTemplateReminder2.setContent("Content reminder 1");
        emailTemplateReminder2.setName("template reminder");

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("Recipient List");

        given(recipientRepository.countAllByRecipientListId(1L)).willReturn(301);
        given(recipientListRepository.findById(1L)).willReturn(Optional.of(recipientList));
        given(emailTemplateRepository.findById(1L)).willReturn(Optional.of(emailTemplate));
        given(emailTemplateRepository.findByIdAndType(2L, EmailTemplateType.REMINDER_1)).willReturn(Optional.of(emailTemplateReminder1));
        given(emailTemplateRepository.findByIdAndType(3L, EmailTemplateType.REMINDER_2)).willReturn(Optional.of(emailTemplateReminder2));
        given(cronJobRepository.save(any(CronJob.class))).willReturn(new CronJob(cronjobDTO));
        given(userACL.isAdmin()).willReturn(true);

        // when
        CronjobDTO newCronjobDTO = cronJobService.saveCronjob(cronjobDTO);

        // then
        assertThat(newCronjobDTO.getEndDate().compareTo(endDate) == 0).isTrue();
        assertThat(newCronjobDTO.getStartDate()).isEqualTo(startDate);
        assertThat(cronjobDTO.getRecipientListId()).isEqualTo(1L);
        assertThat(cronjobDTO.getTemplateMainId()).isEqualTo(1L);
        assertThat(cronjobDTO.getTemplateReminder1Id()).isEqualTo(2L);
        assertThat(cronjobDTO.getTemplateReminder2Id()).isEqualTo(3L);
        assertThat(cronjobDTO.getTemplateReminderTime1()).isEqualTo(7L);
        assertThat(cronjobDTO.getTemplateReminderTime2()).isEqualTo(14L);
        assertThat(cronjobDTO.getFrequency()).isEqualTo(Frequency.ONCE_PER_WEEK);
        assertThat(cronjobDTO.getSendHour()).isEqualTo(0);
        assertThat(cronjobDTO.getWeekday()).isEqualTo(WeekDay.FRI);
        assertThat(cronjobDTO.getChunkNumber()).isEqualTo(100);
        assertThat(cronjobDTO.getRecipientListId()).isEqualTo(1L);
        then(cronJobEmailTemplateRepository).should(times(1)).saveAll(anyList());
        then(cronJobStatisticRepository).should(times(1)).save(any(CronJobStatistic.class));
    }

    /**
     * create Cronjob failed by invalid send date
     */
    @Test
    public void testCreateCronjobInvalidSendDate() {
        // given
        Instant endDate
            = Instant.parse("2021-11-05T00:00:00Z");
        Instant startDate
            = Instant.parse("2021-10-22T00:00:00Z");
        CronjobDTO cronjobDTO = new CronjobDTO();
        cronjobDTO.setName("Cronjob name");
        cronjobDTO.setRecipientListId(1L);
        cronjobDTO.setTemplateMainId(1L);
        cronjobDTO.setTemplateReminder1Id(2L);
        cronjobDTO.setTemplateReminder2Id(3L);
        cronjobDTO.setStartDate(startDate);
        cronjobDTO.setEndDate(endDate);
        cronjobDTO.setFrequency(Frequency.ONCE_PER_WEEK);
        cronjobDTO.setWeekday(WeekDay.FRI);
        cronjobDTO.setSendHour(0);
        cronjobDTO.setChunkNumber(100);
        cronjobDTO.setTemplateReminderTime1(7);
        cronjobDTO.setTemplateReminderTime2(14);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("Recipient List");

        given(recipientRepository.countAllByRecipientListId(1L)).willReturn(301);
        given(recipientListRepository.findById(1L)).willReturn(Optional.of(recipientList));
        given(userACL.isAdmin()).willReturn(true);

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> cronJobService.saveCronjob(cronjobDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.endDateInvalid");

    }

    /**
     * create Cronjob failed by main template not found
     */
    @Test
    public void testCreateCronjobMainTemplateNotFound() {
        // given
        Instant endDate
            = Instant.parse("2021-11-12T00:00:00Z");
        Instant startDate
            = Instant.parse("2021-10-22T00:00:00Z");
        CronjobDTO cronjobDTO = new CronjobDTO();
        cronjobDTO.setName("Cronjob name");
        cronjobDTO.setRecipientListId(1L);
        cronjobDTO.setTemplateMainId(1L);
        cronjobDTO.setTemplateReminder1Id(2L);
        cronjobDTO.setTemplateReminder2Id(3L);
        cronjobDTO.setStartDate(startDate);
        cronjobDTO.setEndDate(endDate);
        cronjobDTO.setFrequency(Frequency.ONCE_PER_WEEK);
        cronjobDTO.setWeekday(WeekDay.FRI);
        cronjobDTO.setSendHour(0);
        cronjobDTO.setChunkNumber(100);
        cronjobDTO.setTemplateReminderTime1(7);
        cronjobDTO.setTemplateReminderTime2(14);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("Recipient List");

        given(recipientRepository.countAllByRecipientListId(1L)).willReturn(301);
        given(recipientListRepository.findById(1L)).willReturn(Optional.of(recipientList));
        given(emailTemplateRepository.findById(1L)).willReturn(Optional.empty());
        given(userACL.isAdmin()).willReturn(true);

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> cronJobService.saveCronjob(cronjobDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.templateMainNotFound");

    }

    /**
     * create Cronjob failed by reminder template not found
     */
    @Test
    public void testCreateCronjobReminderTemplateNotFound() {
        // given
        Instant endDate
            = Instant.parse("2021-11-12T00:00:00Z");
        Instant startDate
            = Instant.parse("2021-10-22T00:00:00Z");
        CronjobDTO cronjobDTO = new CronjobDTO();
        cronjobDTO.setName("Cronjob name");
        cronjobDTO.setRecipientListId(1L);
        cronjobDTO.setTemplateMainId(1L);
        cronjobDTO.setTemplateReminder1Id(2L);
        cronjobDTO.setTemplateReminder2Id(3L);
        cronjobDTO.setStartDate(startDate);
        cronjobDTO.setEndDate(endDate);
        cronjobDTO.setFrequency(Frequency.ONCE_PER_WEEK);
        cronjobDTO.setWeekday(WeekDay.FRI);
        cronjobDTO.setSendHour(0);
        cronjobDTO.setChunkNumber(100);
        cronjobDTO.setTemplateReminderTime1(7);
        cronjobDTO.setTemplateReminderTime2(14);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("Recipient List");

        EmailTemplate emailTemplate = new EmailTemplate();
        emailTemplate.setType(EmailTemplateType.MAIN);
        emailTemplate.setId(1L);
        emailTemplate.setContent("Content");
        emailTemplate.setName("template main");

        given(recipientRepository.countAllByRecipientListId(1L)).willReturn(301);
        given(recipientListRepository.findById(1L)).willReturn(Optional.of(recipientList));
        given(emailTemplateRepository.findById(1L)).willReturn(Optional.of(emailTemplate));
        given(emailTemplateRepository.findByIdAndType(2L, EmailTemplateType.REMINDER_1)).willReturn(Optional.empty());
        given(userACL.isAdmin()).willReturn(true);

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> cronJobService.saveCronjob(cronjobDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.templateReminder1NotFound");

    }


    /**
     * create Cronjob failed by recipient list not found
     */
    @Test
    public void testCreateCronjobRecipientListNotFound() {
        // given
        Instant endDate
            = Instant.parse("2021-11-12T00:00:00Z");
        Instant startDate
            = Instant.parse("2021-10-22T00:00:00Z");
        CronjobDTO cronjobDTO = new CronjobDTO();
        cronjobDTO.setName("Cronjob name");
        cronjobDTO.setRecipientListId(1L);
        cronjobDTO.setTemplateMainId(1L);
        cronjobDTO.setTemplateReminder1Id(2L);
        cronjobDTO.setTemplateReminder2Id(3L);
        cronjobDTO.setStartDate(startDate);
        cronjobDTO.setEndDate(endDate);
        cronjobDTO.setFrequency(Frequency.ONCE_PER_WEEK);
        cronjobDTO.setWeekday(WeekDay.FRI);
        cronjobDTO.setSendHour(0);
        cronjobDTO.setChunkNumber(100);
        cronjobDTO.setTemplateReminderTime1(7);
        cronjobDTO.setTemplateReminderTime2(14);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("Recipient List");

        given(recipientListRepository.findById(1L)).willReturn(Optional.empty());
        given(userACL.isAdmin()).willReturn(true);

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> cronJobService.saveCronjob(cronjobDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.recipientListNotFound");
    }

    /**
     * check endDate is overlap from non permission user
     */
    @Test
    public void testCheckEndDateIsOverlapNonPermissionUser() {
        // given
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> cronJobService.getLatestDateOverlapEndDate(Instant.now()));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * check endDate is overlap
     */
    @Test
    public void testCheckEndDateIsOverlap() {
        // given
        CronJob cronJob = new CronJob();
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setEndDate(Instant.parse("2029-11-12T00:00:00Z"));
        Instant endDate = Instant.now();
        given(userACL.isAdmin()).willReturn(true);
        given(cronJobRepository.getTopByStatusIsAndEndDateAfterOrderByEndDateDesc(CronJobStatus.RUNNING, endDate)).willReturn(Optional.of(cronJob));

        // when
        Instant result = cronJobService.getLatestDateOverlapEndDate(endDate);

        // then
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(Instant.parse("2029-11-12T00:00:00Z"));
    }

    /**
     * find list of cronjob
     */
    @Test
    public void testFindCronjobList() {
        List<CronJob> cronJobList = new ArrayList<>();
        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("RecipientList");

        EmailTemplate emailTemplate = new EmailTemplate();
        emailTemplate.setType(EmailTemplateType.MAIN);
        emailTemplate.setId(1L);
        emailTemplate.setContent("Content");
        emailTemplate.setName("template main");

        EmailTemplate emailTemplateReminder1 = new EmailTemplate();
        emailTemplateReminder1.setType(EmailTemplateType.REMINDER_1);
        emailTemplateReminder1.setId(2L);
        emailTemplateReminder1.setContent("Content reminder 1");
        emailTemplateReminder1.setName("template reminder");

        EmailTemplate emailTemplateReminder2 = new EmailTemplate();
        emailTemplateReminder2.setType(EmailTemplateType.REMINDER_2);
        emailTemplateReminder2.setId(3L);
        emailTemplateReminder2.setContent("Content reminder 1");
        emailTemplateReminder2.setName("template reminder");

        List<CronJobEmailTemplate> cronJobEmailTemplateList = new ArrayList<>();
        Instant endDate
            = Instant.parse("2021-11-12T00:00:00Z");
        for (int i = 0; i < 20; i++) {
            CronJob cronJob = new CronJob();
            cronJob.setId((long) (i + 1));
            cronJob.setArchived(i % 2 == 0);
            cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
            cronJob.setEndDate(endDate);
            cronJob.setSendHour(1);
            cronJob.setChunkNumber(100);
            cronJob.setWeekday(WeekDay.FRI);
            cronJob.setWeekOfMonth(1);
            cronJob.setStatus(CronJobStatus.FINISHED);
            cronJob.setRecipientList(recipientList);
            cronJobList.add(cronJob);
        }

        CronJobEmailTemplate cronJobEmailTemplateMain = new CronJobEmailTemplate();
        cronJobEmailTemplateMain.setCronJob(cronJobList.get(0));
        cronJobEmailTemplateMain.setEmailTemplate(emailTemplate);
        cronJobEmailTemplateMain.setType(EmailTemplateType.MAIN);
        cronJobEmailTemplateList.add(cronJobEmailTemplateMain);

        CronJobEmailTemplate cronJobEmailTemplate1 = new CronJobEmailTemplate();
        cronJobEmailTemplate1.setCronJob(cronJobList.get(0));
        cronJobEmailTemplate1.setEmailTemplate(emailTemplateReminder1);
        cronJobEmailTemplate1.setType(EmailTemplateType.REMINDER_1);
        cronJobEmailTemplateList.add(cronJobEmailTemplate1);

        CronJobEmailTemplate cronJobEmailTemplate2 = new CronJobEmailTemplate();
        cronJobEmailTemplate2.setCronJob(cronJobList.get(0));
        cronJobEmailTemplate2.setType(EmailTemplateType.REMINDER_2);
        cronJobEmailTemplate2.setEmailTemplate(emailTemplateReminder2);
        cronJobEmailTemplateList.add(cronJobEmailTemplate2);

        // given
        given(userACL.isAdmin()).willReturn(true);
        given(cronJobRepository.findAllByArchivedIsAndDeletedIsFalse(eq(true), any(Pageable.class))).willReturn(new PageImpl<>(cronJobList));
        given(cronJobEmailTemplateRepository.findAllByCronJob_Id(1L)).willReturn(cronJobEmailTemplateList);
        // when
        Page<CronjobDataDTO> page = cronJobService.findCronjobByArchived(PageRequest.of(0, 20), true);

        // then
        assertThat(page.getContent().size()).isEqualTo(20);
        CronjobDataDTO cronjobDataDTO = page.getContent().get(0);
        assertThat(cronjobDataDTO.getId()).isEqualTo(1);
        assertThat(cronjobDataDTO.getRecipientList().getId()).isEqualTo(1);
        assertThat(cronjobDataDTO.getRecipientList().getName()).isEqualTo("RecipientList");
        assertThat(cronjobDataDTO.getTemplateMain().getId()).isEqualTo(1);
        assertThat(cronjobDataDTO.getTemplateReminder1().getId()).isEqualTo(2);
        assertThat(cronjobDataDTO.getTemplateReminder2().getId()).isEqualTo(3);
        assertThat(cronjobDataDTO.getStatus()).isEqualTo(CronJobStatus.FINISHED);
        assertThat(cronjobDataDTO.getChunkNumber()).isEqualTo(100);
        assertThat(cronjobDataDTO.getFrequency()).isEqualTo(Frequency.ONCE_PER_WEEK);
        assertThat(cronjobDataDTO.getSendHour()).isEqualTo(1);
        assertThat(cronjobDataDTO.getWeekOfMonth()).isEqualTo(1);
        assertThat(cronjobDataDTO.getEndDate()).isEqualTo(endDate);
    }

    /**
     * archive cronjob from non permission user
     */
    @Test
    public void testArchiveCronJobCaseUserDontHavePermission() {
        // given
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> cronJobService.archive(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * archive non exist cronjob
     */
    @Test
    public void testArchiveCronJobFromNonExistData() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> cronJobService.archive(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    /**
     * archive cronjob failed due to cronjob is running
     */
    @Test
    public void testArchiveCronJobCaseCronJobIsRunning() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        CronJob cronJob = new CronJob();
        cronJob.setStatus(CronJobStatus.RUNNING);

        given(cronJobRepository.findById(eq(1L))).willReturn(Optional.of(cronJob));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> cronJobService.archive(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.cronjobStatusInvalid");
    }

    /**
     * archive cronjob failed due to cronjob is archived
     */
    @Test
    public void testRestoreCronJobCaseCronJobIsArchived() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        CronJob cronJob = new CronJob();
        cronJob.setStatus(CronJobStatus.CANCELLED);
        cronJob.setDeleted(false);
        cronJob.setArchived(true);

        given(cronJobRepository.findById(eq(1L))).willReturn(Optional.of(cronJob));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> cronJobService.archive(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.cronjobStatusInvalid");
    }

    /**
     * archive cronjob success
     */
    @Test
    public void testArchiveCronJobSuccess() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        CronJob cronJob = new CronJob();
        cronJob.setStatus(CronJobStatus.CANCELLED);
        cronJob.setDeleted(false);
        cronJob.setArchived(false);

        given(cronJobRepository.findById(eq(1L))).willReturn(Optional.of(cronJob));

        // when
        cronJobService.archive(1L);

        // then
        assertThat(cronJob.isArchived()).isEqualTo(true);
    }

    /**
     * create Cronjob failed chunk number less than 1
     */
    @Test
    public void testCreateCronjobInvalidChunkNumber() {
        // given
        Instant endDate
            = Instant.parse("2021-11-12T00:00:00Z");
        CronjobDTO cronjobDTO = new CronjobDTO();
        cronjobDTO.setRecipientListId(1L);
        cronjobDTO.setTemplateMainId(1L);
        cronjobDTO.setTemplateReminder1Id(3L);
        cronjobDTO.setTemplateReminder2Id(3L);
        cronjobDTO.setEndDate(endDate);
        cronjobDTO.setFrequency(Frequency.ONCE_PER_WEEK);
        cronjobDTO.setWeekday(WeekDay.FRI);
        cronjobDTO.setSendHour(0);
        cronjobDTO.setChunkNumber(0);
        cronjobDTO.setTemplateReminderTime1(10);
        cronjobDTO.setTemplateReminderTime2(13);

        given(userACL.isAdmin()).willReturn(true);

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> cronJobService.saveCronjob(cronjobDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.chunkNumberMustBeGreaterThanZero");
    }

    /**
     * create Cronjob failed template reminder time less than 1
     */
    @Test
    public void testCreateCronjobInvalidReminderTimeLessThan1() {
        // given
        Instant endDate
            = Instant.parse("2021-11-12T00:00:00Z");
        CronjobDTO cronjobDTO = new CronjobDTO();
        cronjobDTO.setRecipientListId(1L);
        cronjobDTO.setTemplateMainId(1L);
        cronjobDTO.setTemplateReminder1Id(3L);
        cronjobDTO.setTemplateReminder2Id(3L);
        cronjobDTO.setEndDate(endDate);
        cronjobDTO.setFrequency(Frequency.ONCE_PER_WEEK);
        cronjobDTO.setWeekday(WeekDay.FRI);
        cronjobDTO.setSendHour(0);
        cronjobDTO.setChunkNumber(100);
        cronjobDTO.setTemplateReminderTime1(0);
        cronjobDTO.setTemplateReminderTime2(3);

        given(userACL.isAdmin()).willReturn(true);

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> cronJobService.saveCronjob(cronjobDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.templateReminderMustBeGreaterThanZero");
    }

    /**
     * create Cronjob failed template reminder time 2 less than template reminder 1
     */
    @Test
    public void testCreateCronjobInvalidReminderTime() {
        // given
        Instant endDate
            = Instant.parse("2021-11-12T00:00:00Z");
        CronjobDTO cronjobDTO = new CronjobDTO();
        cronjobDTO.setRecipientListId(1L);
        cronjobDTO.setTemplateMainId(1L);
        cronjobDTO.setTemplateReminder1Id(3L);
        cronjobDTO.setTemplateReminder2Id(3L);
        cronjobDTO.setEndDate(endDate);
        cronjobDTO.setFrequency(Frequency.ONCE_PER_WEEK);
        cronjobDTO.setWeekday(WeekDay.FRI);
        cronjobDTO.setSendHour(0);
        cronjobDTO.setChunkNumber(100);
        cronjobDTO.setTemplateReminderTime1(14);
        cronjobDTO.setTemplateReminderTime2(13);

        given(userACL.isAdmin()).willReturn(true);

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> cronJobService.saveCronjob(cronjobDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.templateReminder2MustBeGreaterThan1");
    }

    /**
     * update cronjob status failed by not send all email template main
     */
    @Test
    public void testUpdateFailedByNotFinishSendAllEmailTemplateMain() {
        // given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setDeleted(false);
        cronJob.setArchived(false);

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setCronJob(cronJob);
        cronJobStatistic.setId(1L);
        cronJobStatistic.setTotalEmailToSend(1000);
        cronJobStatistic.setEmailSent(900);

        given(cronJobRepository.findById(1L)).willReturn(Optional.of(cronJob));
        given(cronJobStatisticRepository.findByCronJobId(1L)).willReturn(Optional.of(cronJobStatistic));

        // when
        cronJobService.updateIfFinished(cronJob);

        // then
        assertThat(cronJob.getStatus().equals(CronJobStatus.RUNNING)).isTrue();
    }

    /**
     * update cronjob status if is finished without SENT email tracking status
     */
    @Test
    public void testUpdateIfFinishedWithOutEmailTrackingStatusSent() {
        // given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setDeleted(false);
        cronJob.setArchived(false);

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setCronJob(cronJob);
        cronJobStatistic.setId(1L);
        cronJobStatistic.setTotalEmailToSend(1000);
        cronJobStatistic.setEmailSent(1000);

        given(cronJobRepository.findById(1L)).willReturn(Optional.of(cronJob));
        given(cronJobStatisticRepository.findByCronJobId(1L)).willReturn(Optional.of(cronJobStatistic));
        given(emailTrackingRepository.findAllEmailTrackingSentAndIncompleteByCronjobId(1L)).willReturn(new ArrayList<>());

        // when
        cronJobService.updateIfFinished(cronJob);

        // then
        assertThat(cronJob.getStatus().equals(CronJobStatus.FINISHED)).isTrue();
    }

    /**
     * update cronjob status if is finished with SENT email tracking status
     */
    @Test
    public void testUpdateIfFinishedWithEmailTrackingStatusSent() {
        // given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setDeleted(false);
        cronJob.setArchived(false);

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setCronJob(cronJob);
        cronJobStatistic.setId(1L);
        cronJobStatistic.setTotalEmailToSend(1000);
        cronJobStatistic.setEmailSent(1000);

        List<EmailTracking> emailTrackingList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            EmailTracking emailTracking = new EmailTracking();
            if (i % 2 == 0){
                emailTracking.setStatus(EmailStatus.SENT);
            }else {
                emailTracking.setStatus(EmailStatus.INCOMPLETE);
            }
            emailTracking.setSentReminder1(true);
            emailTracking.setSentReminder2(true);
            emailTrackingList.add(emailTracking);
        }

        given(cronJobRepository.findById(1L)).willReturn(Optional.of(cronJob));
        given(cronJobStatisticRepository.findByCronJobId(1L)).willReturn(Optional.of(cronJobStatistic));
        given(emailTrackingRepository.findAllEmailTrackingSentAndIncompleteByCronjobId(1L)).willReturn(emailTrackingList);

        // when
        cronJobService.updateIfFinished(cronJob);

        // then
        assertThat(cronJob.getStatus().equals(CronJobStatus.FINISHED)).isTrue();
    }

    /**
     * update cronjob status failed with SENT email tracking status
     */
    @Test
    public void testUpdateFailedWithEmailTrackingStatusSent() {
        // given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setDeleted(false);
        cronJob.setArchived(false);

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setCronJob(cronJob);
        cronJobStatistic.setId(1L);
        cronJobStatistic.setTotalEmailToSend(1000);
        cronJobStatistic.setEmailSent(1000);

        List<EmailTracking> emailTrackingList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            EmailTracking emailTracking = new EmailTracking();
            if (i % 2 == 0){
                emailTracking.setStatus(EmailStatus.SENT);
            }else {
                emailTracking.setStatus(EmailStatus.INCOMPLETE);
            }
            emailTracking.setSentReminder1(true);
            emailTracking.setSentReminder2(false);
            emailTrackingList.add(emailTracking);
        }

        given(cronJobRepository.findById(1L)).willReturn(Optional.of(cronJob));
        given(cronJobStatisticRepository.findByCronJobId(1L)).willReturn(Optional.of(cronJobStatistic));
        given(emailTrackingRepository.findAllEmailTrackingSentAndIncompleteByCronjobId(1L)).willReturn(emailTrackingList);

        // when
        cronJobService.updateIfFinished(cronJob);

        // then
        assertThat(cronJob.getStatus().equals(CronJobStatus.RUNNING)).isTrue();
    }

    /**
     * Save email tracking successfully
     */
    @Test
    public void testSaveEmailTracking() {
        // given
        CronJob cronJob = new CronJob();
        cronJob.setTemplateReminderTime1(7);
        cronJob.setTemplateReminderTime2(10);

        Recipient recipient = new Recipient();
        recipient.setId(1L);

        // when
        EmailTracking emailTracking = cronJobService.saveEmailTracking(cronJob, recipient);

        // then
        assertThat(emailTracking.getCronJob().getId()).isEqualTo(cronJob.getId());
        assertThat(emailTracking.getStatus()).isEqualTo(EmailStatus.SENT);
        assertThat(emailTracking.getRecipient().getId()).isEqualTo(recipient.getId());
        assertThat(emailTracking.getReminderDate1().truncatedTo(ChronoUnit.DAYS)).isEqualTo(Instant.now().plus(7, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
        assertThat(emailTracking.getReminderDate2().truncatedTo(ChronoUnit.DAYS)).isEqualTo(Instant.now().plus(10, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
    }

    /**
     * Save email tracking failed because email tracking already exist
     */
    @Test
    public void testSaveEmailFailedByEmailTrackingAlreadyExist() {
        // given
        CronJob cronJob = new CronJob();
        cronJob.setTemplateReminderTime1(7);
        cronJob.setTemplateReminderTime2(10);

        Recipient recipient = new Recipient();
        recipient.setId(1L);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setRecipient(recipient);
        emailTracking.setId(1L);

        given(emailTrackingRepository.findOneByRecipientAndCronJob(recipient, cronJob)).willReturn(Optional.of(emailTracking));
        // when
        EmailTracking savedEmailTracking = cronJobService.saveEmailTracking(cronJob, recipient);

        // then
        assertThat(savedEmailTracking).isNull();
    }

    /**
     * Get cronjob but not admin
     */
    @Test
    public void testGetCronjobsWithNotAdmin(){
        //given
        given(userACL.isAdmin()).willReturn(false);

        //when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, ()-> cronJobService.getCronjobs(PageRequest.of(0, 5), null));

        //then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * get cronjob with case name is null
     */
    @Test
    public void testGetCronjobsWithNameNull(){
        //given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setName("Test");
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setDeleted(false);
        cronJob.setArchived(false);

        given(userACL.isAdmin()).willReturn(true);
        given(cronJobRepository.findCronJobAllByDeletedIsFalse(PageRequest.of(0, 5))).willReturn(new PageImpl<>(Collections.singletonList(cronJob)));

        //when
        Page<CronjobDataDTO> result = cronJobService.getCronjobs(PageRequest.of(0, 5), null);

        //then
        assertThat(result.getContent().size()).isEqualTo(1);
        assertThat(result.getContent().get(0).getName()).isEqualTo("Test");

    }

    /**
     * get cronjob with case name not null
     */
    @Test
    public void testGetCronjobsWithNameNotNull(){
        //given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setName("Test");
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setDeleted(false);
        cronJob.setArchived(false);

        given(userACL.isAdmin()).willReturn(true);
        given(cronJobRepository.findAllByNameContainingAndDeletedIsFalse(PageRequest.of(0, 5), "Te")).willReturn(new PageImpl<>(Collections.singletonList(cronJob)));

        //when
        Page<CronjobDataDTO> result = cronJobService.getCronjobs(PageRequest.of(0, 5), "Te");

        //then
        assertThat(result.getContent().size()).isEqualTo(1);
        assertThat(result.getContent().get(0).getName()).isEqualTo("Test");

    }

}