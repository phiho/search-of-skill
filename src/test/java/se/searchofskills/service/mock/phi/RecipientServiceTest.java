package se.searchofskills.service.mock.phi;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

import org.mockito.junit.jupiter.MockitoExtension;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import se.searchofskills.domain.Recipient;
import se.searchofskills.domain.RecipientList;
import se.searchofskills.domain.enumeration.CronJobStatus;
import se.searchofskills.repository.RecipientListRepository;
import se.searchofskills.repository.RecipientRepository;
import se.searchofskills.service.RecipientService;
import se.searchofskills.service.UserACL;
import se.searchofskills.service.dto.RecipientListDTO;
import se.searchofskills.service.error.AccessForbiddenException;


import org.junit.jupiter.api.Test;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.ResourceNotFoundException;

import javax.swing.text.html.Option;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * 1. Tạo mock data
 * 2. Định nghĩa hành vi
 * 3. Gọi method
 * 4. Kiểm tra kết quả
 */
@ExtendWith(MockitoExtension.class)
public class RecipientServiceTest {
    @InjectMocks
    RecipientService recipientService;

    @Mock
    RecipientRepository recipientRepository;
    @Mock
    RecipientListRepository recipientListRepository;
    @Mock
    UserACL userACL;

    @Test
    public void testImportRecipientListWithNonAdmin() {
        /**
         * userACL = access control list
         * method = boolean isAdmin().
         */
        given(userACL.isAdmin()).willReturn(false);
        //when
        //()->recipientService.importRecipientList(null, ""). This are candidates for method call
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> recipientService.importRecipientList(null, ""));

        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testImportRecipientListSuccess() throws IOException {
        given(userACL.isAdmin()).willReturn(true);
        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        File file = new File("src/main/resources/testing_excel.xlsx");

        given(recipientListRepository.save(any(RecipientList.class))).willReturn(recipientList);

        List<Recipient> recipients = recipientService.importRecipientList(new FileInputStream(file), "testing_file.xlsx");

        assertThat(recipients.size()).isEqualTo(10);
        //getRecipientList -> retrieve RecipientList of 1 Recipient
        assertThat(recipients.get(0).getRecipientList().getId()).isEqualTo(1L);
    }

    @Test
    public void testImportRecipientListWithRedundantColumn() throws IOException {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_add_more_column.xlsx");
        given(recipientListRepository.save(any(RecipientList.class))).willReturn(recipientList);

        List<Recipient> recipients = recipientService.importRecipientList(new FileInputStream(file), "file linhtinh");

        assertThat(recipients.size()).isEqualTo(10);
        assertThat(recipients.get(0).getRecipientList().getId()).isEqualTo(1L);
        assertThat(recipients.get(0).getEmail()).isEqualTo("sebastian.fjall@vallentuna.se");
        assertThat(recipients.get(3).getReferenceNumber()).isEqualTo("TOP-2020/40");
    }


    @Test
    //empty data in 1 column of a record
    public void testImportBlankData() throws FileNotFoundException {
        given(userACL.isAdmin()).willReturn(true);
        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_empty_data.xlsx");

        BadRequestException badRequestException = assertThrows(BadRequestException.class,
            () -> recipientService.importRecipientList(new FileInputStream(file), "test_no_records.xlsx"));
        assertThat(badRequestException.getMessage()).isEqualTo("error.uploadExcelDataEmpty");
    }

    @Test
    public void testImportEmptyField() {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_empty_any_column.xlsx");

        // when
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> recipientService.importRecipientList(new FileInputStream(file), "testing_excel_empty_any_column.xlsx"));

        // then
        assertThat(result.getMessage()).isEqualTo("error.cellContentError: 2-7");
    }

    @Test
    public void testImportInvalidEmail() throws FileNotFoundException {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        File file = new File("src/main/resources/testing_excel_non_invalid_email.xlsx");

        BadRequestException result = assertThrows(BadRequestException.class,
            () -> recipientService.importRecipientList(new FileInputStream(file), "recipientListName"));
        assertThat(result.getMessage()).isEqualTo(("error.invalidEmailFromExcel: 2-6"));
    }

    @Test
    public void testImportInvalidRegistrar() {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        File file = new File("src/main/resources/testing_excel_non_invalid_registrar.xlsx");

        BadRequestException result = assertThrows(BadRequestException.class,
            () -> recipientService.importRecipientList(new FileInputStream(file), "list"));
    }

    @Test
    public void testImportUploadingWrongFormat() {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/~$testing_excel_non_invalid_email.xlsx");

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> recipientService.importRecipientList(new FileInputStream(file), "~$testing_excel_non_invalid_email.xlsx"));

        // then
        assertThat(result.getMessage()).isEqualTo("error.uploadExcelWrongFormat");
    }

    @Test
    public void testImportRegistrarNull() throws FileNotFoundException {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_registrar_null.xlsx");

        // when
        List<Recipient> result = recipientService.importRecipientList(new FileInputStream(file), "testing_excel_registrar_null.xlsx");

        // record number 4 -> 3
        assertThat(result.get(3).getTitle()).isEqualTo("UPPHANDLING LOKALANPASSNING GULDET, GÄLLIVARE");
        assertThat(result.get(3).getRegistrar()).isNull();
    }

    @Test
    public void testImportRecipientListWithReferenceNumberNull() throws FileNotFoundException {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_reference_number_null.xlsx");

        // when
        List<Recipient> result = recipientService.importRecipientList(new FileInputStream(file), "testing_excel_reference_number_null.xlsx");

        // then
        assertThat(result.get(0).getTitle()).isEqualTo("GC-väg Brottby");
        assertThat(result.get(0).getReferenceNumber()).isNull();
    }

    /**
     * create recipient with import excel with email null
     * return handle exception
     */
    @Test
    public void testImportRecipientListWithEmailNull() throws FileNotFoundException {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_enail_null.xlsx");

        // when
        List<Recipient> result = recipientService.importRecipientList(new FileInputStream(file), "testing_excel_enail_null.xlsx");

        // then
        assertThat(result.get(0).getTitle()).isEqualTo("GC-väg Brottby");
        assertThat(result.get(0).getReferenceNumber()).isEqualTo("KS 2020.142");
        assertThat(result.get(0).getProcuringAgency()).isEqualTo("Vallentuna kommun");
        assertThat(result.get(0).getRegistrar()).isEqualTo("kund@vasyd.se");
        assertThat(result.get(0).getEmail()).isNull();
    }

    /**
     * create recipient with import excel with register and email null
     * return handle exception
     */
    @Test
    public void testImportRecipientListWithBothRegisterEmailNull() throws FileNotFoundException {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_registrar_and_email_null.xlsx");

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> recipientService.importRecipientList(new FileInputStream(file), "testing_excel_registrar_and_email_null.xlsx"));

        // then
        assertThat(result.getMessage()).isEqualTo("error.bothEmailAndRegisterCannotEmpty: 2-5");
    }

    /**
     * create recipient with import excel with any column is number
     * return handle exception
     */
    @Test
    public void testImportRecipientListWithColumnIsNumber() throws FileNotFoundException {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_any_colum_is_number.xlsx");

        // when
        List<Recipient> result = recipientService.importRecipientList(new FileInputStream(file), "testing_excel_registrar_null.xlsx");

        // then
        assertThat(result.size()).isEqualTo(10);
        assertThat(result.get(1).getProcuringAgency()).isEqualTo("1.0");
    }

    /**
     * create recipient with import excel with all column is number
     * return handle exception
     */
    @Test
    public void testImportRecipientListWithAllColumnIsNumber() throws FileNotFoundException {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_all_colum_is_number.xlsx");

        // when
        List<Recipient> result = recipientService.importRecipientList(new FileInputStream(file), "testing_excel_registrar_null.xlsx");

        // then
        assertThat(result.size()).isEqualTo(10);
        assertThat(result.get(1).getTitle()).isEqualTo("1.0");
        assertThat(result.get(1).getReferenceNumber()).isEqualTo("1.0");
        assertThat(result.get(1).getProcuringAgency()).isEqualTo("1.0");
        assertThat(result.get(1).getCounty()).isEqualTo("1.0");
        assertThat(result.get(1).getProcurementUrl()).isEqualTo("1.0");
    }

    @Test
    public void testRemoveEmptyWithValueCellIsBlank() throws IOException {
        //given
        File file = new File("src/main/resources/test_excel_correct_rules_cell_blank.xlsx");
        FileInputStream fileInputStream = new FileInputStream(file);
        Workbook workbook = WorkbookFactory.create(fileInputStream);
        Sheet sheet = workbook.getSheetAt(0);

        //when
        Sheet result = recipientService.removeEmptyRow(sheet);

        //then remove row 5 bacause row 5 has cells blank
        assertThat(result.getLastRowNum()).isEqualTo(10);
        assertThat(result.getRow(5)).isNull();
        assertThat(result.getRow(1)).isNotNull();
        assertThat(result.getRow(2)).isNotNull();
        assertThat(result.getRow(3)).isNotNull();
        assertThat(result.getRow(4)).isNotNull();
        assertThat(result.getRow(6)).isNotNull();
        assertThat(result.getRow(7)).isNotNull();
        assertThat(result.getRow(8)).isNotNull();
        assertThat(result.getRow(9)).isNotNull();
        assertThat(result.getRow(10)).isNotNull();

    }

    @Test
    public void testUpdateRecipientListNameCaseUserNonPermission() {
        given(userACL.isAdmin()).willReturn(false);

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class
            , () -> recipientService.updateRecipientList(new RecipientListDTO()));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");

    }

    @Test
    public void testDeleteRecipientListNameCaseRecipientNotFound() {
        given(userACL.isAdmin()).willReturn(true);
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class
            , () -> recipientService.deleteRecipientList(1L));

        assertThat(result.getMessage()).isEqualTo("error.notFound");

    }

    @Test
    public void testUpdateRecipientListName() {
        given(userACL.isAdmin()).willReturn(true);
        RecipientListDTO recipientListDTO = new RecipientListDTO();
        recipientListDTO.setId(1L);
        recipientListDTO.setName("   new    Name    ");

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("oldName");

        given(recipientListRepository.findById(eq(1L))).willReturn(Optional.of(recipientList));

        recipientService.updateRecipientList(recipientListDTO);

        assertThat(recipientList.getName()).isEqualTo("new Name");
    }

    @Test
    public void testDeleteRecipientList() {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setDeleted(false);

        given(recipientListRepository.findByIdAndDeletedIsFalse(eq(1L))).willReturn(Optional.of(recipientList));
        given(recipientListRepository.countAllCrobjobIsUsingRecipientById(CronJobStatus.RUNNING, 1L)).willReturn(0L);
        //when
        recipientService.deleteRecipientList(1L);

        assertThat(recipientList.isDeleted()).isEqualTo(true);
    }

    @Test
    public void testFindAllRecipientListByName() {
        given(userACL.isAdmin()).willReturn(true);
        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("Name");
        given(recipientListRepository.findAllByDeletedAndNameContainingIgnoreCase(PageRequest.of(1, 20), false, "a"))
            .willReturn(new PageImpl<>(Collections.singletonList(recipientList)));
        Page<RecipientListDTO> page = recipientService.findAllRecipientListsByName(PageRequest.of(1, 20), "a");

        //then
        assertThat(page.getContent().size()).isEqualTo(1);
        assertThat(page.getContent().get(0).getId()).isEqualTo(1);
        assertThat(page.getContent().get(0).getName().contains("a")).isTrue();
    }

    @Test
    public void testFindAllRecipientListWithoutNameParam() {
        given(userACL.isAdmin()).willReturn(true);
        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("Name");

        given(recipientListRepository.findAllByDeleted(PageRequest.of(1, 20), false))
            .willReturn(new PageImpl<>(Collections.singletonList(recipientList)));

        Page<RecipientListDTO> page = recipientService.findAllRecipientListsByName(PageRequest.of(1, 20), "");

        assertThat(page.getContent().size()).isEqualTo(1);
        assertThat(page.getContent().get(0).getName().equals("Name")).isTrue();
        assertThat(page.getContent().get(0).getId()).isEqualTo(1);
    }

    @Test
    public void testDeleteRecipientListIsUsingByRunningCronjob() {
        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setDeleted(false);

        given(userACL.isAdmin()).willReturn(true);
        given(recipientListRepository.findByIdAndDeletedIsFalse(1L))
            .willReturn(Optional.of(recipientList));
        given(recipientListRepository.countAllCrobjobIsUsingRecipientById(CronJobStatus.RUNNING, 1L)).willReturn(1L);

        BadRequestException result = assertThrows(BadRequestException.class
        , ()->recipientService.deleteRecipientList(1L));

        assertThat(result.getMessage()).isEqualTo("error.recipientListAlreadyInUse");
    }
    @Test
    public void testUpdateRecipientListNameCaseAllWhiteSpace(){
        given(userACL.isAdmin()).willReturn(true);
        RecipientListDTO recipientListDTO = new RecipientListDTO();
        recipientListDTO.setId(1L);
        recipientListDTO.setName("     ");

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("old Name");
        given(recipientListRepository.findById(eq(1L))).willReturn(Optional.of(recipientList));

        BadRequestException result = assertThrows(BadRequestException.class
        ,()-> recipientService.updateRecipientList(recipientListDTO));
        assertThat(result.getMessage()).isEqualTo("error.noWhiteSpace");
    }
}
