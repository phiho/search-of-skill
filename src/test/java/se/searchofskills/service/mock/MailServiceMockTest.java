package se.searchofskills.service.mock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.searchofskills.config.ApplicationProperties;
import se.searchofskills.service.MailService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class MailServiceMockTest {
    @Mock
    private ApplicationProperties applicationProperties;

    @InjectMocks
    private MailService mailService;

    /**
     * Test is able to send all email
     */
    @Test
    public void testIsAbleToSendAllEmail() {
        // given
        given(applicationProperties.isSendAll()).willReturn(true);

        // when
        boolean result = mailService.isAbleToSendToSpecificEmail("huudk@techlead.vn");

        // then
        assertThat(result).isEqualTo(true);
    }

    /**
     *  Test is able to send specific email
     */
    @Test
    public void testIsAbleToSendToSpecificEmail() {
        // given
        String[] emails = {"huudk@techlead.vn"};
        given(applicationProperties.isSendAll()).willReturn(false);
        given(applicationProperties.getEmails()).willReturn(emails);

        // when
        boolean result = mailService.isAbleToSendToSpecificEmail("huudk@techlead.vn");

        // then
        assertThat(result).isEqualTo(true);
    }

    /**
     * Test user can not receive email by missing in configuration
     */
    @Test
    public void testIsNotAbleToSendToSpecificEmail() {
        // given
        String[] emails = {"khachuuptit@gmail.com"};
        given(applicationProperties.isSendAll()).willReturn(false);
        given(applicationProperties.getEmails()).willReturn(emails);

        // when
        boolean result = mailService.isAbleToSendToSpecificEmail("huudk@techlead.vn");

        // then
        assertThat(result).isEqualTo(false);
    }
}
