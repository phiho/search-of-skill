package se.searchofskills.service.mock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import se.searchofskills.domain.*;
import se.searchofskills.domain.enumeration.EmailStatus;
import se.searchofskills.domain.enumeration.EmailTemplateType;
import se.searchofskills.repository.CronJobStatisticRepository;
import se.searchofskills.repository.CustomEmailTrackingRepository;
import se.searchofskills.repository.EmailTemplateRepository;
import se.searchofskills.repository.EmailTrackingRepository;
import se.searchofskills.repository.RecipientRepository;
import se.searchofskills.service.AutoSendEmailEvent;
import se.searchofskills.service.CronJobService;
import se.searchofskills.service.EmailTrackingService;
import se.searchofskills.service.UserACL;
import se.searchofskills.service.dto.EmailTrackingDTO;
import se.searchofskills.service.dto.EmailTrackingDataDTO;
import se.searchofskills.service.dto.ResendEmailTrackingDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.ResourceNotFoundException;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class EmailTrackingServiceMockTest {
    @InjectMocks
    EmailTrackingService emailTrackingService;

    @Mock
    CronJobService cronJobService;

    @Mock
    EmailTrackingRepository emailTrackingRepository;

    @Mock
    CronJobStatisticRepository cronJobStatisticRepository;

    @Mock
    EmailTemplateRepository emailTemplateRepository;

    @Mock
    ApplicationEventPublisher applicationEventPublisher;

    @Mock
    CustomEmailTrackingRepository customEmailTrackingRepository;

    @Mock
    private UserACL userACL;

    @Mock
    RecipientRepository recipientRepository;

    /**
     * get list email tracking from non admin permission
     */
    @Test
    public void testGetListEmailTrackingFromNonAdminPermission() {
        // given
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> emailTrackingService.getListOfEmailTracking(PageRequest.of(0, 10), null, null, false));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * get list email tracking all successfully
     */
    @Test
    public void testGetListEmailTrackingAllSuccessfully() {
        // given
        List<EmailTracking> emailTrackingList = new ArrayList<>();
        Instant now = Instant.now();
        for (int i = 0; i < 20; i++) {
            long id = i + 1;
            EmailTracking emailTracking = new EmailTracking();
            emailTracking.setId(id);
            emailTracking.setReminderDate1(now.plusSeconds(7 * 86400));
            emailTracking.setReminderDate2(now.plusSeconds(14 * 86400));
            emailTracking.setStatus(EmailStatus.SENT);

            CronJob cronJob = new CronJob();
            cronJob.setId(id);

            RecipientList recipientList = new RecipientList();
            recipientList.setId(id);
            recipientList.setName("RecipientList" + id);

            Recipient recipient = new Recipient();
            recipient.setId(id);
            recipient.setRecipientList(recipientList);

            emailTracking.setRecipient(recipient);
            emailTracking.setCronJob(cronJob);
            emailTrackingList.add(emailTracking);

        }
        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findAllByDeletedIsFalseAndArchived(any(Pageable.class), eq(false))).willReturn(new PageImpl<>(emailTrackingList));

        // when
        Page<EmailTrackingDTO> page = emailTrackingService.getListOfEmailTracking(PageRequest.of(0, 20), null, null, false);

        // then
        assertThat(page.getContent().size()).isEqualTo(20);
        EmailTrackingDTO emailTrackingDTO = page.getContent().get(0);
        assertThat(emailTrackingDTO.getId()).isEqualTo(1);
        assertThat(emailTrackingDTO.getCronJob().getId()).isEqualTo(1);
        assertThat(emailTrackingDTO.getRecipient().getId()).isEqualTo(1);
        assertThat(emailTrackingDTO.getStatus()).isEqualTo(EmailStatus.SENT);
        assertThat(emailTrackingDTO.getReminderDate1()).isEqualTo(now.plusSeconds(7 * 86400));
        assertThat(emailTrackingDTO.getReminderDate2()).isEqualTo(now.plusSeconds(14 * 86400));

    }

    /**
     * get list email tracking with recipientId and not archive successfully
     */
    @Test
    public void testGetListEmailTrackingWithRecipientIdAndNotArchiveSuccessfully() {
        // given
        List<EmailTracking> emailTrackingList = new ArrayList<>();
        Instant now = Instant.now();
        for (int i = 10; i < 20; i++) {
            long id = i;
            EmailTracking emailTracking = new EmailTracking();
            emailTracking.setId(id);
            emailTracking.setReminderDate1(now.plusSeconds(7 * 86400));
            emailTracking.setReminderDate2(now.plusSeconds(14 * 86400));
            emailTracking.setStatus(EmailStatus.SENT);

            CronJob cronJob = new CronJob();
            cronJob.setId(id);

            RecipientList recipientList = new RecipientList();
            recipientList.setId(id);
            recipientList.setName("RecipientList" + id);

            Recipient recipient = new Recipient();
            recipient.setId(id);
            recipient.setRecipientList(recipientList);

            emailTracking.setRecipient(recipient);
            emailTracking.setCronJob(cronJob);
            emailTrackingList.add(emailTracking);

        }

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findAllByThisId(any(Pageable.class), any(Long.class), eq(false))).willReturn(new PageImpl<>(emailTrackingList));

        // when
        Page<EmailTrackingDTO> page = emailTrackingService.getListOfEmailTracking(PageRequest.of(0, 20), 1L, null, false);

        // then
        assertThat(page.getContent().size()).isEqualTo(10);
        EmailTrackingDTO emailTrackingDTO = page.getContent().get(0);
        assertThat(emailTrackingDTO.getId()).isEqualTo(10);
        assertThat(emailTrackingDTO.getCronJob().getId()).isEqualTo(10);
        assertThat(emailTrackingDTO.getRecipient().getId()).isEqualTo(10);
        assertThat(emailTrackingDTO.getStatus()).isEqualTo(EmailStatus.SENT);
        assertThat(emailTrackingDTO.getReminderDate1()).isEqualTo(now.plusSeconds(7 * 86400));
        assertThat(emailTrackingDTO.getReminderDate2()).isEqualTo(now.plusSeconds(14 * 86400));

    }

    /**
     * get list email tracking with recipientId and archived successfully
     */
    @Test
    public void testGetListEmailTrackingWithRecipientIdAndArchivedSuccessfully() {
        // given
        List<EmailTracking> emailTrackingList = new ArrayList<>();
        Instant now = Instant.now();
        for (int i = 10; i < 20; i++) {
            long id = i;
            EmailTracking emailTracking = new EmailTracking();
            emailTracking.setId(id);
            emailTracking.setReminderDate1(now.plusSeconds(7 * 86400));
            emailTracking.setReminderDate2(now.plusSeconds(14 * 86400));
            emailTracking.setStatus(EmailStatus.SENT);
            emailTracking.setArchived(true);

            CronJob cronJob = new CronJob();
            cronJob.setId(id);

            RecipientList recipientList = new RecipientList();
            recipientList.setId(id);
            recipientList.setName("RecipientList" + id);

            Recipient recipient = new Recipient();
            recipient.setId(id);
            recipient.setRecipientList(recipientList);

            emailTracking.setRecipient(recipient);
            emailTracking.setCronJob(cronJob);
            emailTrackingList.add(emailTracking);

        }

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findAllByThisId(any(Pageable.class), eq(1L), eq(true))).willReturn(new PageImpl<>(emailTrackingList));

        // when
        Page<EmailTrackingDTO> page = emailTrackingService.getListOfEmailTracking(PageRequest.of(0, 20), 1L, null, true);

        // then
        assertThat(page.getContent().size()).isEqualTo(10);
        EmailTrackingDTO emailTrackingDTO = page.getContent().get(0);
        assertThat(emailTrackingDTO.getId()).isEqualTo(10);
        assertThat(emailTrackingDTO.getCronJob().getId()).isEqualTo(10);
        assertThat(emailTrackingDTO.getRecipient().getId()).isEqualTo(10);
        assertThat(emailTrackingDTO.getStatus()).isEqualTo(EmailStatus.SENT);
        assertThat(emailTrackingDTO.getReminderDate1()).isEqualTo(now.plusSeconds(7 * 86400));
        assertThat(emailTrackingDTO.getReminderDate2()).isEqualTo(now.plusSeconds(14 * 86400));

    }

    /**
     * get list email tracking with keyword successfully
     */
    @Test
    public void testGetListEmailTrackingWithKeywordSuccessfully() {
        // given
        List<EmailTracking> emailTrackingList = new ArrayList<>();
        Instant now = Instant.now();
        for (int i = 0; i < 20; i++) {
            long id = i + 1;
            EmailTracking emailTracking = new EmailTracking();
            emailTracking.setId(id);
            emailTracking.setReminderDate1(now.plusSeconds(7 * 86400));
            emailTracking.setReminderDate2(now.plusSeconds(14 * 86400));
            emailTracking.setStatus(EmailStatus.SENT);

            CronJob cronJob = new CronJob();
            cronJob.setId(id);

            RecipientList recipientList = new RecipientList();
            recipientList.setId(id);
            recipientList.setName("RecipientList" + id);

            Recipient recipient = new Recipient();
            recipient.setId(id);
            recipient.setRecipientList(recipientList);

            emailTracking.setRecipient(recipient);
            emailTracking.setCronJob(cronJob);
            emailTrackingList.add(emailTracking);

        }

        given(userACL.isAdmin()).willReturn(true);
        given(customEmailTrackingRepository.findEmailTrackingCriteria(any(Pageable.class), eq("RecipientList"), eq(false))).willReturn(new PageImpl<>(emailTrackingList));

        // when
        Page<EmailTrackingDTO> page = emailTrackingService.getListOfEmailTracking(PageRequest.of(0, 20), null, "RecipientList", false);

        // then
        assertThat(page.getContent().size()).isEqualTo(20);
        EmailTrackingDTO emailTrackingDTO = page.getContent().get(0);
        assertThat(emailTrackingDTO.getId()).isEqualTo(1);
        assertThat(emailTrackingDTO.getCronJob().getId()).isEqualTo(1);
        assertThat(emailTrackingDTO.getRecipient().getId()).isEqualTo(1);
        assertThat(emailTrackingDTO.getStatus()).isEqualTo(EmailStatus.SENT);
        assertThat(emailTrackingDTO.getReminderDate1()).isEqualTo(now.plusSeconds(7 * 86400));
        assertThat(emailTrackingDTO.getReminderDate2()).isEqualTo(now.plusSeconds(14 * 86400));

    }

    /**
     * get an email tracking from non admin permission
     */
    @Test
    public void testGetAnEmailTrackingFromNonAdminPermission() {
        // given
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> emailTrackingService.findOne(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * get an email tracking not found
     */
    @Test
    public void testGetAnEmailTrackingNotFound() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> emailTrackingService.findOne(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    /**
     * get an email tracking successfully
     */
    @Test
    public void testGetAnEmailTrackingSuccessfully() {
        Instant now = Instant.now();
        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setReminderDate1(now.plusSeconds(7 * 86400));
        emailTracking.setReminderDate2(now.plusSeconds(14 * 86400));
        emailTracking.setStatus(EmailStatus.SENT);

        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("RecipientList" + 1L);

        Recipient recipient = new Recipient();
        recipient.setId(1L);
        recipient.setRecipientList(recipientList);

        emailTracking.setRecipient(recipient);
        emailTracking.setCronJob(cronJob);
        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.of(emailTracking));

        // when
        EmailTrackingDTO emailTrackingDTO = emailTrackingService.findOne(1L);

        // then
        assertThat(emailTrackingDTO.getId()).isEqualTo(1);
        assertThat(emailTrackingDTO.getCronJob().getId()).isEqualTo(1);
        assertThat(emailTrackingDTO.getRecipient().getId()).isEqualTo(1);
        assertThat(emailTrackingDTO.getStatus()).isEqualTo(EmailStatus.SENT);
        assertThat(emailTrackingDTO.getReminderDate1()).isEqualTo(now.plusSeconds(7 * 86400));
        assertThat(emailTrackingDTO.getReminderDate2()).isEqualTo(now.plusSeconds(14 * 86400));
    }

    /**
     * update an email tracking from non admin permission
     */
    @Test
    public void testUpdateAnEmailTrackingFromNonAdminPermission() {
        // given
        given(userACL.isAdmin()).willReturn(false);
        Instant now = Instant.now();
        EmailTrackingDataDTO emailTrackingDTO = new EmailTrackingDataDTO();
        emailTrackingDTO.setId(1L);
        emailTrackingDTO.setStatus(EmailStatus.APPROVED);
        emailTrackingDTO.setReminderDate1(now.plusSeconds(10 * 86400));
        emailTrackingDTO.setReminderDate2(now.plusSeconds(20 * 86400));
        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> emailTrackingService.updateEmailTracking(emailTrackingDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * update an email tracking not found
     */
    @Test
    public void testUpdateAnEmailTrackingNotFound() {
        // given
        Instant now = Instant.now();
        given(userACL.isAdmin()).willReturn(true);
        EmailTrackingDataDTO emailTrackingDTO = new EmailTrackingDataDTO();
        emailTrackingDTO.setId(1L);
        emailTrackingDTO.setStatus(EmailStatus.APPROVED);
        emailTrackingDTO.setReminderDate1(now.plusSeconds(10 * 86400));
        emailTrackingDTO.setReminderDate2(now.plusSeconds(20 * 86400));

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> emailTrackingService.updateEmailTracking(emailTrackingDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    /**
     * update an email tracking archived
     */
    @Test
    public void testUpdateAnEmailTrackingArchive() {
        // given
        Instant now = Instant.now();
        given(userACL.isAdmin()).willReturn(true);
        EmailTrackingDataDTO emailTrackingDTO = new EmailTrackingDataDTO();
        emailTrackingDTO.setId(1L);
        emailTrackingDTO.setStatus(EmailStatus.APPROVED);
        emailTrackingDTO.setReminderDate1(now.plusSeconds(10 * 86400));
        emailTrackingDTO.setReminderDate2(now.plusSeconds(20 * 86400));

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setArchived(true);
        given(emailTrackingRepository.findByIdAndDeletedIsFalse(emailTrackingDTO.getId())).willReturn(Optional.of(emailTracking));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTrackingService.updateEmailTracking(emailTrackingDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.emailTrackingArchived");
    }

    /**
     * update an email tracking successfully
     */
    @Test
    public void testUpdateAnEmailTrackingSuccessfully() {
        Instant now = Instant.now();
        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setReminderDate1(now.plusSeconds(7 * 86400));
        emailTracking.setReminderDate2(now.plusSeconds(14 * 86400));
        emailTracking.setStatus(EmailStatus.SENT);

        EmailTrackingDataDTO emailTrackingDTO = new EmailTrackingDataDTO();
        emailTrackingDTO.setId(1L);
        emailTrackingDTO.setStatus(EmailStatus.APPROVED);
        emailTrackingDTO.setReminderDate1(now.plusSeconds(10 * 86400));
        emailTrackingDTO.setReminderDate2(now.plusSeconds(20 * 86400));

        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("RecipientList" + 1L);

        Recipient recipient = new Recipient();
        recipient.setId(1L);
        recipient.setEmail("johndoe@gmail.com");
        recipient.setRecipientList(recipientList);

        emailTracking.setRecipient(recipient);
        emailTracking.setCronJob(cronJob);
        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.of(emailTracking));

        // when
        EmailTrackingDataDTO updatedEmailTrackingDTO = emailTrackingService.updateEmailTracking(emailTrackingDTO);

        // then
        then(emailTrackingRepository).should(times(1)).save(emailTracking);
        assertThat(emailTrackingDTO.getId()).isEqualTo(1);
        assertThat(updatedEmailTrackingDTO.getStatus()).isEqualTo(EmailStatus.APPROVED);
        assertThat(updatedEmailTrackingDTO.getReminderDate1()).isEqualTo(now.plusSeconds(10 * 86400));
        assertThat(updatedEmailTrackingDTO.getReminderDate2()).isEqualTo(now.plusSeconds(20 * 86400));
    }

    /**
     * update an email tracking and registrar successfully
     */
    @Test
    public void testUpdateAnEmailTrackingAndRegistrarSuccessfully() {
        Instant now = Instant.now();
        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setReminderDate1(now.plusSeconds(7 * 86400));
        emailTracking.setReminderDate2(now.plusSeconds(14 * 86400));
        emailTracking.setStatus(EmailStatus.SENT);

        EmailTrackingDataDTO emailTrackingDTO = new EmailTrackingDataDTO();
        emailTrackingDTO.setId(1L);
        emailTrackingDTO.setStatus(EmailStatus.APPROVED);
        emailTrackingDTO.setReminderDate1(now.plusSeconds(10 * 86400));
        emailTrackingDTO.setReminderDate2(now.plusSeconds(20 * 86400));
        emailTrackingDTO.setRegistrar("Techlead@gmail.com");

        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("RecipientList" + 1L);

        Recipient recipient = new Recipient();
        recipient.setId(1L);
        recipient.setRecipientList(recipientList);
        recipient.setRegistrar("Techlead@techlead.vn");

        emailTracking.setRecipient(recipient);
        emailTracking.setCronJob(cronJob);
        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.of(emailTracking));

        // when
        EmailTrackingDataDTO updatedEmailTrackingDTO = emailTrackingService.updateEmailTracking(emailTrackingDTO);

        // then
        then(emailTrackingRepository).should(times(1)).save(emailTracking);
        assertThat(emailTrackingDTO.getId()).isEqualTo(1);
        assertThat(updatedEmailTrackingDTO.getStatus()).isEqualTo(EmailStatus.APPROVED);
        assertThat(updatedEmailTrackingDTO.getReminderDate1()).isEqualTo(now.plusSeconds(10 * 86400));
        assertThat(updatedEmailTrackingDTO.getReminderDate2()).isEqualTo(now.plusSeconds(20 * 86400));
        assertThat(updatedEmailTrackingDTO.getRegistrar()).isEqualTo("Techlead@gmail.com");
    }

    /**
     * update an email tracking and registrar with case email invalid
     */
    @Test
    public void testUpdateAnEmailTrackingAndRegistrarWithCaseEmailInvalid() {
        Instant now = Instant.now();
        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setReminderDate1(now.plusSeconds(7 * 86400));
        emailTracking.setReminderDate2(now.plusSeconds(14 * 86400));
        emailTracking.setStatus(EmailStatus.SENT);

        EmailTrackingDataDTO emailTrackingDTO = new EmailTrackingDataDTO();
        emailTrackingDTO.setId(1L);
        emailTrackingDTO.setStatus(EmailStatus.APPROVED);
        emailTrackingDTO.setReminderDate1(now.plusSeconds(10 * 86400));
        emailTrackingDTO.setReminderDate2(now.plusSeconds(20 * 86400));
        emailTrackingDTO.setRegistrar("Techgmail.com");

        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("RecipientList" + 1L);

        Recipient recipient = new Recipient();
        recipient.setId(1L);
        recipient.setRecipientList(recipientList);
        recipient.setRegistrar("Techlead@techlead.vn");

        emailTracking.setRecipient(recipient);
        emailTracking.setCronJob(cronJob);
        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.of(emailTracking));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTrackingService.updateEmailTracking(emailTrackingDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.invalidEmail");
    }

    /**
     * delete from non permission user
     */
    @Test
    public void testDeleteCronJobCaseNonPermissionUser() {
        // given
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> emailTrackingService.delete(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * delete email tracking not found
     */
    @Test
    public void testDeleteEmailTrackingNotFound() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setDeleted(false);

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> emailTrackingService.delete(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    /**
     * delete email tracking not archive
     */
    @Test
    public void testDeleteEmailTrackingNotArchive() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setArchived(false);
        emailTracking.setDeleted(false);

        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1)).willReturn(Optional.of(emailTracking));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTrackingService.delete(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.emailTrackingNotArchived");
    }

    /**
     * delete email tracking success
     */
    @Test
    public void testDeleteEmailTrackingSuccess() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setArchived(true);
        emailTracking.setDeleted(false);

        given(emailTrackingRepository.findByIdAndDeletedIsFalse(eq(1L))).willReturn(Optional.of(emailTracking));

        // when
        emailTrackingService.delete(1L);

        // then
        assertThat(emailTracking.isDeleted()).isEqualTo(true);
    }

    /**
     * update an email tracking reminder is sent
     */
    @Test
    public void testUpdateAnEmailTrackingReminderIsSent() {
        // given
        Instant now = Instant.now();
        given(userACL.isAdmin()).willReturn(true);
        EmailTrackingDataDTO emailTrackingDTO = new EmailTrackingDataDTO();
        emailTrackingDTO.setId(1L);
        emailTrackingDTO.setStatus(EmailStatus.APPROVED);
        emailTrackingDTO.setReminderDate1(now.plusSeconds(10 * 86400));
        emailTrackingDTO.setReminderDate2(now.plusSeconds(20 * 86400));

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setDeleted(false);
        emailTracking.setSentReminder1(true);
        emailTracking.setReminderDate1(now.plusSeconds(20 * 86400));

        // when
        given(emailTrackingRepository.findByIdAndDeletedIsFalse(eq(1L))).willReturn(Optional.of(emailTracking));
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTrackingService.updateEmailTracking(emailTrackingDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.reminderDate1WasSent");
    }

    /*
     * auto send reminder 1 successfully
     */
    @Test
    public void testAutoSendingReminderEmailOne() {
        // given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        // an email tracking running to the reminder date time
        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setSentReminder1(false);
        emailTracking.setReminderDate1(Instant.now());
        emailTracking.setRecipient(new Recipient());
        emailTracking.setCronJob(cronJob);

        EmailTemplate mainEmail = new EmailTemplate();
        mainEmail.setId(1L);
        mainEmail.setType(EmailTemplateType.MAIN);
        mainEmail.setContent("Main content");

        given(emailTemplateRepository.findReminderTemplateOfCronJob(eq(EmailTemplateType.MAIN), eq(1L))).willReturn(Collections.singletonList(mainEmail));
        given(emailTrackingRepository.findAllEmailTrackingHavingReminder(eq(EmailStatus.SENT), eq(EmailStatus.INCOMPLETE), any(Instant.class))).willReturn(Collections.singletonList(emailTracking));
        given(emailTemplateRepository.findReminderTemplateOfCronJob(eq(EmailTemplateType.REMINDER_1), eq(1L))).willReturn(Collections.singletonList(new EmailTemplate()));

        // when
        emailTrackingService.autoSendingReminderEmail();

        // then
        // call to send email service
        then(applicationEventPublisher).should(times(1)).publishEvent(any(AutoSendEmailEvent.class));
        // update sent reminder
        assertThat(emailTracking.isSentReminder1()).isEqualTo(true);
    }

    /*
     * auto send reminder 1 successfully and update CronjobStatistic
     */
    @Test
    public void testAutoSendingReminderEmailOneAndUpdateCronjobStatistic() {
        // given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setId(1L);
        cronJobStatistic.setCronJob(cronJob);
        cronJobStatistic.setReminderSent1(10);

        // an email tracking running to the reminder date time
        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setSentReminder1(false);
        emailTracking.setReminderDate1(Instant.now());
        emailTracking.setRecipient(new Recipient());
        emailTracking.setCronJob(cronJob);
        EmailTemplate mainEmail = new EmailTemplate();
        mainEmail.setId(1L);
        mainEmail.setType(EmailTemplateType.MAIN);
        mainEmail.setContent("Main content");

        given(emailTemplateRepository.findReminderTemplateOfCronJob(eq(EmailTemplateType.MAIN), eq(1L))).willReturn(Collections.singletonList(mainEmail));
        given(emailTrackingRepository.findAllEmailTrackingHavingReminder(eq(EmailStatus.SENT), eq(EmailStatus.INCOMPLETE), any(Instant.class))).willReturn(Collections.singletonList(emailTracking));
        given(emailTemplateRepository.findReminderTemplateOfCronJob(eq(EmailTemplateType.REMINDER_1), eq(1L))).willReturn(Collections.singletonList(new EmailTemplate()));
        given(cronJobStatisticRepository.findByCronJobId(1L)).willReturn(Optional.of(cronJobStatistic));
        // when
        emailTrackingService.autoSendingReminderEmail();

        // then
        // call to send email service
        then(applicationEventPublisher).should(times(1)).publishEvent(any(AutoSendEmailEvent.class));
        then(cronJobStatisticRepository).should(times(1)).save(any(CronJobStatistic.class));

        // update sent reminder
        assertThat(emailTracking.isSentReminder1()).isEqualTo(true);
        assertThat(cronJobStatistic.getReminderSent1()).isEqualTo(11);
    }

    /**
     * auto send reminder 2 successfully and update CronjobStatistic
     */
    @Test
    public void testAutoSendingReminderEmailTwoAndUpdateCronjobStatistic() {
        // given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setId(1L);
        cronJobStatistic.setCronJob(cronJob);
        cronJobStatistic.setReminderSent2(11);

        // an email tracking running to the reminder date time
        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setSentReminder1(true);
        emailTracking.setSentReminder2(false);
        emailTracking.setReminderDate2(Instant.now());
        emailTracking.setRecipient(new Recipient());
        emailTracking.setCronJob(cronJob);
        EmailTemplate mainEmail = new EmailTemplate();
        mainEmail.setId(1L);
        mainEmail.setType(EmailTemplateType.MAIN);
        mainEmail.setContent("Main content");

        given(emailTemplateRepository.findReminderTemplateOfCronJob(eq(EmailTemplateType.MAIN), eq(1L))).willReturn(Collections.singletonList(mainEmail));
        given(emailTrackingRepository.findAllEmailTrackingHavingReminder(eq(EmailStatus.SENT), eq(EmailStatus.INCOMPLETE), any(Instant.class))).willReturn(Collections.singletonList(emailTracking));
        given(emailTemplateRepository.findReminderTemplateOfCronJob(eq(EmailTemplateType.REMINDER_2), eq(1L))).willReturn(Collections.singletonList(new EmailTemplate()));
        given(emailTemplateRepository.findReminderTemplateOfCronJob(eq(EmailTemplateType.REMINDER_1), eq(1L))).willReturn(Collections.singletonList(new EmailTemplate()));
        given(cronJobStatisticRepository.findByCronJobId(1L)).willReturn(Optional.of(cronJobStatistic));

        // when
        emailTrackingService.autoSendingReminderEmail();

        // then
        // call to send email service
        then(applicationEventPublisher).should(times(1)).publishEvent(any(AutoSendEmailEvent.class));
        then(cronJobStatisticRepository).should(times(1)).save(any(CronJobStatistic.class));

        // update sent reminder
        assertThat(emailTracking.isSentReminder1()).isEqualTo(true);
        assertThat(cronJobStatistic.getReminderSent2()).isEqualTo(12);
    }

    /**
     * auto send reminder 2 successfully
     */
    @Test
    public void testAutoSendingReminderEmailTwo() {
        // given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        // an email tracking running to the reminder date time
        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setSentReminder1(true);
        emailTracking.setSentReminder2(false);
        emailTracking.setReminderDate2(Instant.now());
        emailTracking.setRecipient(new Recipient());
        emailTracking.setCronJob(cronJob);
        EmailTemplate mainEmail = new EmailTemplate();
        mainEmail.setId(1L);
        mainEmail.setType(EmailTemplateType.MAIN);
        mainEmail.setContent("Main content");

        given(emailTemplateRepository.findReminderTemplateOfCronJob(eq(EmailTemplateType.MAIN), eq(1L))).willReturn(Collections.singletonList(mainEmail));
        given(emailTrackingRepository.findAllEmailTrackingHavingReminder(eq(EmailStatus.SENT), eq(EmailStatus.INCOMPLETE), any(Instant.class))).willReturn(Collections.singletonList(emailTracking));
        given(emailTemplateRepository.findReminderTemplateOfCronJob(eq(EmailTemplateType.REMINDER_2), eq(1L))).willReturn(Collections.singletonList(new EmailTemplate()));
        given(emailTemplateRepository.findReminderTemplateOfCronJob(eq(EmailTemplateType.REMINDER_1), eq(1L))).willReturn(Collections.singletonList(new EmailTemplate()));

        // when
        emailTrackingService.autoSendingReminderEmail();

        // then
        // call to send email service
        then(applicationEventPublisher).should(times(1)).publishEvent(any(AutoSendEmailEvent.class));

        // update sent reminder
        assertThat(emailTracking.isSentReminder1()).isEqualTo(true);
    }

    /**
     * resend from non permission user
     */
    @Test
    public void testResendEmailTrackingCaseNonPermissionUser() {
        // given
        ResendEmailTrackingDTO resendEmailTrackingDTO = new ResendEmailTrackingDTO();
        resendEmailTrackingDTO.setEmail("email@yahoo.com");
        resendEmailTrackingDTO.setRegistrar("registrar@yahoo.com");
        resendEmailTrackingDTO.setId(1L);
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> emailTrackingService.resendEmailTracking(resendEmailTrackingDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * resend email tracking not found
     */
    @Test
    public void testResendEmailTrackingNotFound() {
        // given
        ResendEmailTrackingDTO resendEmailTrackingDTO = new ResendEmailTrackingDTO();
        resendEmailTrackingDTO.setEmail("email@yahoo.com");
        resendEmailTrackingDTO.setRegistrar("registrar@yahoo.com");
        resendEmailTrackingDTO.setId(1L);
        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.empty());

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> emailTrackingService.resendEmailTracking(resendEmailTrackingDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    /**
     * resend email tracking invalid status
     */
    @Test
    public void testResendEmailTrackingInvalidStatus() {
        // given
        Instant now = Instant.now();
        ResendEmailTrackingDTO resendEmailTrackingDTO = new ResendEmailTrackingDTO();
        resendEmailTrackingDTO.setEmail("email@yahoo.com");
        resendEmailTrackingDTO.setRegistrar("registrar@yahoo.com");
        resendEmailTrackingDTO.setId(1L);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setReminderDate1(now.plusSeconds(7 * 86400));
        emailTracking.setReminderDate2(now.plusSeconds(14 * 86400));
        emailTracking.setStatus(EmailStatus.SENT);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.of(emailTracking));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTrackingService.resendEmailTracking(resendEmailTrackingDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.resendError");
    }

    /**
     * resend email tracking successfully
     */
    @Test
    public void testResendEmailTrackingSuccessfully() {
        // given
        Instant now = Instant.now();
        ResendEmailTrackingDTO resendEmailTrackingDTO = new ResendEmailTrackingDTO();
        resendEmailTrackingDTO.setEmail("email@yahoo.com");
        resendEmailTrackingDTO.setRegistrar("registrar@yahoo.com");
        resendEmailTrackingDTO.setId(1L);

        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        Recipient recipient = new Recipient();
        recipient.setId(1L);
        recipient.setEmail("recipient@com.vn");
        recipient.setRegistrar("recipientcc@com.vn");

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setReminderDate1(now.plusSeconds(7 * 86400));
        emailTracking.setReminderDate2(now.plusSeconds(14 * 86400));
        emailTracking.setStatus(EmailStatus.COULD_NOT_BE_SENT);
        emailTracking.setCronJob(cronJob);
        emailTracking.setRecipient(recipient);

        EmailTemplate emailTemplate = new EmailTemplate();
        emailTemplate.setType(EmailTemplateType.MAIN);
        emailTemplate.setId(1L);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.of(emailTracking));
        given(emailTemplateRepository.findMainTemplateOfCronJob(EmailTemplateType.MAIN, 1L)).willReturn(Collections.singletonList(emailTemplate));

        // when
        emailTrackingService.resendEmailTracking(resendEmailTrackingDTO);

        // then
        then(applicationEventPublisher).should(times(1)).publishEvent(any(AutoSendEmailEvent.class));
        then(emailTrackingRepository).should(times(1)).save(any(EmailTracking.class));
    }

    /**
     * resend email tracking with invalid registar
     */
    @Test
    public void testResendEmailTrackingWithInvalidRegistrar() {
        // given
        Instant now = Instant.now();
        ResendEmailTrackingDTO resendEmailTrackingDTO = new ResendEmailTrackingDTO();
        resendEmailTrackingDTO.setEmail("email@yahoo.com");
        resendEmailTrackingDTO.setRegistrar("registrar@yahoo");
        resendEmailTrackingDTO.setId(1L);

        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        Recipient recipient = new Recipient();
        recipient.setId(1L);
        recipient.setEmail("recipient@com.vn");
        recipient.setRegistrar("recipientcc@com.vn");

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setReminderDate1(now.plusSeconds(7 * 86400));
        emailTracking.setReminderDate2(now.plusSeconds(14 * 86400));
        emailTracking.setStatus(EmailStatus.COULD_NOT_BE_SENT);
        emailTracking.setCronJob(cronJob);
        emailTracking.setRecipient(recipient);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.of(emailTracking));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTrackingService.resendEmailTracking(resendEmailTrackingDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.registrarFormatEmailInvalid");
    }

    /**
     * resend email template MAIN not found
     */
    @Test
    public void testResendEmailTemplateMainNotFound() {
        // given
        Instant now = Instant.now();
        ResendEmailTrackingDTO resendEmailTrackingDTO = new ResendEmailTrackingDTO();
        resendEmailTrackingDTO.setEmail("email@yahoo.com");
        resendEmailTrackingDTO.setRegistrar("registrar@yahoo.com");
        resendEmailTrackingDTO.setId(1L);

        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        Recipient recipient = new Recipient();
        recipient.setId(1L);
        recipient.setEmail("recipient@com.vn");
        recipient.setRegistrar("recipientcc@com.vn");

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setReminderDate1(now.plusSeconds(7 * 86400));
        emailTracking.setReminderDate2(now.plusSeconds(14 * 86400));
        emailTracking.setStatus(EmailStatus.COULD_NOT_BE_SENT);
        emailTracking.setCronJob(cronJob);
        emailTracking.setRecipient(recipient);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.of(emailTracking));
        given(emailTemplateRepository.findMainTemplateOfCronJob(EmailTemplateType.MAIN, 1L)).willReturn(new ArrayList<>());

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> emailTrackingService.resendEmailTracking(resendEmailTrackingDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.emailTemplateMainNotFound");
    }

    /**
     * update status from non permission user
     */
    @Test
    public void testUpdateStatusEmailTrackingCaseNonPermissionUser() {
        // given
        List<Long> ids = Arrays.asList(1L, 2L);
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> emailTrackingService.updateStatusForEmailTracking(ids, EmailStatus.APPROVED));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * update status successfully
     */
    @Test
    public void testUpdateStatusEmailTrackingSuccessfully() {
        // given
        List<Long> ids = Arrays.asList(1L, 2L);
        List<EmailTracking> emailTrackingList = new ArrayList<>();
        EmailTracking emailTracking1 = new EmailTracking();
        emailTracking1.setId(1L);
        emailTracking1.setStatus(EmailStatus.SENT);
        EmailTracking emailTracking2 = new EmailTracking();
        emailTracking2.setId(2L);
        emailTracking2.setStatus(EmailStatus.REJECTED);
        emailTrackingList.add(emailTracking1);
        emailTrackingList.add(emailTracking2);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findAllByIdInAndDeletedIsFalse(ids)).willReturn(emailTrackingList);

        // when
        emailTrackingService.updateStatusForEmailTracking(ids, EmailStatus.APPROVED);

        // then
        assertThat(emailTracking1.getStatus()).isEqualTo(EmailStatus.APPROVED);
        assertThat(emailTracking2.getStatus()).isEqualTo(EmailStatus.APPROVED);
        then(emailTrackingRepository).should(times(1)).saveAll(anyList());
        then(cronJobStatisticRepository).should(times(0)).save(any(CronJobStatistic.class));
    }

    /**
     * update status successfully and update cronjob statistic
     */
    @Test
    public void testUpdateStatusEmailTrackingSuccessfullyAndUpdateCronjobStatistic() {
        // given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setId(1L);
        cronJobStatistic.setCronJob(cronJob);
        List<Long> ids = Arrays.asList(1L, 2L);
        List<EmailTracking> emailTrackingList = new ArrayList<>();
        EmailTracking emailTracking1 = new EmailTracking();
        emailTracking1.setId(1L);
        emailTracking1.setStatus(EmailStatus.SENT);
        emailTracking1.setCronJob(cronJob);
        EmailTracking emailTracking2 = new EmailTracking();
        emailTracking2.setId(2L);
        emailTracking2.setStatus(EmailStatus.REJECTED);
        emailTracking2.setCronJob(cronJob);
        emailTrackingList.add(emailTracking1);
        emailTrackingList.add(emailTracking2);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findAllByIdInAndDeletedIsFalse(ids)).willReturn(emailTrackingList);
        given(cronJobStatisticRepository.findByCronJobId(1L)).willReturn(Optional.of(cronJobStatistic));

        // when
        emailTrackingService.updateStatusForEmailTracking(ids, EmailStatus.UNANSWERED);

        // then
        assertThat(emailTracking1.getStatus()).isEqualTo(EmailStatus.UNANSWERED);
        assertThat(emailTracking2.getStatus()).isEqualTo(EmailStatus.UNANSWERED);
        then(emailTrackingRepository).should(times(1)).saveAll(anyList());
        then(cronJobStatisticRepository).should(times(2)).save(any(CronJobStatistic.class));
    }

    /**
     * update status successfully and update cronjob statistic subtract unAnswer
     */
    @Test
    public void testUpdateStatusEmailTrackingSuccessfullyAndUpdateCronjobStatisticSubtractUnAnswer() {
        // given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setId(1L);
        cronJobStatistic.setCronJob(cronJob);
        List<Long> ids = Arrays.asList(1L, 2L);
        List<EmailTracking> emailTrackingList = new ArrayList<>();
        EmailTracking emailTracking1 = new EmailTracking();
        emailTracking1.setId(1L);
        emailTracking1.setStatus(EmailStatus.UNANSWERED);
        emailTracking1.setCronJob(cronJob);
        EmailTracking emailTracking2 = new EmailTracking();
        emailTracking2.setId(2L);
        emailTracking2.setStatus(EmailStatus.UNANSWERED);
        emailTracking2.setCronJob(cronJob);
        emailTrackingList.add(emailTracking1);
        emailTrackingList.add(emailTracking2);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findAllByIdInAndDeletedIsFalse(ids)).willReturn(emailTrackingList);
        given(cronJobStatisticRepository.findByCronJobId(1L)).willReturn(Optional.of(cronJobStatistic));

        // when
        emailTrackingService.updateStatusForEmailTracking(ids, EmailStatus.SENT);

        // then
        assertThat(emailTracking1.getStatus()).isEqualTo(EmailStatus.SENT);
        assertThat(emailTracking2.getStatus()).isEqualTo(EmailStatus.SENT);
        then(emailTrackingRepository).should(times(1)).saveAll(anyList());
        then(cronJobStatisticRepository).should(times(2)).save(any(CronJobStatistic.class));
    }

    /**
     * update an email tracking failed by reminder date 1 or 2 invalid
     */
    @Test
    public void testUpdateAnEmailTrackingInvalidReminderDate() {
        // given
        given(userACL.isAdmin()).willReturn(true);
        Instant now = Instant.now();
        EmailTrackingDataDTO emailTrackingDTO = new EmailTrackingDataDTO();
        emailTrackingDTO.setId(1L);
        emailTrackingDTO.setStatus(EmailStatus.APPROVED);
        emailTrackingDTO.setReminderDate1(now);
        emailTrackingDTO.setReminderDate2(now.plusSeconds(20 * 86400));

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setReminderDate1(now.plusSeconds(7 * 86400));
        emailTracking.setReminderDate2(now.plusSeconds(14 * 86400));
        emailTracking.setStatus(EmailStatus.SENT);

        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.of(emailTracking));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTrackingService.updateEmailTracking(emailTrackingDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.templateReminderMustBeGreaterThanCurrentDay");
    }

    /**
     * update an email tracking failed by reminder date 2 less than reminder date 1
     */
    @Test
    public void testUpdateAnEmailTrackingReminderDate2LessThan1() {
        // given
        given(userACL.isAdmin()).willReturn(true);
        Instant now = Instant.now();
        EmailTrackingDataDTO emailTrackingDTO = new EmailTrackingDataDTO();
        emailTrackingDTO.setId(1L);
        emailTrackingDTO.setStatus(EmailStatus.APPROVED);
        emailTrackingDTO.setReminderDate1(now.plusSeconds(10 * 86400));
        emailTrackingDTO.setReminderDate2(now.plusSeconds(9 * 86400));

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setReminderDate1(now.plusSeconds(7 * 86400));
        emailTracking.setReminderDate2(now.plusSeconds(14 * 86400));
        emailTracking.setStatus(EmailStatus.SENT);

        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.of(emailTracking));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTrackingService.updateEmailTracking(emailTrackingDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.templateReminder2MustBeGreaterThan1");
    }

    /**
     * update cronjob statistic when update an email tracking to UNANSWERED status
     */
    @Test
    public void testUpdateCronjobStatisticWhenUpdateToUnAnsweredStatus() {
        // give
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setCronJob(cronJob);
        cronJobStatistic.setUnansweredEmail(10);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setStatus(EmailStatus.SENT);
        emailTracking.setCronJob(cronJob);

        given(cronJobStatisticRepository.findByCronJobId(1L)).willReturn(Optional.of(cronJobStatistic));

        // when
        emailTrackingService.updateCronjobStatistic(emailTracking, EmailStatus.UNANSWERED);

        // then
        then(cronJobStatisticRepository).should(times(1)).save(any(CronJobStatistic.class));
        assertThat(cronJobStatistic.getUnansweredEmail()).isEqualTo(11);
    }

    /**
     * update cronjob statistic when remove an email tracking from UNANSWERED status
     */
    @Test
    public void testUpdateCronjobStatisticWhenRemoveFromUnAnsweredStatus() {
        // give
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setCronJob(cronJob);
        cronJobStatistic.setUnansweredEmail(10);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setStatus(EmailStatus.UNANSWERED);
        emailTracking.setCronJob(cronJob);

        given(cronJobStatisticRepository.findByCronJobId(1L)).willReturn(Optional.of(cronJobStatistic));

        // when
        emailTrackingService.updateCronjobStatistic(emailTracking, EmailStatus.SENT);

        // then
        then(cronJobStatisticRepository).should(times(1)).save(any(CronJobStatistic.class));
        assertThat(cronJobStatistic.getUnansweredEmail()).isEqualTo(9);
    }

    /**
     * update comment of email tracking successfully
     */
    @Test
    public void testUpdateCommentEmailTrackingSuccessfully() {
        // give
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setCronJob(cronJob);
        cronJobStatistic.setUnansweredEmail(10);

        EmailTrackingDataDTO emailTrackingDTO = new EmailTrackingDataDTO();
        emailTrackingDTO.setId(1L);
        emailTrackingDTO.setStatus(EmailStatus.APPROVED);
        emailTrackingDTO.setComment("New comment");

        Recipient recipient = new Recipient();
        recipient.setId(1L);
        recipient.setRegistrar("johndoe@gmail.com");
        recipient.setEmail("johndoe@gmail.com");
        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setStatus(EmailStatus.UNANSWERED);
        emailTracking.setCronJob(cronJob);
        emailTracking.setComment("Comment");
        emailTracking.setRecipient(recipient);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.of(emailTracking));

        // when
        emailTrackingService.updateEmailTracking(emailTrackingDTO);

        // then
        assertThat(emailTracking.getComment()).isEqualTo("New comment");
    }

    /**
     * update comment failed with all spaces
     */
    @Test
    public void testUpdateCommentEmailTrackingFailed() {
        // give
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setCronJob(cronJob);
        cronJobStatistic.setUnansweredEmail(10);

        EmailTrackingDataDTO emailTrackingDTO = new EmailTrackingDataDTO();
        emailTrackingDTO.setId(1L);
        emailTrackingDTO.setStatus(EmailStatus.APPROVED);
        emailTrackingDTO.setComment("  ");

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setStatus(EmailStatus.UNANSWERED);
        emailTracking.setCronJob(cronJob);
        emailTracking.setComment("Comment");

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.of(emailTracking));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTrackingService.updateEmailTracking(emailTrackingDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.noWhiteSpace");
    }

    /**
     * update email tracking failed with email null
     */
    @Test
    public void testUpdateEmailTrackingWithCaseEmailNull() {
        // give
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);

        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setCronJob(cronJob);
        cronJobStatistic.setUnansweredEmail(10);

        Recipient recipient = new Recipient();

        EmailTrackingDataDTO emailTrackingDTO = new EmailTrackingDataDTO();
        emailTrackingDTO.setId(1L);
        emailTrackingDTO.setStatus(EmailStatus.APPROVED);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setStatus(EmailStatus.UNANSWERED);
        emailTracking.setRecipient(recipient);
        emailTracking.setCronJob(cronJob);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.of(emailTracking));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTrackingService.updateEmailTracking(emailTrackingDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.registrarCanNotNullWhenEmailNull");
    }

    /**
     * Archive With not admin
     */
    @Test
    public void testArchiveWithNotAdmin(){
        //given
        given(userACL.isAdmin()).willReturn(false);

        //when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> emailTrackingService.archive(1));

        //then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * Archive With not found
     */
    @Test
    public void testArchiveWithNotFound(){
        //given
        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findById(1L)).willReturn(Optional.empty());

        //when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> emailTrackingService.archive(1));

        //then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    /**
     * Archive With email tracking archived
     */
    @Test
    public void testArchiveWithArchived(){
        //given
        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setArchived(true);
        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findById(1L)).willReturn(Optional.of(emailTracking));

        //when
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTrackingService.archive(1));

        //then
        assertThat(result.getMessage()).isEqualTo("error.emailTrackingArchived");
    }

    /**
     * Archive With successful
     */
    @Test
    public void testArchiveSuccessful(){
        //given
        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setArchived(false);
        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findById(1L)).willReturn(Optional.of(emailTracking));

        //when
        emailTrackingService.archive(1);

        //then
        assertThat(emailTracking.isArchived()).isTrue();
    }

    /**
     * Restore With not admin
     */
    @Test
    public void testRestoreWithNotAdmin(){
        //given
        given(userACL.isAdmin()).willReturn(false);

        //when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> emailTrackingService.restore(1));

        //then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * Restore With not found
     */
    @Test
    public void testRestoreWithNotFound(){
        //given
        given(userACL.isAdmin()).willReturn(true);

        //when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> emailTrackingService.restore(1));

        //then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    /**
     * Restore With reminder 1 not sent and time in the pass
     */
    @Test
    public void testRestoreWithReminder1NotSentAndInThePass(){
        //given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setSendHour(4);
        cronJob.setTemplateReminderTime1(7);
        cronJob.setTemplateReminderTime2(14);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setCronJob(cronJob);
        emailTracking.setSentReminder1(false);
        emailTracking.setReminderDate1(LocalDateTime.of(2021, 10, 9, 14, 0, 0, 0).toInstant(ZoneOffset.UTC));
        emailTracking.setSentReminder2(false);
        emailTracking.setArchived(true);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findById(1L)).willReturn(Optional.of(emailTracking));
        Instant reminder1 = LocalDateTime.ofInstant(Instant.now().truncatedTo(ChronoUnit.HOURS).plus(cronJob.getTemplateReminderTime1(), ChronoUnit.DAYS), ZoneId.of("UTC")).with(ChronoField.HOUR_OF_DAY, cronJob.getSendHour()).toInstant(ZoneOffset.UTC);
        Instant reminder2 = LocalDateTime.ofInstant(Instant.now().truncatedTo(ChronoUnit.HOURS).plus(cronJob.getTemplateReminderTime2(), ChronoUnit.DAYS), ZoneId.of("UTC")).with(ChronoField.HOUR_OF_DAY, cronJob.getSendHour()).toInstant(ZoneOffset.UTC);

        //when
        emailTrackingService.restore(1);

        //then
        assertThat(emailTracking.getReminderDate1()).isEqualTo(reminder1);
        assertThat(emailTracking.getReminderDate2()).isEqualTo(reminder2);
    }

    /**
     * Restore With reminder 1 not sent and in the future
     */
    @Test
    public void testRestoreWithReminder1NotSentAndInTheFuture(){
        //given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setSendHour(4);
        cronJob.setTemplateReminderTime1(7);
        cronJob.setTemplateReminderTime2(14);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setCronJob(cronJob);
        emailTracking.setSentReminder1(false);
        emailTracking.setReminderDate1(Instant.now().truncatedTo(ChronoUnit.DAYS).plus(4, ChronoUnit.HOURS).plus(1, ChronoUnit.DAYS));
        emailTracking.setReminderDate2(Instant.now().truncatedTo(ChronoUnit.DAYS).plus(4, ChronoUnit.HOURS).plus(7, ChronoUnit.DAYS));
        emailTracking.setSentReminder2(false);
        emailTracking.setArchived(true);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findById(1L)).willReturn(Optional.of(emailTracking));
        Instant reminder1 = LocalDateTime.ofInstant(Instant.now().truncatedTo(ChronoUnit.DAYS).plus(4, ChronoUnit.HOURS).plus(1, ChronoUnit.DAYS), ZoneId.of("UTC")).with(ChronoField.HOUR_OF_DAY, cronJob.getSendHour()).toInstant(ZoneOffset.UTC);
        Instant reminder2 = LocalDateTime.ofInstant(Instant.now().truncatedTo(ChronoUnit.DAYS).plus(4, ChronoUnit.HOURS).plus(7, ChronoUnit.DAYS), ZoneId.of("UTC")).with(ChronoField.HOUR_OF_DAY, cronJob.getSendHour()).toInstant(ZoneOffset.UTC);

        //when
        emailTrackingService.restore(1);

        //then
        assertThat(emailTracking.getReminderDate1()).isEqualTo(reminder1);
        assertThat(emailTracking.getReminderDate2()).isEqualTo(reminder2);
    }

    /**
     * Restore With reminder 2 not sent and in the pass
     */
    @Test
    public void testRestoreWithReminder2NotSentAndInThePass(){
        //given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setSendHour(4);
        cronJob.setTemplateReminderTime1(7);
        cronJob.setTemplateReminderTime2(14);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setCronJob(cronJob);
        emailTracking.setSentReminder1(true);
        emailTracking.setSentReminder2(false);
        emailTracking.setReminderDate2(LocalDateTime.of(2021, 10, 9, 14, 0, 0, 0).toInstant(ZoneOffset.UTC));
        emailTracking.setArchived(true);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findById(1L)).willReturn(Optional.of(emailTracking));
        Instant reminder2 = LocalDateTime.ofInstant(Instant.now().truncatedTo(ChronoUnit.HOURS).plus(cronJob.getTemplateReminderTime2(), ChronoUnit.DAYS), ZoneId.of("UTC")).with(ChronoField.HOUR_OF_DAY, cronJob.getSendHour()).toInstant(ZoneOffset.UTC);

        //when
        emailTrackingService.restore(1);

        //then
        assertThat(emailTracking.getReminderDate2()).isEqualTo(reminder2);
    }

    /**
     * Restore With reminder 2 not sent and in the future
     */
    @Test
    public void testRestoreWithReminder2NotSentAndInTheFuture(){
        //given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setSendHour(4);
        cronJob.setTemplateReminderTime1(7);
        cronJob.setTemplateReminderTime2(14);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setCronJob(cronJob);
        emailTracking.setSentReminder1(true);
        emailTracking.setSentReminder2(false);
        emailTracking.setReminderDate2(Instant.now().truncatedTo(ChronoUnit.DAYS).plus(cronJob.getSendHour(), ChronoUnit.HOURS).plus(7, ChronoUnit.DAYS));
        emailTracking.setArchived(true);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findById(1L)).willReturn(Optional.of(emailTracking));
        Instant reminder2 = LocalDateTime.ofInstant(Instant.now().truncatedTo(ChronoUnit.DAYS).plus(cronJob.getSendHour(), ChronoUnit.HOURS).plus(7, ChronoUnit.DAYS), ZoneId.of("UTC")).toInstant(ZoneOffset.UTC);

        //when
        emailTrackingService.restore(1);

        //then
        assertThat(emailTracking.getReminderDate2()).isEqualTo(reminder2);
    }

    /**
     * Restore With both reminder sent
     */
    @Test
    public void testRestoreWithSent(){
        //given
        Instant now = Instant.now();

        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setSendHour(4);
        cronJob.setTemplateReminderTime1(7);
        cronJob.setTemplateReminderTime2(14);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setCronJob(cronJob);
        emailTracking.setSentReminder1(true);
        emailTracking.setReminderDate1(now.minusSeconds(84600));
        emailTracking.setSentReminder2(true);
        emailTracking.setReminderDate2(now.minusSeconds(7 * 84600));
        emailTracking.setArchived(true);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findById(1L)).willReturn(Optional.of(emailTracking));

        //when
        emailTrackingService.restore(1);

        //then
        then(emailTrackingRepository).should(times(1)).save(any());
        assertThat(emailTracking.isArchived()).isFalse();
        assertThat(emailTracking.getReminderDate1()).isEqualTo(now.minusSeconds(84600));
        assertThat(emailTracking.getReminderDate2()).isEqualTo(now.minusSeconds(7 * 84600));
    }


    @Test
    public void testRestoreWithNotArchive(){
        //given
        CronJob cronJob = new CronJob();
        cronJob.setId(1L);
        cronJob.setSendHour(4);
        cronJob.setTemplateReminderTime1(7);
        cronJob.setTemplateReminderTime2(14);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setCronJob(cronJob);
        emailTracking.setArchived(false);
        emailTracking.setSentReminder1(true);
        emailTracking.setSentReminder2(true);

        given(userACL.isAdmin()).willReturn(true);
        given(emailTrackingRepository.findById(1L)).willReturn(Optional.of(emailTracking));

        //when
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTrackingService.restore(1));

        //then
        assertThat(result.getMessage()).isEqualTo("error.emailTrackingNotArchived");
    }

}
