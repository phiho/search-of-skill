package se.searchofskills.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.SearchOfSkillsApp;
import se.searchofskills.domain.*;
import se.searchofskills.domain.enumeration.*;

import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for {@link EmailTrackingRepository}.
 */
@SpringBootTest(classes = SearchOfSkillsApp.class)
@Transactional
public class EmailTemplateRepositoryIT {

    @Autowired
    private EmailTemplateRepository emailTemplateRepository;

    @Autowired
    private CronJobEmailTemplateRepository cronJobEmailTemplateRepository;

    @Autowired
    private CronJobRepository cronJobRepository;

    @Autowired
    private RecipientListRepository recipientListRepository;

    @BeforeEach
    public void setup() {
        RecipientList recipientList = new RecipientList();
        recipientList.setName("Recipient List");
        recipientList = recipientListRepository.save(recipientList);

        CronJob cronJob = new CronJob();
        cronJob.setName("Cronjob");
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setStartDate(Instant.now());
        cronJob.setEndDate(Instant.now());
        cronJob.setChunkNumber(10);
        cronJob.setRecipientList(recipientList);
        cronJob.setWeekOfMonth(1);
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setSendHour(1);
        cronJob.setWeekday(WeekDay.MON);
        cronJob = cronJobRepository.save(cronJob);

        EmailTemplate emailTemplate1 = new EmailTemplate();
        emailTemplate1.setName("Name of reminder template 1");
        emailTemplate1.setType(EmailTemplateType.REMINDER_1);
        emailTemplate1.setContent("Content reminder template 1");
        emailTemplate1.setDeleted(false);

        EmailTemplate emailTemplate2 = new EmailTemplate();
        emailTemplate2.setName("Name of reminder template 2");
        emailTemplate2.setType(EmailTemplateType.REMINDER_2);
        emailTemplate2.setContent("Content reminder template 2");
        emailTemplate2.setDeleted(false);

        emailTemplate1 = emailTemplateRepository.save(emailTemplate1);
        emailTemplate2 = emailTemplateRepository.save(emailTemplate2);

        CronJobEmailTemplate cronJobEmailTemplate1 = new CronJobEmailTemplate();
        cronJobEmailTemplate1.setEmailTemplate(emailTemplate1);
        cronJobEmailTemplate1.setCronJob(cronJob);
        cronJobEmailTemplate1.setType(EmailTemplateType.REMINDER_1);

        CronJobEmailTemplate cronJobEmailTemplate2 = new CronJobEmailTemplate();
        cronJobEmailTemplate2.setEmailTemplate(emailTemplate2);
        cronJobEmailTemplate2.setCronJob(cronJob);
        cronJobEmailTemplate2.setType(EmailTemplateType.REMINDER_2);

        cronJobEmailTemplateRepository.save(cronJobEmailTemplate1);
        cronJobEmailTemplateRepository.save(cronJobEmailTemplate2);

    }

    /**
     * test find reminder template of cronjob
     */
    @Test
    public void testFindReminderTemplateOfCronJob() {
        // when
        List<EmailTemplate> reminder1EmailTemplates = emailTemplateRepository.findReminderTemplateOfCronJob(EmailTemplateType.REMINDER_1, 1L);
        List<EmailTemplate> reminder2EmailTemplates = emailTemplateRepository.findReminderTemplateOfCronJob(EmailTemplateType.REMINDER_2, 1L);

        // then
        assertThat(reminder1EmailTemplates.size()).isEqualTo(1);
        assertThat(reminder1EmailTemplates.get(0).getType()).isEqualTo(EmailTemplateType.REMINDER_1);
        assertThat(reminder1EmailTemplates.get(0).getName()).isEqualTo("Name of reminder template 1");
        assertThat(reminder1EmailTemplates.get(0).getContent()).isEqualTo("Content reminder template 1");

        assertThat(reminder2EmailTemplates.size()).isEqualTo(1);
        assertThat(reminder2EmailTemplates.get(0).getType()).isEqualTo(EmailTemplateType.REMINDER_2);
        assertThat(reminder2EmailTemplates.get(0).getName()).isEqualTo("Name of reminder template 2");
        assertThat(reminder2EmailTemplates.get(0).getContent()).isEqualTo("Content reminder template 2");
    }

}
