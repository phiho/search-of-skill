package se.searchofskills.domain;

import se.searchofskills.domain.enumeration.EmailTemplateType;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cronjob_email_template")
public class CronJobEmailTemplate implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cronjob_id")
    private CronJob cronJob;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "email_template_id")
    private EmailTemplate emailTemplate;

    @Enumerated(EnumType.STRING)
    private EmailTemplateType type; // MAIN, REMINDER_1 ,REMINDER_2

    public CronJobEmailTemplate() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CronJob getCronJob() {
        return cronJob;
    }

    public void setCronJob(CronJob cronJob) {
        this.cronJob = cronJob;
    }

    public EmailTemplate getEmailTemplate() {
        return emailTemplate;
    }

    public void setEmailTemplate(EmailTemplate emailTemplate) {
        this.emailTemplate = emailTemplate;
    }

    public EmailTemplateType getType() {
        return type;
    }

    public void setType(EmailTemplateType type) {
        this.type = type;
    }
}
