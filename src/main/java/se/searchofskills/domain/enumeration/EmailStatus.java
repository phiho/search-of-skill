package se.searchofskills.domain.enumeration;

public enum EmailStatus {
    SENT,
    REJECTED, // deprecated: no use from this comment deprecated
    APPROVED, // deprecated
    APPROVED_BUT_DONT_INCLUDE_ANY_CV,
    CAN_BE_APPROVED_BUT_PAYMENT_IS_REQUIRED, // deprecated
    UNANSWERED,
    COULD_NOT_BE_SENT,
    GOT_ALL,
    RECEIVED_WITHOUT,
    RECEIVED_WITH,
    RECEIVED_COST,
    CONFIDENTIALITY_WITHOUT,
    CONFIDENTIALITY_WITH ,
    CANCELED,
    NOT_READY_YET,
    APPEALED,
    PAYMENT,
    COMMENT,
    INCOMPLETE,
    BY_MAIL
}
