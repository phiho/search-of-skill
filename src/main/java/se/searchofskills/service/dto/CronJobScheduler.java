package se.searchofskills.service.dto;

import se.searchofskills.domain.CronJob;
import se.searchofskills.domain.enumeration.Frequency;
import se.searchofskills.domain.enumeration.WeekDay;

import java.time.Instant;

public class CronJobScheduler {
    private WeekDay weekday;
    private int sendHour;
    private Frequency frequency;
    private int weekOfMonth;
    private Instant endDate;
    private Instant nextSendDate;
    private int totalEmailToSend;
    private int emailSent;
    private int chunkNumber;

    public CronJobScheduler() {
    }

    public CronJobScheduler(CronJob cronJob) {
        this.weekday = cronJob.getWeekday();
        this.sendHour = cronJob.getSendHour();
        this.frequency = cronJob.getFrequency();
        this.weekOfMonth = cronJob.getWeekOfMonth();
        this.endDate = cronJob.getEndDate();
        this.nextSendDate = cronJob.getNextSendDate();
        this.chunkNumber = cronJob.getChunkNumber();
    }

    public int getChunkNumber() {
        return chunkNumber;
    }

    public void setChunkNumber(int chunkNumber) {
        this.chunkNumber = chunkNumber;
    }

    public int getTotalEmailToSend() {
        return totalEmailToSend;
    }

    public void setTotalEmailToSend(int totalEmailToSend) {
        this.totalEmailToSend = totalEmailToSend;
    }

    public int getEmailSent() {
        return emailSent;
    }

    public void setEmailSent(int emailSent) {
        this.emailSent = emailSent;
    }

    public WeekDay getWeekday() {
        return weekday;
    }

    public void setWeekday(WeekDay weekday) {
        this.weekday = weekday;
    }

    public int getSendHour() {
        return sendHour;
    }

    public void setSendHour(int sendHour) {
        this.sendHour = sendHour;
    }

    public Frequency getFrequency() {
        return frequency;
    }

    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
    }

    public int getWeekOfMonth() {
        return weekOfMonth;
    }

    public void setWeekOfMonth(int weekOfMonth) {
        this.weekOfMonth = weekOfMonth;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Instant getNextSendDate() {
        return nextSendDate;
    }

    public void setNextSendDate(Instant nextSendDate) {
        this.nextSendDate = nextSendDate;
    }
}
