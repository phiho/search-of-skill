package se.searchofskills.service.dto;

import se.searchofskills.domain.EmailTemplate;
import se.searchofskills.domain.enumeration.EmailTemplateType;

import javax.validation.constraints.NotBlank;

public class EmailTemplateDTO {
    private Long id;
    @NotBlank
    private String name;
    @NotBlank
    private String content;
    private EmailTemplateType type; // MAIN, REMINDER_1, REMINDER_2
    private boolean deleted;

    public EmailTemplateDTO() {
    }

    public EmailTemplateDTO(EmailTemplate emailTemplate) {
        this.id = emailTemplate.getId();
        this.name = emailTemplate.getName();
        this.content = emailTemplate.getContent();
        this.type = emailTemplate.getType();
        this.deleted = emailTemplate.isDeleted();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public EmailTemplateType getType() {
        return type;
    }

    public void setType(EmailTemplateType type) {
        this.type = type;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
