package se.searchofskills.service.dto;

import javax.validation.constraints.*;

public class ResendEmailTrackingDTO {
    @NotNull
    private Long id;

    @NotBlank
    @Email
    @Pattern(regexp = "^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$")
    @Size(min = 5, max = 254)
    private String email;

    private String registrar;

    public ResendEmailTrackingDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }
}
