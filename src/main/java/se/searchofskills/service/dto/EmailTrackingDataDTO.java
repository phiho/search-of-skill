package se.searchofskills.service.dto;

import se.searchofskills.domain.EmailTracking;
import se.searchofskills.domain.enumeration.EmailStatus;

import java.time.Instant;

public class EmailTrackingDataDTO {
    private Long id;

    private Instant reminderDate1;

    private Instant reminderDate2;

    private EmailStatus status;

    private String comment;

    private String registrar;

    public EmailTrackingDataDTO() {
    }

    public EmailTrackingDataDTO(EmailTracking emailTracking) {
        this.id = emailTracking.getId();
        this.status = emailTracking.getStatus();
        this.reminderDate1 = emailTracking.getReminderDate1();
        this.reminderDate2 = emailTracking.getReminderDate2();
        this.comment = emailTracking.getComment();
        this.registrar = emailTracking.getRegistrar();
    }

    public EmailTrackingDataDTO(EmailTracking emailTracking, String registrar) {
        this.id = emailTracking.getId();
        this.status = emailTracking.getStatus();
        this.reminderDate1 = emailTracking.getReminderDate1();
        this.reminderDate2 = emailTracking.getReminderDate2();
        this.comment = emailTracking.getComment();
        this.registrar = registrar;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getReminderDate1() {
        return reminderDate1;
    }

    public void setReminderDate1(Instant reminderDate1) {
        this.reminderDate1 = reminderDate1;
    }

    public Instant getReminderDate2() {
        return reminderDate2;
    }

    public void setReminderDate2(Instant reminderDate2) {
        this.reminderDate2 = reminderDate2;
    }

    public EmailStatus getStatus() {
        return status;
    }

    public void setStatus(EmailStatus status) {
        this.status = status;
    }

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }
}
