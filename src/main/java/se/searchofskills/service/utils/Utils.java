package se.searchofskills.service.utils;

import org.apache.commons.lang3.StringUtils;
import se.searchofskills.domain.CronJob;
import se.searchofskills.domain.enumeration.Frequency;
import se.searchofskills.domain.enumeration.WeekDay;
import se.searchofskills.service.dto.CronJobScheduler;
import se.searchofskills.service.error.ResourceNotFoundException;

import java.time.*;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Optional;
import java.util.regex.Pattern;

public class Utils {
    public static <T> T requireExists(Optional<T> optional, String message) {
        return optional.orElseThrow(() -> new ResourceNotFoundException(message));
    }

    /**
     * Truncate instant to hours to send email and calculate at UTC zone
     * @param date: date to truncate
     * @return date after truncate to hour
     */
    public static Instant truncateInstant(Instant date) {
        return date.truncatedTo(ChronoUnit.HOURS).atZone(ZoneOffset.UTC).toInstant();
    }

    /**
     * Follow LocalDateTime standard
     * @param weekDay: Mon to Sun
     * @return result of weekday
     */
    public static int parseIntegerWeekDay(WeekDay weekDay) {
        switch (weekDay) {
            case MON:
                return 1;
            case TUE:
                return 2;
            case WED:
                return 3;
            case THU:
                return 4;
            case FRI:
                return 5;
            case SAT:
                return 6;
            case SUN:
                return 7;
        }
        return 0;
    }

    /**
     * Check if that the next send date ready to be sent
     * @param now: the time at the present
     * @param nextSendDate: the time pretend to send next chunk
     * @return true if next_send_date equal or before current date and vice versa
     */
    public static boolean isDateBefore(Instant now, Instant nextSendDate) {
        now = Utils.truncateInstant(now);
        nextSendDate = Utils.truncateInstant(nextSendDate);
        return nextSendDate.compareTo(now) <= 0;
    }

    /**
     * Calculate next send date of a cronjob after created or just send next chunk
     * @param now: current date
     * @return instant pretend next send date
     */
    public static Instant calculateNextSendDate(Instant now, CronJobScheduler cronJobScheduler) {
        now = Utils.truncateInstant(now);
        Instant nextSendDate = Utils.truncateInstant(cronJobScheduler.getNextSendDate());

        // If next send date still equal or smaller than curren date, continue to add days by settings, stop when it is in the future
        while(nextSendDate.compareTo(now) <= 0) {
            switch (cronJobScheduler.getFrequency()) {
                case ONCE_PER_WEEK:
                    nextSendDate = nextSendDate.plus(7, ChronoUnit.DAYS);
                    break;
                case ONCE_PER_TWO_WEEK:
                    nextSendDate = nextSendDate.plus(14, ChronoUnit.DAYS);
                    break;
                case ONCE_PER_MONTH:
                    int currentWeekDay = Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday());
                    int weekOfMonth = cronJobScheduler.getWeekOfMonth();
                    DayOfWeek dayOfWeek = DayOfWeek.of(currentWeekDay);
                    int currentMonth = nextSendDate.atZone(ZoneId.of("UTC")).getMonthValue();
                    int nextMonth = currentMonth + 1 == 13 ? 1 : currentMonth + 1;
                    // set time of next month at the same weekday, hour and week of month
                    nextSendDate = LocalDateTime.ofInstant(nextSendDate, ZoneId.of("UTC"))
                        .with(ChronoField.MONTH_OF_YEAR, nextMonth)
                        .with(TemporalAdjusters.dayOfWeekInMonth(weekOfMonth, dayOfWeek))
                        .with(ChronoField.DAY_OF_WEEK, Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday()))
                        .with(ChronoField.HOUR_OF_DAY, cronJobScheduler.getSendHour())
                        .toInstant(ZoneOffset.UTC);
                    break;
            }
        }

        return nextSendDate;
    }

    /**
     * Calculate next send date when create cronjob at the first time
     * @param now: current time
     * @return cronjob updated first next send date
     */
    public static Instant calculateFirstSendDate(Instant now, CronJobScheduler cronJobScheduler) {
        now = Utils.truncateInstant(now);
        ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(now, ZoneId.of("UTC"));

        int currentDayOfWeek = now.atZone(ZoneId.of("UTC")).getDayOfWeek().getValue(); // 1-7
        int currentHour = now.atZone(ZoneId.of("UTC")).getHour(); // 1-7
        int currentWeekOfMonth = LocalDateTime.ofInstant(now, ZoneId.of("UTC")).get(ChronoField.ALIGNED_WEEK_OF_MONTH);
        int currentMonth = now.atZone(ZoneId.of("UTC")).getMonthValue(); // 1-12

        Instant nextSendDate = now;

        switch (cronJobScheduler.getFrequency()) {
            case ONCE_PER_WEEK:
            case ONCE_PER_TWO_WEEK:
                // If have the same weekdays, continue to check hours
                if (currentDayOfWeek == Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday())) {
                    // If send hour is >= current hours + 1 -> able to be sent email
                    if (cronJobScheduler.getSendHour() >= currentHour + 1) {
                        // set the same date but different hour
                        nextSendDate = LocalDateTime.ofInstant(now, ZoneId.of("UTC")).withHour(cronJobScheduler.getSendHour()).toInstant(ZoneOffset.UTC);
                    } else {
                        // case: e.g: today is MONDAY, weekday is SUNDAY, need to plus 7 days
                        nextSendDate = nextSendDate.plus(7, ChronoUnit.DAYS);
                        // set correct hour
                        nextSendDate = LocalDateTime.ofInstant(nextSendDate, ZoneId.of("UTC")).withHour(cronJobScheduler.getSendHour()).toInstant(ZoneOffset.UTC);
                    }
                } else {
                    // Still loop if nextSendDate's weekday is not the same as cronjob weekday
                    // NOTE: LocalDateTime getDayOfWeek start from MONDAY - value = 1... SUNDAY - value = 7
                    while (LocalDateTime.ofInstant(nextSendDate, ZoneId.of("UTC")).getDayOfWeek().getValue() != Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday())) {
                        // case: e.g today is SAT, weekday is THURSDAY, need to plus 5 days more to the next THURSDAY
                        // plus 1 day more until have the same weekday
                        nextSendDate = nextSendDate.plus(1, ChronoUnit.DAYS);
                        // set correct hour
                        nextSendDate = LocalDateTime.ofInstant(nextSendDate, ZoneId.of("UTC")).withHour(cronJobScheduler.getSendHour()).toInstant(ZoneOffset.UTC);
                    }
                }
                break;
            case ONCE_PER_MONTH:
                int currentWeekDay = Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday());
                int weekOfMonth = cronJobScheduler.getWeekOfMonth();
                DayOfWeek dayOfWeek = DayOfWeek.of(currentWeekDay);
                if (cronJobScheduler.getWeekOfMonth() == 1 && currentWeekOfMonth != 1) { // if intend on 1st week
                    // -> plus one more month with settings
                    nextSendDate = LocalDateTime.ofInstant(nextSendDate, ZoneId.of("UTC"))
                        .with(ChronoField.MONTH_OF_YEAR, currentMonth + 1)
                        .with(TemporalAdjusters.dayOfWeekInMonth(weekOfMonth, dayOfWeek))
                        .with(ChronoField.DAY_OF_WEEK, Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday()))
                        .with(ChronoField.HOUR_OF_DAY, cronJobScheduler.getSendHour())
                        .toInstant(ZoneOffset.UTC);
                } else if (cronJobScheduler.getWeekOfMonth() > currentWeekOfMonth) {
                    // add to this month
                    nextSendDate = LocalDateTime.ofInstant(nextSendDate, ZoneId.of("UTC"))
                        .with(ChronoField.MONTH_OF_YEAR, currentMonth)
                        .with(TemporalAdjusters.dayOfWeekInMonth(weekOfMonth, dayOfWeek))
                        .with(ChronoField.DAY_OF_WEEK, Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday()))
                        .with(ChronoField.HOUR_OF_DAY, cronJobScheduler.getSendHour())
                        .toInstant(ZoneOffset.UTC);
                } else {
                    // same day of week and current hour is on future
                    // -> Add date to this month
                    if ((currentDayOfWeek == Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday())
                        && currentHour + 1 <= cronJobScheduler.getSendHour())) {
                        nextSendDate = LocalDateTime.ofInstant(nextSendDate, ZoneId.of("UTC"))
                            .with(ChronoField.MONTH_OF_YEAR, currentMonth)
                            .with(TemporalAdjusters.dayOfWeekInMonth(weekOfMonth, dayOfWeek))
                            .with(ChronoField.DAY_OF_WEEK, Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday()))
                            .with(ChronoField.HOUR_OF_DAY, cronJobScheduler.getSendHour())
                            .toInstant(ZoneOffset.UTC);
                    } else if (currentDayOfWeek < Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday())) {
                        // current day of week < intend weekday
                        // -> Add date to this month
                        nextSendDate = LocalDateTime.ofInstant(nextSendDate, ZoneId.of("UTC"))
                            .with(ChronoField.MONTH_OF_YEAR, currentMonth)
                            .with(TemporalAdjusters.dayOfWeekInMonth(weekOfMonth, dayOfWeek))
                            .with(ChronoField.DAY_OF_WEEK, Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday()))
                            .with(ChronoField.HOUR_OF_DAY, cronJobScheduler.getSendHour())
                            .toInstant(ZoneOffset.UTC);
                    } else if ((currentDayOfWeek == Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday()) && currentHour + 1 > cronJobScheduler.getSendHour()) // same day of week and intend hour is on the past
                        || currentDayOfWeek > Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday())) { // or current day of week is on the past
                        // -> plus one more month with settings
                        nextSendDate = LocalDateTime.ofInstant(nextSendDate, ZoneId.of("UTC"))
                            .with(ChronoField.MONTH_OF_YEAR, currentMonth + 1)
                            .with(TemporalAdjusters.dayOfWeekInMonth(weekOfMonth, dayOfWeek))
                            .with(ChronoField.DAY_OF_WEEK, Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday()))
                            .with(ChronoField.HOUR_OF_DAY, cronJobScheduler.getSendHour())
                            .toInstant(ZoneOffset.UTC);
                    }
                }
                break;
        }

        return nextSendDate;
    }

    /**
     * Calculated end date of a cronjob when finish sending all main email
     * @param cronJobScheduler: cronjob settings information
     * @return end date calculated
     */
    public static Instant calculateEndDate(CronJobScheduler cronJobScheduler) {
        Instant nextSendDate = Utils.truncateInstant(cronJobScheduler.getNextSendDate());
        Instant endDate = nextSendDate;

        // remain email need to be sent
        int remainChunk = cronJobScheduler.getTotalEmailToSend() - cronJobScheduler.getEmailSent();
        int chunkTimeRemain = (int) Math.ceil((double) remainChunk / cronJobScheduler.getChunkNumber());

        if (cronJobScheduler.getFrequency().equals(Frequency.ONCE_PER_WEEK)) {
            // 7 * remain chunk time
            endDate = nextSendDate.plus(7L * (chunkTimeRemain - 1), ChronoUnit.DAYS);
        } else if (cronJobScheduler.getFrequency().equals(Frequency.ONCE_PER_TWO_WEEK)) {
            endDate = nextSendDate.plus(14L * (chunkTimeRemain - 1), ChronoUnit.DAYS);
        } else {
            // month
            LocalDateTime pretendEndDateAtMonth = LocalDateTime.ofInstant(nextSendDate, ZoneId.of("UTC")).plus((chunkTimeRemain - 1), ChronoUnit.MONTHS);
            int intendYear = pretendEndDateAtMonth.getYear();
            int intendMonth = pretendEndDateAtMonth.getMonthValue();
            int currentWeekDay = Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday());
            int weekOfMonth = cronJobScheduler.getWeekOfMonth();
            DayOfWeek dayOfWeek = DayOfWeek.of(currentWeekDay);
            LocalDateTime localDateTime = LocalDateTime.ofInstant(nextSendDate, ZoneId.of("UTC"))
                .with(ChronoField.YEAR, intendYear)
                .with(ChronoField.MONTH_OF_YEAR, intendMonth)
                .with(TemporalAdjusters.dayOfWeekInMonth(weekOfMonth, dayOfWeek))
                .with(ChronoField.DAY_OF_WEEK, Utils.parseIntegerWeekDay(cronJobScheduler.getWeekday()))
                .with(ChronoField.HOUR_OF_DAY, cronJobScheduler.getSendHour());
            endDate = localDateTime.toInstant(ZoneOffset.UTC);
        }
        return endDate;
    }

    /**
     * Check email is valid format or not
     * An email is valid is email has been format ****@**.**** . Email must have at least 2 characters after the dot
     * @param email
     * @return
     */
    public static boolean isValidEmail(String email) {
        Pattern pattern = Pattern.compile("^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+$", Pattern.CASE_INSENSITIVE);
        return pattern.matcher(email).matches();
    }

    /**
     * Replace all extra whitespace start, end and middle of text
     * @param text: text
     * @return handled text
     */
    public static String handleWhitespace(String text) {
        if (text != null) {
            return text.trim().replaceAll(" +", " ");
        }
        return null;
    }

    public static boolean isAllSpaces(String text) {
        return StringUtils.isBlank(text) && StringUtils.isNotEmpty(text);
    }

    /**
     * Calculate Reminder
     * @param now
     * @param day
     * @param sendHour
     * @return
     */
    public static Instant calculateReminder(Instant now, int day, int sendHour){
        now = Utils.truncateInstant(now);
        Instant reminderDate = now.plus(day, ChronoUnit.DAYS);
        reminderDate = LocalDateTime.ofInstant(reminderDate, ZoneId.of("UTC"))
            .with(ChronoField.HOUR_OF_DAY, sendHour)
            .toInstant(ZoneOffset.UTC);
        return reminderDate;
    }

}
