package se.searchofskills.service;

import org.springframework.stereotype.Service;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.security.SecurityUtils;

@Service
public class UserACL {
    /**
     * Check if current user is admin
     * @return true if user is logged in and has role 'admin', false otherwise
     */
    public boolean isAdmin() {
        return SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN);
    }
}
