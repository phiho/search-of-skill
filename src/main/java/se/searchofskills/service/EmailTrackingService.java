package se.searchofskills.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.config.ApplicationProperties;
import se.searchofskills.domain.*;
import se.searchofskills.domain.enumeration.EmailStatus;
import se.searchofskills.domain.enumeration.EmailTemplateType;
import se.searchofskills.repository.*;
import se.searchofskills.service.dto.EmailTrackingDTO;
import se.searchofskills.service.dto.EmailTrackingDataDTO;
import se.searchofskills.service.dto.ResendEmailTrackingDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.ResourceNotFoundException;
import se.searchofskills.service.utils.Utils;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@Transactional
public class EmailTrackingService {
    private final Logger log = LoggerFactory.getLogger(EmailTrackingService.class);

    private final EmailTrackingRepository emailTrackingRepository;

    private final ApplicationProperties applicationProperties;

    private final CronJobEmailTemplateRepository cronJobEmailTemplateRepository;

    private final CronJobService cronJobService;

    private final ApplicationEventPublisher eventPublisher;

    private final EmailTemplateRepository emailTemplateRepository;

    private final CronJobStatisticRepository cronJobStatisticRepository;

    private final UserACL userACL;

    private final CustomEmailTrackingRepository customEmailTrackingRepository;

    private final RecipientRepository recipientRepository;

    public EmailTrackingService(EmailTrackingRepository emailTrackingRepository,
                                ApplicationProperties applicationProperties, CronJobEmailTemplateRepository cronJobEmailTemplateRepository, CronJobService cronJobService, UserACL userACL,
                                EmailTemplateRepository emailTemplateRepository,
                                ApplicationEventPublisher eventPublisher, CronJobStatisticRepository cronJobStatisticRepository, CustomEmailTrackingRepository customEmailTrackingRepository, RecipientRepository recipientRepository) {
        this.emailTrackingRepository = emailTrackingRepository;
        this.applicationProperties = applicationProperties;
        this.cronJobEmailTemplateRepository = cronJobEmailTemplateRepository;
        this.cronJobService = cronJobService;
        this.eventPublisher = eventPublisher;
        this.emailTemplateRepository = emailTemplateRepository;
        this.userACL = userACL;
        this.cronJobStatisticRepository = cronJobStatisticRepository;
        this.customEmailTrackingRepository = customEmailTrackingRepository;
        this.recipientRepository = recipientRepository;
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        Runnable task = this::autoChangeEmailTrackingFromSentToUnanswered;
        service.schedule(task, 60, TimeUnit.SECONDS);

        Runnable taskUpdateAnswer = this::updateUnAnswerForCronjobStatistic;
        service.schedule(taskUpdateAnswer, 120, TimeUnit.SECONDS);

        Runnable taskMigrate = this::migrate;
        service.schedule(taskMigrate, 180, TimeUnit.SECONDS);
        service.shutdown();
    }

    /**
     * Get email tracking list by admin
     *
     * @param pageable : pagination
     * @return email tracking DTO lists
     * @throws AccessForbiddenException if user is not admin
     */
    public Page<EmailTrackingDTO> getListOfEmailTracking(Pageable pageable, Long recipientID, String keyword, boolean archived) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        if(recipientID != null && recipientID > 0){
            return emailTrackingRepository.findAllByThisId(pageable, recipientID, archived).map(EmailTrackingDTO::new);
        }else if (keyword != null && !keyword.equals("")){
            return customEmailTrackingRepository.findEmailTrackingCriteria(pageable, keyword, archived).map(EmailTrackingDTO::new);
        }else {
            return emailTrackingRepository.findAllByDeletedIsFalseAndArchived(pageable, archived).map(EmailTrackingDTO::new);
        }
    }

    /**
     * Get an email tracking by id
     * @param id: id of email tracking
     * @return email tracking DTO
     * @throws AccessForbiddenException if user is not admin
     * @throws ResourceNotFoundException if email tracking not found
     */
    public EmailTrackingDTO findOne(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        return Utils.requireExists(emailTrackingRepository.findByIdAndDeletedIsFalse(id).map(EmailTrackingDTO::new), "error.notFound");
    }

    /**
     * Update an email tracking
     * @param emailTrackingDataDTO
     * @return updated email tracking
     * @throws AccessForbiddenException if user is not admin
     * @throws ResourceNotFoundException if email tracking is deleted or not exist
     */
    public EmailTrackingDataDTO updateEmailTracking(EmailTrackingDataDTO emailTrackingDataDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        EmailTracking emailTracking = Utils.requireExists(emailTrackingRepository.findByIdAndDeletedIsFalse(emailTrackingDataDTO.getId()), "error.notFound");
        if (emailTracking.isArchived()){
            throw new BadRequestException("error.emailTrackingArchived", null);
        }

        // If update comment
        if (emailTrackingDataDTO.getComment() != null) {
            if (Utils.isAllSpaces(emailTrackingDataDTO.getComment())) {
                throw new BadRequestException("error.noWhiteSpace", null);
            }
            emailTracking.setComment(Utils.handleWhitespace(emailTrackingDataDTO.getComment()));
        }

        // If update reminder
        if (emailTrackingDataDTO.getReminderDate2() != null && emailTrackingDataDTO.getReminderDate1() != null) {
            if ((compareDayOfInstant(emailTrackingDataDTO.getReminderDate2(), Instant.now()) <= 0 && !emailTracking.isSentReminder2()) ||
                (compareDayOfInstant(emailTrackingDataDTO.getReminderDate1(), Instant.now()) <= 0 && !emailTracking.isSentReminder1())) {
                throw new BadRequestException("error.templateReminderMustBeGreaterThanCurrentDay", null);
            }

            if (compareDayOfInstant(emailTrackingDataDTO.getReminderDate2(), emailTrackingDataDTO.getReminderDate1()) <= 0 && !emailTracking.isSentReminder2()) {
                throw new BadRequestException("error.templateReminder2MustBeGreaterThan1", null);
            }
        }

        if (emailTrackingDataDTO.getReminderDate1() != null && !equalDayOfInstant(emailTrackingDataDTO.getReminderDate1().atZone(ZoneOffset.UTC).toInstant(), emailTracking.getReminderDate1())) {
            if (emailTracking.isSentReminder1()) {
                throw new BadRequestException("error.reminderDate1WasSent", null);
            }
            emailTracking.setReminderDate1(emailTrackingDataDTO.getReminderDate1());
        }
        if (emailTrackingDataDTO.getReminderDate2() != null && !equalDayOfInstant(emailTrackingDataDTO.getReminderDate2().atZone(ZoneOffset.UTC).toInstant(), emailTracking.getReminderDate2())) {
            if (emailTracking.isSentReminder2()) {
                throw new BadRequestException("error.reminderDate2WasSent", null);
            }
            emailTracking.setReminderDate2(emailTrackingDataDTO.getReminderDate2());
        }

        // If update registrar
        Recipient recipient = emailTracking.getRecipient();
        if (StringUtils.isBlank(emailTrackingDataDTO.getRegistrar())){
            if (recipient.getEmail() == null){
                throw new BadRequestException("error.registrarCanNotNullWhenEmailNull", null);
            }else {
                emailTracking.setRegistrar(null);
            }
        }else {
            if (!Utils.isValidEmail(emailTrackingDataDTO.getRegistrar())){
                throw new BadRequestException("error.invalidEmail", null);
            }
            emailTracking.setRegistrar(emailTrackingDataDTO.getRegistrar());
        }

        if (emailTrackingDataDTO.getStatus() != null) {
            EmailStatus emailStatus = emailTrackingDataDTO.getStatus();
            emailTracking.setStatus(emailStatus);
            updateCronjobStatistic(emailTracking, emailStatus); // Update unanswered value in cronjob statistic
        }

        emailTrackingRepository.save(emailTracking);
        cronJobService.updateIfFinished(emailTracking.getCronJob());
        return new EmailTrackingDataDTO(emailTracking);
    }

    /**
     * Equals day of 2 instants
     * @param instant1
     * @param instant2
     * @return true if they are equal
     */
    private boolean equalDayOfInstant(Instant instant1, Instant instant2) {
        return instant1.truncatedTo(ChronoUnit.DAYS).equals(instant2.truncatedTo(ChronoUnit.DAYS));
    }

    /**
     * Compare day of 2 instants
     * @param instant1
     * @param instant2
     * @return 0 if equal, -1 if instant1 less than instant2, 1 if instant1 greater than instant2
     */
    private int compareDayOfInstant(Instant instant1, Instant instant2) {
        return instant1.truncatedTo(ChronoUnit.DAYS).compareTo(instant2.truncatedTo(ChronoUnit.DAYS));
    }


    /**
     * update cronjob statistic with specific status
     * @param emailTracking
     * @param emailStatus
     */
    public void updateCronjobStatistic(EmailTracking emailTracking, EmailStatus emailStatus) {
        if (emailTracking.getCronJob() != null) {
            Optional<CronJobStatistic> cronJobStatistic = cronJobStatisticRepository.findByCronJobId(emailTracking.getCronJob().getId());
            if (cronJobStatistic.isPresent()) {
                if (!emailStatus.equals(EmailStatus.UNANSWERED) && emailTracking.getStatus().equals(EmailStatus.UNANSWERED)) {
                    cronJobStatistic.get().setUnansweredEmail(cronJobStatistic.get().getUnansweredEmail() > 0 ? cronJobStatistic.get().getUnansweredEmail() - 1 : 0);
                    cronJobStatisticRepository.save(cronJobStatistic.get());
                } else if (emailStatus.equals(EmailStatus.UNANSWERED) && !emailTracking.getStatus().equals(EmailStatus.UNANSWERED)) {
                    cronJobStatistic.get().setUnansweredEmail(cronJobStatistic.get().getUnansweredEmail() + 1);
                    cronJobStatisticRepository.save(cronJobStatistic.get());
                }
            }
        }
    }

    /**
     * Delete an email tracking
     * @param id: id of email tracking
     * @throws AccessForbiddenException if user is not admin
     * @throws ResourceNotFoundException if email tracking is not exist
     */
    public void delete(Long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        EmailTracking emailTracking = Utils.requireExists(emailTrackingRepository.findByIdAndDeletedIsFalse(id), "error.notFound");
        if (!emailTracking.isArchived()){
            throw new BadRequestException("error.emailTrackingNotArchived", null);
        }
        emailTracking.setDeleted(true);
        emailTrackingRepository.save(emailTracking);
    }

    /*
     * Run reminder email tracking every 5 minutes
     */
    @Scheduled(fixedDelay = 300000)
    public void autoSendingReminderEmail() {
        Instant now = Utils.truncateInstant(Instant.now());
        List<EmailTracking> emailTrackingList = emailTrackingRepository.findAllEmailTrackingHavingReminder(EmailStatus.SENT, EmailStatus.INCOMPLETE, now);

        if (emailTrackingList.size() > 0) {
            for (EmailTracking emailTracking : emailTrackingList) {
                Recipient recipient = emailTracking.getRecipient();
                if (emailTracking.getCronJob() == null) {
                    log.error("Can't find cronjob of email tracking " + emailTracking.getId());
                    // need to continue instead of break because we need to send other email tracking if occurs any bug
                    continue;
                }
                List<EmailTemplate> emailTemplates = emailTemplateRepository.findReminderTemplateOfCronJob(EmailTemplateType.MAIN, emailTracking.getCronJob().getId());
                if (emailTemplates.size() == 0) {
                    log.error("Can't find main email template of email tracking with ID is " + emailTracking.getId());
                    continue;
                }
                if (!emailTracking.isSentReminder1()) {
                    // if not send reminder template 1
                    List<EmailTemplate> reminderList1 = emailTemplateRepository.findReminderTemplateOfCronJob(EmailTemplateType.REMINDER_1, emailTracking.getCronJob().getId());
                    if (reminderList1.size() == 0) {
                        log.error("Can't find email template REMINDER_1 of " + emailTracking.getId());
                        continue;
                    }
                    // Send email reminder 1
                    eventPublisher.publishEvent(new AutoSendEmailEvent(recipient, reminderList1.get(0), EmailTemplateType.REMINDER_1, emailTracking.getCreatedDate(), emailTemplates.get(0), null, emailTracking));
                    // Mark as sent
                    emailTracking.setSentReminder1(true);
                    emailTrackingRepository.save(emailTracking);

                    // update statistic data of cronjob
                    if (emailTracking.getCronJob() != null) {
                        Optional<CronJobStatistic> cronJobStatistic = cronJobStatisticRepository.findByCronJobId(emailTracking.getCronJob().getId());
                        if (cronJobStatistic.isPresent()) {
                            cronJobStatistic.get().setReminderSent1(cronJobStatistic.get().getReminderSent1() + 1);
                            cronJobStatisticRepository.save(cronJobStatistic.get());
                        }
                    }

                } else if (!emailTracking.isSentReminder2()) {
                    // sent reminder template 1 but not sent reminder template 2
                    List<EmailTemplate> reminderList2 = emailTemplateRepository.findReminderTemplateOfCronJob(EmailTemplateType.REMINDER_2, emailTracking.getCronJob().getId());
                    if (reminderList2.size() == 0) {
                        log.error("Can't find email template REMINDER_2" + emailTracking.getId());
                        continue;
                    }

                    List<EmailTemplate> reminderList1 = emailTemplateRepository.findReminderTemplateOfCronJob(EmailTemplateType.REMINDER_1, emailTracking.getCronJob().getId());
                    if (reminderList1.size() == 0) {
                        log.error("Can't find email template REMINDER_1" + emailTracking.getId());
                        continue;
                    }

                    // Send email reminder 2
                    eventPublisher.publishEvent(new AutoSendEmailEvent(recipient, reminderList2.get(0), EmailTemplateType.REMINDER_2, emailTracking.getCreatedDate(), emailTemplates.get(0), reminderList1.get(0), emailTracking));
                    // Mark as sent
                    emailTracking.setSentReminder2(true);
                    emailTrackingRepository.save(emailTracking);
                    cronJobService.updateIfFinished(emailTracking.getCronJob());

                    // update statistic data of cronjob
                    if (emailTracking.getCronJob() != null) {
                        Optional<CronJobStatistic> cronJobStatistic = cronJobStatisticRepository.findByCronJobId(emailTracking.getCronJob().getId());
                        if (cronJobStatistic.isPresent()) {
                            cronJobStatistic.get().setReminderSent2(cronJobStatistic.get().getReminderSent2() + 1);
                            cronJobStatisticRepository.save(cronJobStatistic.get());
                        }
                    }
                }
            }
        }
    }

    /**
     * Resend an email tracking
     * @param resendEmailTrackingDTO
     * @throws AccessForbiddenException if user is not admin
     * @throws ResourceNotFoundException if email tracking is deleted or not exist
     * @throws BadRequestException if email tracking is not status could not be sent: error.resendError key
     * @throws BadRequestException if registrar is invalid format: error.registrarFormatEmailInvalid key
     */
    public void resendEmailTracking(ResendEmailTrackingDTO resendEmailTrackingDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        EmailTracking emailTracking = Utils.requireExists(emailTrackingRepository.findByIdAndDeletedIsFalse(resendEmailTrackingDTO.getId()), "error.notFound");
        if (emailTracking.getStatus().equals(EmailStatus.COULD_NOT_BE_SENT)) {
            emailTracking.setStatus(EmailStatus.SENT);
            Recipient recipient = new Recipient();
            recipient.setId(emailTracking.getRecipient().getId());
            recipient.setReferenceNumber(emailTracking.getRecipient().getReferenceNumber());
            recipient.setTitle(emailTracking.getRecipient().getTitle());
            recipient.setEmail(resendEmailTrackingDTO.getEmail());
            if (!StringUtils.isBlank(resendEmailTrackingDTO.getRegistrar())) {
                if (Utils.isValidEmail(resendEmailTrackingDTO.getRegistrar())) {
                    recipient.setRegistrar(resendEmailTrackingDTO.getRegistrar());
                } else {
                    throw new BadRequestException("error.registrarFormatEmailInvalid", null);
                }
            }
            List<EmailTemplate> emailTemplates = emailTemplateRepository.findMainTemplateOfCronJob(EmailTemplateType.MAIN, emailTracking.getCronJob().getId());
            if (emailTemplates.size() == 0) {
                throw new ResourceNotFoundException("error.emailTemplateMainNotFound");
            }
            eventPublisher.publishEvent(new AutoSendEmailEvent(recipient, emailTemplates.get(0), EmailTemplateType.MAIN, null, null, null, null));
            emailTrackingRepository.save(emailTracking);
            cronJobService.updateIfFinished(emailTracking.getCronJob());
        } else {
            throw new BadRequestException("error.resendError", null);
        }
    }

    /**
     * Update status for an or multiple email tracking
     * @param emailTrackingIds
     * @throws AccessForbiddenException if user is not admin
     */
    public void updateStatusForEmailTracking(List<Long> emailTrackingIds, EmailStatus emailStatus) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        List<EmailTracking> emailTrackingList = emailTrackingRepository.findAllByIdInAndDeletedIsFalse(emailTrackingIds);

        for (EmailTracking emailTracking: emailTrackingList) {
            // update statistic data of cronjob
            updateCronjobStatistic(emailTracking, emailStatus);
            emailTracking.setStatus(emailStatus);
        }
        emailTrackingRepository.saveAll(emailTrackingList);


        Set<CronJob> updateCronjobList = emailTrackingList.stream().map(EmailTracking::getCronJob).collect(Collectors.toSet());
        for (CronJob cronJob: updateCronjobList) {
            cronJobService.updateIfFinished(cronJob);
        }
    }

    /**
     * get all email tracking with sent status
     * @param cronjobId
     * @return list of valid email tracking
     */
    public List<EmailTracking> findAllEmailTrackingWithSentStatusByCronjobId(long cronjobId) {
        return emailTrackingRepository.findAllEmailTrackingSentAndIncompleteByCronjobId(cronjobId);
    }

    /*
     * Run automatically change sent email to unanswer everyday at 01:00 am
     */
    // TODO find the way to mock test for this case using while to get all items
    @Scheduled(cron = "0 0 1 * * ?")
    public void autoChangeEmailTrackingFromSentToUnanswered() {
        Long timeChangeSentToUnAnswered = applicationProperties.getTimeChangeSentToUnanswered();
        if (timeChangeSentToUnAnswered == null || timeChangeSentToUnAnswered <= 0) {
            return;
        }
        int page = 0;
        List<EmailTracking> emailTrackingList = emailTrackingRepository.findEmailTrackingsToChangeUnanswer(PageRequest.of(page, 50), EmailStatus.SENT, EmailStatus.INCOMPLETE);
        while (emailTrackingList.size() > 0) {
            for (EmailTracking emailTracking: emailTrackingList) {
                // if the time sent reminder 2 to current time is 1 month or more
                if (emailTracking.getReminderDate2() != null  && (Instant.now().getEpochSecond() - emailTracking.getReminderDate2().getEpochSecond() > timeChangeSentToUnAnswered)) {
                    emailTracking.setStatus(EmailStatus.UNANSWERED);

                    // update cronjob statistic for number of unanswered
                    if (emailTracking.getCronJob() != null) {
                        List<CronJobStatistic> cronJobStatistics = cronJobStatisticRepository.findAllByCronJobId(emailTracking.getCronJob().getId());
                        if (cronJobStatistics.size() > 0) {
                            for (CronJobStatistic cronJobStatistic: cronJobStatistics) {
                                cronJobStatistic.setUnansweredEmail(cronJobStatistic.getUnansweredEmail() + 1);
                            }
                            cronJobStatisticRepository.saveAll(cronJobStatistics);
                        }
                    }
                }
            }
            emailTrackingRepository.saveAll(emailTrackingList);
            page++;
            emailTrackingList = emailTrackingRepository.findEmailTrackingsToChangeUnanswer(PageRequest.of(page, 50), EmailStatus.SENT, EmailStatus.INCOMPLETE);
        }
    }

    public void updateUnAnswerForCronjobStatistic() {
        List<CronJobStatistic> cronJobStatistics = cronJobStatisticRepository.findAll();
        for (CronJobStatistic cronJobStatistic: cronJobStatistics) {
            if (cronJobStatistic.getCronJob() == null) {
                continue;
            }
            int countAllAnswerEmailTracking = emailTrackingRepository.countAllByStatusAndCronJobId(EmailStatus.UNANSWERED, cronJobStatistic.getCronJob().getId());
            cronJobStatistic.setUnansweredEmail(countAllAnswerEmailTracking);
        }
        cronJobStatisticRepository.saveAll(cronJobStatistics);
    }

    /**
     *  get email tracking list filter by id by admin
     * @param pageable
     * @param id
     * @return list email tracking with pagination
     */
    public Page<EmailTrackingDTO> findAllById(Pageable pageable, long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        return emailTrackingRepository.findAllByThisId(pageable, id, false).map(EmailTrackingDTO::new);
    }

    /**
     * archive an email tracking by id
     * @param id
     */
    public void archive(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        EmailTracking emailTracking = Utils.requireExists(emailTrackingRepository.findById(id), "error.notFound");
        if (emailTracking.isArchived()){
            throw new BadRequestException("error.emailTrackingArchived", null);
        }
        emailTracking.setArchived(true);
        emailTrackingRepository.save(emailTracking);
    }

    /**
     * restore an email tracking by id
     * @param id
     */
    public void restore(long id) {
        Instant now = Instant.now();

        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        EmailTracking emailTracking = Utils.requireExists(emailTrackingRepository.findById(id), "error.notFound");
        if (!emailTracking.isArchived()){
            throw new BadRequestException("error.emailTrackingNotArchived", null);
        }
        emailTracking.setArchived(false);
        if (!emailTracking.isSentReminder1() && Utils.isDateBefore(now, emailTracking.getReminderDate1())){
            CronJob cronJob = emailTracking.getCronJob();
            emailTracking.setReminderDate1(Utils.calculateReminder(now, cronJob.getTemplateReminderTime1(), cronJob.getSendHour()));
            emailTracking.setReminderDate2(Utils.calculateReminder(now, cronJob.getTemplateReminderTime2(), cronJob.getSendHour()));
        }else if (emailTracking.isSentReminder1() && !emailTracking.isSentReminder2() && Utils.isDateBefore(now, emailTracking.getReminderDate2())){
            CronJob cronJob = emailTracking.getCronJob();
            emailTracking.setReminderDate2(Utils.calculateReminder(now, cronJob.getTemplateReminderTime2(), cronJob.getSendHour()));
        }
        emailTrackingRepository.save(emailTracking);
    }

    public void migrate() {
        boolean migrate = applicationProperties.isMigrate();
        if (migrate){
            List<EmailTracking> emailTrackingList = emailTrackingRepository.findAllByDeletedIsFalse();
            for (EmailTracking emailTracking : emailTrackingList) {
                Recipient recipient = emailTracking.getRecipient();
                if (emailTracking.getRegistrar() == null) {
                    emailTracking.setRegistrar(recipient.getRegistrar());
                }
                emailTrackingRepository.save(emailTracking);
            }
        }
    }
}
