package se.searchofskills.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import se.searchofskills.domain.*;
import se.searchofskills.domain.enumeration.CronJobStatus;
import se.searchofskills.domain.enumeration.EmailStatus;
import se.searchofskills.domain.enumeration.EmailTemplateType;
import se.searchofskills.repository.*;
import se.searchofskills.service.dto.*;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.ResourceNotFoundException;
import se.searchofskills.service.utils.Utils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class CronJobService {
    private final Logger log = LoggerFactory.getLogger(CronJobService.class);

    private final CronJobRepository cronJobRepository;
    private final UserACL userACL;
    private final CronJobStatisticRepository cronJobStatisticRepository;
    private final CronJobEmailTemplateRepository cronJobEmailTemplateRepository;
    private final EmailTemplateRepository emailTemplateRepository;
    private final RecipientListRepository recipientListRepository;
    private final RecipientRepository recipientRepository;
    private final ApplicationEventPublisher eventPublisher;
    private final EmailTrackingRepository emailTrackingRepository;
    private final TimeService timeService;

    public CronJobService(CronJobRepository cronJobRepository,
                          UserACL userACL,
                          CronJobStatisticRepository cronJobStatisticRepository,
                          CronJobEmailTemplateRepository cronJobEmailTemplateRepository,
                          RecipientListRepository recipientListRepository,
                          RecipientRepository recipientRepository,
                          ApplicationEventPublisher applicationEventPublisher,
                          EmailTemplateRepository emailTemplateRepository,
                          EmailTrackingRepository emailTrackingRepository, TimeService timeService) {
        this.cronJobRepository = cronJobRepository;
        this.userACL = userACL;
        this.cronJobStatisticRepository = cronJobStatisticRepository;
        this.cronJobEmailTemplateRepository = cronJobEmailTemplateRepository;
        this.recipientListRepository = recipientListRepository;
        this.recipientRepository = recipientRepository;
        this.eventPublisher = applicationEventPublisher;
        this.emailTemplateRepository = emailTemplateRepository;
        this.emailTrackingRepository = emailTrackingRepository;
        this.timeService = timeService;
    }

    /**
     * Restore a cronjob
     * @param id: id of cronjob
     * @throws AccessForbiddenException if user is not admin
     * @throws ResourceNotFoundException if cronjob not exist
     * @throws BadRequestException if cronjob is deleted or status not cancelled and not archived.
     */
    public void restore(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        CronJob cronJob = Utils.requireExists(cronJobRepository.findById(id), "error.notFound");

        // If cronjob is not cancelled, not archived or deleted
        if (!cronJob.getStatus().equals(CronJobStatus.CANCELLED) || !cronJob.isArchived() || cronJob.isDeleted()) {
            throw new BadRequestException("error.cronjobStatusInvalid", null);
        }

        cronJob.setArchived(false);
        cronJobRepository.save(cronJob);
    }

    /**
     * cancel a cronjob is running
     * @param id: id of cronjob
     * @throws AccessForbiddenException if user is not admin
     * @throws ResourceNotFoundException if cronjob not exist
     * @throws BadRequestException if cronjob is deleted or status not cancelled and not archived.
     */
    public void cancel(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        CronJob cronJob = Utils.requireExists(cronJobRepository.findById(id), "error.notFound");

        // If cronjob is not running, archived or deleted
        if (!cronJob.getStatus().equals(CronJobStatus.RUNNING) || cronJob.isArchived() || cronJob.isDeleted()) {
            throw new BadRequestException("error.cronjobStatusInvalid", null);
        }

        cronJob.setStatus(CronJobStatus.CANCELLED);
        cronJobRepository.save(cronJob);
    }

    /**
     * Resume a cronjob
     * @param id: id of a cronjob
     */
    public void resume(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        CronJob cronJob = Utils.requireExists(cronJobRepository.findById(id), "error.notFound");

        CronJobStatistic cronJobStatistic = Utils.requireExists(cronJobStatisticRepository.findByCronJobId(cronJob.getId()), "error.notFound");

        // If cronjob is not cancelled, archived or deleted
        if (!cronJob.getStatus().equals(CronJobStatus.CANCELLED) || cronJob.isArchived() || cronJob.isDeleted()) {
            throw new BadRequestException("error.cronjobStatusInvalid", null);
        }

        CronJobScheduler cronJobScheduler = new CronJobScheduler(cronJob);
        cronJobScheduler.setTotalEmailToSend(cronJobStatistic.getTotalEmailToSend());
        cronJobScheduler.setEmailSent(cronJobStatistic.getEmailSent());

        cronJob.setStatus(CronJobStatus.RUNNING);
        // calculated first send date
        cronJob.setNextSendDate(Utils.calculateFirstSendDate(Instant.now(), cronJobScheduler));
        cronJobScheduler.setNextSendDate(cronJob.getNextSendDate());
        // calculate new end date
        cronJob.setEndDate(Utils.calculateEndDate(cronJobScheduler));
        cronJobRepository.save(cronJob);
    }

    /**
     * Get cronjob statistic
     * @param cronJobId: id of cronjob
     * @return cronjob statistic data
     */
    public CronJobStatisticDTO getStatistic(long cronJobId) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        CronJobStatistic cronJobStatistic = Utils.requireExists(cronJobStatisticRepository.findByCronJobId(cronJobId), "error.notFound");

        return new CronJobStatisticDTO(cronJobStatistic);
    }

    /**
     * Delete cronjob by id
     * @param id: id of cronjob
     */
    public void delete(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        CronJob cronJob = Utils.requireExists(cronJobRepository.findById(id), "error.notFound");

        // If cronjob is not running, archived or deleted
        if (!cronJob.isArchived() || !(cronJob.getStatus().equals(CronJobStatus.CANCELLED) || cronJob.getStatus().equals(CronJobStatus.FINISHED)) || cronJob.isDeleted()) {
            throw new BadRequestException("error.cronjobStatusInvalid", null);
        }

        cronJob.setDeleted(true);
        cronJobRepository.save(cronJob);
    }

    /**
     * Save cronjob
     * @param cronjobDTO: cronjob data sent from client
     * @return created CronjobDTO
     */
    public CronjobDTO saveCronjob(CronjobDTO cronjobDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        if (cronjobDTO.getChunkNumber() < 1) {
            throw new BadRequestException("error.chunkNumberMustBeGreaterThanZero", null);
        }

        if (cronjobDTO.getTemplateReminderTime1() < 1 || cronjobDTO.getTemplateReminderTime2() < 1) {
            throw new BadRequestException("error.templateReminderMustBeGreaterThanZero", null);
        }

        if (cronjobDTO.getTemplateReminderTime2() <= cronjobDTO.getTemplateReminderTime1()) {
            throw new BadRequestException("error.templateReminder2MustBeGreaterThan1", null);
        }

        CronJobScheduler cronJobScheduler = new CronJobScheduler(new CronJob(cronjobDTO));
        // calculated first send date
        cronJobScheduler.setNextSendDate(cronjobDTO.getStartDate());

        RecipientList recipientList = Utils.requireExists(recipientListRepository.findById(cronjobDTO.getRecipientListId()), "error.recipientListNotFound");
        int totalEmailToSent = recipientRepository.countAllByRecipientListId(recipientList.getId());
        cronJobScheduler.setTotalEmailToSend(totalEmailToSent);
        // calculate new end date
        Instant endDate = Utils.calculateEndDate(cronJobScheduler);

        if (endDate.truncatedTo(ChronoUnit.DAYS).compareTo(cronjobDTO.getEndDate().truncatedTo(ChronoUnit.DAYS)) != 0) {
            throw new BadRequestException("error.endDateInvalid", null);
        }

        EmailTemplate emailTemplateMain = Utils.requireExists(emailTemplateRepository.findById(cronjobDTO.getTemplateMainId()), "error.templateMainNotFound");
        EmailTemplate emailTemplateReminder1 = Utils.requireExists(emailTemplateRepository.findByIdAndType(cronjobDTO.getTemplateReminder1Id(), EmailTemplateType.REMINDER_1), "error.templateReminder1NotFound");
        EmailTemplate emailTemplateReminder2 = Utils.requireExists(emailTemplateRepository.findByIdAndType(cronjobDTO.getTemplateReminder2Id(), EmailTemplateType.REMINDER_2), "error.templateReminder2NotFound");

        CronJob newCronjob = new CronJob(cronjobDTO);
        newCronjob.setNextSendDate(cronjobDTO.getStartDate());
        newCronjob.setStartDate(cronjobDTO.getStartDate());
        newCronjob.setRecipientList(recipientList);
        newCronjob.setStatus(CronJobStatus.RUNNING);
        CronJob cronJob = cronJobRepository.save(newCronjob);

        // save cronjob email templates
        List<CronJobEmailTemplate> cronJobEmailTemplateList = new ArrayList<>();
        CronJobEmailTemplate cronJobEmailTemplateMain = new CronJobEmailTemplate();
        cronJobEmailTemplateMain.setCronJob(cronJob);
        cronJobEmailTemplateMain.setEmailTemplate(emailTemplateMain);
        cronJobEmailTemplateMain.setType(EmailTemplateType.MAIN);
        cronJobEmailTemplateList.add(cronJobEmailTemplateMain);

        CronJobEmailTemplate cronJobEmailTemplateReminder1 = new CronJobEmailTemplate();
        cronJobEmailTemplateReminder1.setCronJob(cronJob);
        cronJobEmailTemplateReminder1.setEmailTemplate(emailTemplateReminder1);
        cronJobEmailTemplateReminder1.setType(EmailTemplateType.REMINDER_1);
        cronJobEmailTemplateList.add(cronJobEmailTemplateReminder1);

        CronJobEmailTemplate cronJobEmailTemplateReminder2 = new CronJobEmailTemplate();
        cronJobEmailTemplateReminder2.setType(EmailTemplateType.REMINDER_2);
        cronJobEmailTemplateReminder2.setEmailTemplate(emailTemplateReminder2);
        cronJobEmailTemplateReminder2.setCronJob(cronJob);
        cronJobEmailTemplateList.add(cronJobEmailTemplateReminder2);

        cronJobEmailTemplateRepository.saveAll(cronJobEmailTemplateList);

        // save cronjob statistic
        CronJobStatistic cronJobStatistic = new CronJobStatistic();
        cronJobStatistic.setCronJob(cronJob);
        cronJobStatistic.setTotalEmailToSend(totalEmailToSent);

        cronJobStatisticRepository.save(cronJobStatistic);

        return new CronjobDTO(cronJob);
    }

    /**
     * get EndDate if it is overlap by any cronjob is running
     * @param endDate: endDate sent from client
     * @return endDate if it's exist or null if not
     */
    public Instant getLatestDateOverlapEndDate(Instant endDate) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Optional<CronJob> cronJob = cronJobRepository.getTopByStatusIsAndEndDateAfterOrderByEndDateDesc(CronJobStatus.RUNNING, endDate);
        return cronJob.map(CronJob::getEndDate).orElse(null);
    }

    /**
     * find list cronjob
     * @param pageable
     * @param isArchived
     * @return page of CronjobDataDTO
     */
    public Page<CronjobDataDTO> findCronjobByArchived(Pageable pageable, boolean isArchived) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Page<CronJob> page = cronJobRepository.findAllByArchivedIsAndDeletedIsFalse(isArchived, pageable);
        return page.map(this::convertCronjobToCronjobDataDTO);
    }

    /**
     * convert cronjob to CronjobDataDTO to return client
     * @param cronJob
     * @return CronjobDataDTO
     */
    public CronjobDataDTO convertCronjobToCronjobDataDTO(CronJob cronJob) {
        CronjobDataDTO cronjobDataDTO = new CronjobDataDTO();
        cronjobDataDTO.setId(cronJob.getId());
        cronjobDataDTO.setName(cronJob.getName());
        cronjobDataDTO.setChunkNumber(cronJob.getChunkNumber());
        cronjobDataDTO.setStartDate(cronJob.getStartDate());
        cronjobDataDTO.setEndDate(cronJob.getEndDate());
        cronjobDataDTO.setSendHour(cronJob.getSendHour());
        cronjobDataDTO.setWeekday(cronJob.getWeekday());
        cronjobDataDTO.setFrequency(cronJob.getFrequency());
        cronjobDataDTO.setWeekOfMonth(cronJob.getWeekOfMonth());
        cronjobDataDTO.setStatus(cronJob.getStatus());
        cronjobDataDTO.setRecipientList(new RecipientListDTO(cronJob.getRecipientList()));

        List<CronJobEmailTemplate> cronJobEmailTemplates = cronJobEmailTemplateRepository.findAllByCronJob_Id(cronJob.getId());

        EmailTemplateDTO emailTemplateMain = this.getEmailTemplateByTypeOfCronjob(cronJobEmailTemplates, EmailTemplateType.MAIN);
        EmailTemplateDTO emailTemplateReminder1 = this.getEmailTemplateByTypeOfCronjob(cronJobEmailTemplates, EmailTemplateType.REMINDER_1);
        EmailTemplateDTO emailTemplateReminder2 = this.getEmailTemplateByTypeOfCronjob(cronJobEmailTemplates, EmailTemplateType.REMINDER_2);

        cronjobDataDTO.setTemplateMain(emailTemplateMain);
        cronjobDataDTO.setTemplateReminder1(emailTemplateReminder1);
        cronjobDataDTO.setTemplateReminder2(emailTemplateReminder2);

        return cronjobDataDTO;
    }

    public CronjobDataDTO convertCronjobToCronjobDataDTOButOnlyName(CronJob cronJob) {
        CronjobDataDTO cronjobDataDTO = new CronjobDataDTO();
        cronjobDataDTO.setName(cronJob.getName());
        return cronjobDataDTO;
    }

    /**
     * get email template by type from cronjob template list of specific cronjob
     * @param cronJobEmailTemplates: template list of cronjob
     * @param type: type of email template
     * @return EmailTemplateDTO
     */
    public EmailTemplateDTO getEmailTemplateByTypeOfCronjob(List<CronJobEmailTemplate> cronJobEmailTemplates, EmailTemplateType type) {
        for (CronJobEmailTemplate cronJobEmailTemplate: cronJobEmailTemplates) {
            if (cronJobEmailTemplate.getType().equals(type)) {
                return new EmailTemplateDTO(cronJobEmailTemplate.getEmailTemplate());
            }
        }
        return null;
    }

    /**
     * archive a cronjob by id
     * @param id of cronjob
     */
    public void archive(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        CronJob cronJob = Utils.requireExists(cronJobRepository.findById(id), "error.notFound");

        // If cronjob is running, archived or deleted
        if (cronJob.getStatus().equals(CronJobStatus.RUNNING) || cronJob.isArchived() || cronJob.isDeleted()) {
            throw new BadRequestException("error.cronjobStatusInvalid", null);
        }

        cronJob.setArchived(true);
        cronJobRepository.save(cronJob);
    }

    // Run every 5 minutes
    @Scheduled(fixedDelay = 300000)
    public void autoSendingEmail() {
        // Get all running cronjob
        List<CronJob> cronJobList = cronJobRepository.findAllByStatusAndNextSendDateBefore(CronJobStatus.RUNNING, Instant.now());

        if (cronJobList.size() > 0) {
            for (CronJob cronJob: cronJobList) {
                sendEmailWithCronJob(cronJob, Instant.now());
            }
        }
    }

    /**
     *
     * @param cronJob: cronjob to send
     * @param now: current time in UTC
     */
    public void sendEmailWithCronJob(CronJob cronJob, Instant now) {
        now = Utils.truncateInstant(now);
        int currentHour = now.atZone(ZoneId.of("UTC")).getHour();

        if (cronJob.getSendHour() <= currentHour) {
            Optional<CronJobStatistic> cronJobStatistic = cronJobStatisticRepository.findByCronJobId(cronJob.getId());
            if (cronJobStatistic.isPresent()) {
                int emailSent = cronJobStatistic.get().getEmailSent(); // offset
                int totalEmailToSend = cronJobStatistic.get().getTotalEmailToSend();
                int chunk = cronJob.getChunkNumber(); // limit

                if (emailSent < totalEmailToSend) { // cronjob still not send all "main email"
                    // It's time to run cronjob!

                    // get recipients to send this phrase
                    int currentPage = (emailSent / chunk) - 1; // default page will start from index 0
                    int nextPage = currentPage + 1;
                    Pageable pageable = PageRequest.of(nextPage, chunk);
                    List<Recipient> recipients = recipientRepository.findAllByRecipientListId(pageable, cronJob.getRecipientList().getId()).getContent();

                    if (recipients.size() > 0) {
                        // get cronjob main template
                        List<EmailTemplate> emailTemplates = emailTemplateRepository.findMainTemplateOfCronJob(EmailTemplateType.MAIN, cronJob.getId());
                        if (emailTemplates.size() == 0) {
                            log.error("Can't find email template MAIN");
                            return;
                        }
                        int totalEmailSent = 0;

                        for (Recipient recipient: recipients) {
                            // call to mail service to send email
                            eventPublisher.publishEvent(new AutoSendEmailEvent(recipient, emailTemplates.get(0), EmailTemplateType.MAIN, null, null, null, null));

                            // add 1 count for email sent to statistic
                            totalEmailSent = totalEmailSent + 1;
                            // save email tracking
                            this.saveEmailTracking(cronJob, recipient);
                        }

                        // update statistic data of cronjob
                        cronJobStatistic.get().setEmailSent(cronJobStatistic.get().getEmailSent() + totalEmailSent);
                        cronJobStatisticRepository.save(cronJobStatistic.get());

                        if (cronJobStatistic.get().getEmailSent() < cronJobStatistic.get().getTotalEmailToSend()) {
                            // update next send date of cronjob
                            cronJob.setNextSendDate(Utils.calculateNextSendDate(now, new CronJobScheduler(cronJob)));
                            cronJobRepository.save(cronJob);
                        }
                    }
                }
            } else {
                log.error("Statistic cronjob not found");
            }
        }
    }

    /**
     * check a cronjob is finished or not, a cronjob need to archive 2 conditions to finish
     * Condition 1: all main email sent
     * Condition 2: All email status does not have status "Sent" or if it is "Sent", both "reminder 1 and 2" must be sent.
     */
    public boolean isFinished(CronJob cronJob) {
        CronJobStatistic cronJobStatistic = Utils.requireExists(cronJobStatisticRepository.findByCronJobId(cronJob.getId()), "error.notFound");
        // Check if all main email was sent
        if (cronJobStatistic.getTotalEmailToSend() == cronJobStatistic.getEmailSent()) {
            // Get all email have status is SENT
            List<EmailTracking> emailTrackingList = emailTrackingRepository.findAllEmailTrackingSentAndIncompleteByCronjobId(cronJob.getId());
            if (emailTrackingList.size() != 0) {
                for (EmailTracking emailTracking : emailTrackingList) {
                    // Check if SENT email has not sent both of reminders or not
                    if (!emailTracking.isSentReminder1() || !emailTracking.isSentReminder2()) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     * update if cronjob is finished
     * @param cronJob
     */
    public void updateIfFinished(CronJob cronJob) {
        cronJob = Utils.requireExists(cronJobRepository.findById(cronJob.getId()),  "error.notFound");
        if (isFinished(cronJob)) {
            if (!cronJob.getStatus().equals(CronJobStatus.FINISHED)) {
                cronJob.setStatus(CronJobStatus.FINISHED);
                cronJobRepository.save(cronJob);
            }
        } else {
            if (cronJob.getStatus().equals(CronJobStatus.FINISHED)) {
                cronJob.setStatus(CronJobStatus.RUNNING);
                cronJobRepository.save(cronJob);
            }
        }
    }

    /**
     * Save new email tracking
     * @param cronJob: cronjob of that
     * @param recipient: recipient data of that email
     */
    public EmailTracking saveEmailTracking(CronJob cronJob, Recipient recipient) {
        Optional<EmailTracking> emailTrackingOptional = emailTrackingRepository.findOneByRecipientAndCronJob(recipient, cronJob);
        if (emailTrackingOptional.isPresent()) {
            log.error("Email tracking of this recipient and cronjob already exist with recipient identifier: "+ recipient.getId() + " and cronjob identifier: "+ cronJob.getId());
            return null;
        }
        Instant now = Utils.truncateInstant(Instant.now());
        int afterDays1 = cronJob.getTemplateReminderTime1();
        int afterDays2 = cronJob.getTemplateReminderTime2();
        Instant sentReminder1 = now.plus(afterDays1, ChronoUnit.DAYS);
        Instant sentReminder2 = now.plus(afterDays2, ChronoUnit.DAYS);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setDeleted(false);
        emailTracking.setStatus(EmailStatus.SENT);
        emailTracking.setCronJob(cronJob);
        emailTracking.setRecipient(recipient);
        emailTracking.setRegistrar(recipient.getRegistrar());
        emailTracking.setSentReminder1(false);
        emailTracking.setSentReminder2(false);
        emailTracking.setReminderDate1(sentReminder1);
        emailTracking.setReminderDate2(sentReminder2);
        emailTrackingRepository.save(emailTracking);
        return emailTracking;
    }

    /**
     * Use when create cronjob for suggestion name
     * @param pageable
     * @param name
     * @return list cronjob
     */
    public Page<CronjobDataDTO> getCronjobs(Pageable pageable, String name) {
        if (!userACL.isAdmin()){
            throw new AccessForbiddenException("error.notAdmin");
        }
        if (name == null || name.isEmpty()){
            Page<CronJob> page = cronJobRepository.findCronJobAllByDeletedIsFalse(pageable);
            return page.map(this::convertCronjobToCronjobDataDTOButOnlyName);
        }else {
            Page<CronJob> page = cronJobRepository.findAllByNameContainingAndDeletedIsFalse(pageable, name);
            return page.map(this::convertCronjobToCronjobDataDTOButOnlyName);
        }
    }
}


