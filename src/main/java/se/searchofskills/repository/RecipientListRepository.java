package se.searchofskills.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.RecipientList;
import se.searchofskills.domain.enumeration.CronJobStatus;

import java.util.Optional;

@Repository
public interface RecipientListRepository extends JpaRepository<RecipientList, Long> {
    Optional<RecipientList> findByIdAndDeletedIsFalse(long id);

    Page<RecipientList> findAllByDeleted(Pageable pageable, boolean deleted);

    Page<RecipientList> findAllByDeletedAndNameContainingIgnoreCase(Pageable pageable, boolean deleted, String name);

    @Query(value = "SELECT count (cr.id) FROM CronJob cr " +
        " WHERE cr.status = :status AND cr.recipientList.id = :recipientListId")
    long countAllCrobjobIsUsingRecipientById(@Param("status") CronJobStatus status, @Param("recipientListId") long recipientListId);
}
