package se.searchofskills.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.CronJob;
import se.searchofskills.domain.EmailTracking;
import se.searchofskills.domain.Recipient;
import se.searchofskills.domain.enumeration.EmailStatus;

import java.time.Instant;
import java.util.List;

import java.util.Optional;

@Repository
public interface EmailTrackingRepository extends JpaRepository<EmailTracking, Long> {
    Page<EmailTracking> findAllByDeletedIsFalseAndArchived(Pageable pageable, boolean archived);

    @Query(value = "SELECT e FROM EmailTracking e " +
        "WHERE (e.status =:status1 OR e.status =:status2)" +
        "AND e.sentReminder1 = true " +
        "AND e.sentReminder2 = true AND e.deleted = false AND e.archived = false")
    List<EmailTracking> findEmailTrackingsToChangeUnanswer(Pageable pageable, @Param("status1") EmailStatus status1, @Param("status2") EmailStatus status2);

    Optional<EmailTracking> findByIdAndDeletedIsFalse(long id);

    List<EmailTracking> findAllByIdInAndDeletedIsFalse(List<Long> ids);

    Optional<EmailTracking> findOneByRecipientAndCronJob(Recipient recipient, CronJob cronJob);

    @Query(value = "SELECT e FROM EmailTracking e " +
        "WHERE (e.status =:status1 OR e.status =:status2)" +
        "AND (e.archived = false  AND e.deleted = false)" +
        "AND ((e.reminderDate1 <= :now AND e.sentReminder1 = false) " +
        "OR (e.reminderDate2 <= :now AND e.sentReminder2 = false AND e.sentReminder1 = true))")
    List<EmailTracking> findAllEmailTrackingHavingReminder(@Param("status1")EmailStatus status1,@Param("status2")EmailStatus status2 , @Param("now")Instant now);

    @Query(value = "SELECT e FROM EmailTracking  e " +
        "WHERE (e.status = 'SENT' OR e.status = 'INCOMPLETE')" +
        "AND (e.cronJob.id = :cronjobId) AND e.deleted = false")
    List<EmailTracking> findAllEmailTrackingSentAndIncompleteByCronjobId(@Param("cronjobId") long id);

    int countAllByStatusAndCronJobId(EmailStatus emailStatus, Long id);

    @Query("select e from EmailTracking e where e.deleted = false and e.archived = :archive and cast(e.recipient.id as string) like %:id%")
    Page<EmailTracking> findAllByThisId(Pageable pageable,@Param("id") long id,@Param("archive") boolean archived);

    List<EmailTracking> findAllByDeletedIsFalse();

}
