package se.searchofskills.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.CronJob;
import se.searchofskills.domain.enumeration.CronJobStatus;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Repository
public interface CronJobRepository extends JpaRepository<CronJob, Long> {
    List<CronJob> findAllByStatusAndNextSendDateBefore(CronJobStatus cronJobStatus, Instant now);

    int countAllByStatusIsAndEndDateAfter(CronJobStatus cronJobStatus, Instant endDate);

    Optional<CronJob> getTopByStatusIsAndEndDateAfterOrderByEndDateDesc(CronJobStatus cronJobStatus, Instant endDate);

    Page<CronJob> findAllByArchivedIsAndDeletedIsFalse(boolean isArchived, Pageable pageable);

    Page<CronJob> findAllByNameContainingAndDeletedIsFalse(Pageable pageable, String name);

    Page<CronJob> findCronJobAllByDeletedIsFalse(Pageable pageable);

}
