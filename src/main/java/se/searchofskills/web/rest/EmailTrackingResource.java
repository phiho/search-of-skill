package se.searchofskills.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.searchofskills.domain.enumeration.EmailStatus;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.EmailTrackingService;
import se.searchofskills.service.dto.EmailTrackingDTO;
import se.searchofskills.service.dto.EmailTrackingDataDTO;
import se.searchofskills.service.dto.ResendEmailTrackingDTO;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class EmailTrackingResource {

    private final EmailTrackingService emailTrackingService;

    public EmailTrackingResource(EmailTrackingService emailTrackingService) {
        this.emailTrackingService = emailTrackingService;
    }

    @ApiOperation(value = "Get email tracking list with pagination")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - get data successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @GetMapping("/emailTrackings")
    public ResponseEntity<List<EmailTrackingDTO>> getListEmailTracking(Pageable pageable, @RequestParam(required = false) Long recipientID, @RequestParam(required = false) String keyword,
                                                                       @RequestParam(required = false) boolean archived) {
        final Page<EmailTrackingDTO> page = emailTrackingService.getListOfEmailTracking(pageable, recipientID, keyword, archived);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @ApiOperation(value = "Get an email tracking by id")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - get data successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @GetMapping("/emailTracking")
    public ResponseEntity<EmailTrackingDTO> getAnEmailTrackingById(@RequestParam Long id) {
        EmailTrackingDTO result = emailTrackingService.findOne(id);
        return ResponseEntity.ok(result);
    }

    @ApiOperation(value = "Update an email tracking")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - update successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PutMapping("/emailTracking")
    public ResponseEntity<EmailTrackingDataDTO> updateAnEmailTracking(@RequestBody EmailTrackingDataDTO emailTrackingDataDTO) {
        emailTrackingDataDTO = emailTrackingService.updateEmailTracking(emailTrackingDataDTO);
        return ResponseEntity.ok(emailTrackingDataDTO);
    }

    @ApiOperation(value = "Delete an email tracking")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - update successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @DeleteMapping("/emailTracking")
    public void deleteAnEmailTracking(@RequestParam Long id) {
        emailTrackingService.delete(id);
    }

    @ApiOperation(value = "resend an email tracking")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - resend successfully"),
        @ApiResponse(code = 400, message = "Bad request - invalid status must be could not be send. key: error.resendError"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PutMapping("/emailTracking/resend")
    public void updateAnEmailTracking(@Valid @RequestBody ResendEmailTrackingDTO resendEmailTrackingDTO) {
        emailTrackingService.resendEmailTracking(resendEmailTrackingDTO);
    }

    @ApiOperation(value = "update an or multiple email tracking")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - update successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PutMapping("/emailTracking/updateStatus")
    public void updateAnEmailTracking(@RequestParam List<Long> ids, @RequestParam EmailStatus emailStatus) {
        emailTrackingService.updateStatusForEmailTracking(ids, emailStatus);
    }

    @ApiOperation(value = "archive an email tracking")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - archive successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PutMapping("/emailTracking/archive")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public void archiveEmailTracking(@RequestParam long id){
        emailTrackingService.archive(id);
    }

    @ApiOperation(value = "Restore an email tracking")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - restore successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PutMapping("/emailTracking/restore")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public void restoreEmailTracking(@RequestParam long id){
        emailTrackingService.restore(id);
    }
}
