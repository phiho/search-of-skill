import { Routes } from '@angular/router';

import { EmailTemplateManagementComponent } from './components/email-template-management.component';
import { EmailTemplateManagementDetailComponent } from './components/email-template-management-detail.component';
import { EmailTemplateManagementUpdateComponent } from './components/email-template-management-update.component';

export const emailTemplateManagementRoute: Routes = [
  {
    path: '',
    component: EmailTemplateManagementComponent,
    data: {
      defaultSort: 'id,asc',
    },
  },
  {
    path: ':id/view',
    component: EmailTemplateManagementDetailComponent,
  },
  {
    path: 'new',
    component: EmailTemplateManagementUpdateComponent,
  },
  {
    path: ':id/edit',
    component: EmailTemplateManagementUpdateComponent,
  },
];
