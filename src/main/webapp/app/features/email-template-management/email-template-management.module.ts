import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SearchOfSkillsSharedModule } from '../../shared/shared.module';
import { EmailTemplateManagementComponent } from './components/email-template-management.component';
import { EmailTemplateManagementDetailComponent } from './components/email-template-management-detail.component';
import { emailTemplateManagementRoute } from './email-template-management.route';
import { EmailTemplateManagementUpdateComponent } from './components/email-template-management-update.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { EmailTemplateDetailDialogComponent } from './components/email-template-detail-dialog.component';

@NgModule({
  imports: [SearchOfSkillsSharedModule, RouterModule.forChild(emailTemplateManagementRoute), CKEditorModule],
  declarations: [
    EmailTemplateManagementComponent,
    EmailTemplateManagementDetailComponent,
    EmailTemplateManagementUpdateComponent,
    EmailTemplateDetailDialogComponent,
  ],
})
export class EmailTemplateManagementModule {}
