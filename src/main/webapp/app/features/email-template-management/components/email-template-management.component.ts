import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, ParamMap, Router } from '@angular/router';

import { ITEMS_PER_PAGE } from '../../../shared/constants/pagination.constants';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDialogComponent } from '../../../shared/dialog/confirm-dialog.component';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { combineLatest } from 'rxjs';
import { EmailTemplate } from '../models/email-template.model';
import { EmailTemplateService } from '../services/email-template.service';
import { loading } from '../../../shared/rxjs/loading';
import { Page } from '../../../shared/models/page.model';
import { HandleRequestService } from '../../../core/handle-request/handle-request.service';

@Component({
  selector: 'jhi-email-template-mgmt',
  templateUrl: './email-template-management.component.html',
})
export class EmailTemplateManagementComponent implements OnInit {
  emailTemplates: EmailTemplate[] = [];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private modalService: NgbModal,
    private translatePipe: TranslatePipe,
    private emailTemplateService: EmailTemplateService,
    private handleRequestService: HandleRequestService
  ) {}

  ngOnInit(): void {
    this.page = 1;
    this.ascending = true;
    this.predicate = 'id';
    this.loadAll();
    this.handleNavigation();
  }

  private loadAll(): void {
    this.emailTemplateService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .pipe(loading())
      .subscribe((res: Page<EmailTemplate>) => this.onSuccess(res));
  }

  private onSuccess(emailTemplatePage: Page<EmailTemplate>): void {
    this.totalItems = emailTemplatePage.total;
    this.emailTemplates = emailTemplatePage.items;
  }
  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  deleteRecipientList(emailTemplate: EmailTemplate): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.delete';
    modalRef.componentInstance.title = 'entity.action.delete';
    modalRef.componentInstance.content = 'emailTemplateManagement.delete.question';
    modalRef.componentInstance.translateValues = { name: emailTemplate.name };

    modalRef.result.then(
      () => {
        this.emailTemplateService
          .delete(emailTemplate.id!)
          .pipe(loading())
          .subscribe(
            () => {
              this.emailTemplates = this.emailTemplates.filter(elm => elm.id !== emailTemplate.id);
              this.toastrService.success(this.translatePipe.transform('emailTemplateManagement.deleted', { param: emailTemplate.id }));
            },
            error => {
              this.handleRequestService.showMessageError('error.actionFailed', error.error);
            }
          );
      },
      () => {}
    );
  }

  private handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      this.page = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      this.predicate = sort[0];
      this.ascending = sort[1] === 'asc';
      this.loadAll();
    }).subscribe();
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
      },
    });
  }

  trackIdentity(index: number, item: EmailTemplate): any {
    return item.id;
  }
}
