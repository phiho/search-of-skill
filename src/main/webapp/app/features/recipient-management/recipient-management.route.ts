import { Routes } from '@angular/router';

import { RecipientManagementComponent } from './components/recipient-management.component';
import { RecipientManagementDetailComponent } from './components/recipient-management-detail.component';
import { RecipientManagementUpdateComponent } from './components/recipient-management-update.component';

export const recipientManagementRoute: Routes = [
  {
    path: '',
    component: RecipientManagementComponent,
    data: {
      defaultSort: 'id,asc',
    },
  },
  {
    path: ':id/view',
    component: RecipientManagementDetailComponent,
    data: {
      defaultSort: 'id,asc',
    },
  },
  {
    path: 'new',
    component: RecipientManagementUpdateComponent,
  },
  {
    path: ':id/edit',
    component: RecipientManagementUpdateComponent,
  },
];
