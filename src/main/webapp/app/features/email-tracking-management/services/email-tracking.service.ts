import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { createRequestOption, Pagination } from '../../../shared/util/request-util';
import { Observable } from 'rxjs';
import { Page } from '../../../shared/models/page.model';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from '../../../app.constants';
import { EmailStatus, EmailTracking } from '../models/email-tracking.model';
import { ResendEmailTracking } from '../models/resend-email-tracking.model';

@Injectable({ providedIn: 'root' })
export class EmailTrackingService {
  public resourceUrl = SERVER_API_URL + 'api';

  constructor(private http: HttpClient) {}

  query(req: Pagination, recipientId: number, keyword: String, archived = false): Observable<Page<EmailTracking>> {
    const url = `${this.resourceUrl}/emailTrackings?`;
    const options = createRequestOption(req)
      .append('keyword', keyword.toString())
      .append('archived', String(archived))
      .append('recipientID', recipientId !== 0 ? recipientId.toString() : '');

    return this.http
      .get<EmailTracking[]>(url, { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<EmailTracking> = {
            items: res.body!,
            total: Number(res.headers.get('X-Total-Count')),
          };
          return page;
        })
      );
  }

  updateStatus(emailTracking: EmailTracking[], emailStatus: EmailStatus): Observable<void> {
    const options: HttpParams = new HttpParams()
      .append('emailStatus', emailStatus)
      .append('ids', emailTracking.map(elm => elm.id).toString());
    return this.http.put<void>(this.resourceUrl + '/emailTracking/updateStatus', null, { params: options });
  }

  delete(id: number): Observable<void> {
    const options: HttpParams = new HttpParams().append('id', id + '');
    return this.http.delete<void>(this.resourceUrl + '/emailTracking', { params: options });
  }

  findById(id: number): Observable<EmailTracking> {
    const options: HttpParams = new HttpParams().append('id', id + '');
    return this.http.get<EmailTracking>(this.resourceUrl + '/emailTracking', { params: options });
  }

  resend(resendEmailTracking: ResendEmailTracking): Observable<void> {
    return this.http.put<void>(this.resourceUrl + '/emailTracking/resend', resendEmailTracking);
  }

  edit(emailTracking: EmailTracking): Observable<void> {
    return this.http.put<void>(this.resourceUrl + '/emailTracking', emailTracking);
  }

  archive(id: number): Observable<void> {
    const options: HttpParams = new HttpParams().append('id', id + '');
    return this.http.put<void>(this.resourceUrl + '/emailTracking/archive', null, { params: options });
  }

  restore(id: number): Observable<void> {
    const options: HttpParams = new HttpParams().append('id', id + '');
    return this.http.put<void>(this.resourceUrl + '/emailTracking/restore', null, { params: options });
  }
}
