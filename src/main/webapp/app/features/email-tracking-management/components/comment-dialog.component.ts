import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EmailTracking } from '../models/email-tracking.model';
import { FormBuilder, Validators } from '@angular/forms';
import { EmailTrackingService } from '../services/email-tracking.service';
import { HandleRequestService } from '../../../core/handle-request/handle-request.service';

@Component({
  selector: 'jhi-comment-dialog-mgmt',
  templateUrl: './comment-dialog.component.html',
})
export class CommentDialogComponent implements OnInit {
  emailTracking!: EmailTracking;
  emailTrackingId!: number;

  editForm = this.fb.group({
    id: [],
    comment: [' ', [Validators.maxLength(16777215)]],
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private modalService: NgbActiveModal,
    private emailTrackingService: EmailTrackingService,
    private handleRequestService: HandleRequestService
  ) {}

  ngOnInit(): void {
    this.emailTrackingService.findById(this.emailTrackingId).subscribe(
      (emailTracking: EmailTracking) => {
        this.emailTracking = emailTracking;
        this.updateForm(emailTracking);
      },
      error => {
        this.handleRequestService.showMessageError('error.actionFailed', error.error);
      }
    );
  }

  back(): void {
    window.history.back();
  }

  cancel(): void {
    this.modalService.dismiss();
  }

  save(): void {
    const emailTracking: EmailTracking = {
      id: this.emailTracking.id,
      comment: this.editForm.get('comment')?.value,
    };
    this.emailTrackingService.edit(emailTracking).subscribe(
      () => {
        this.modalService.close();
        this.handleRequestService.showMessageSuccess('emailTrackingManagement.updated', { param: emailTracking.id });
      },
      error => {
        this.handleRequestService.showMessageError('error.actionFailed', error.error);
      }
    );
  }

  private updateForm(emailTracking: EmailTracking): void {
    this.editForm.patchValue({
      comment: emailTracking.comment,
    });
  }
}
