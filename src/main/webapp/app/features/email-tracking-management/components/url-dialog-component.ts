import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-url-dialog-mgmt',
  templateUrl: './url-dialog.component.html',
})
export class UrlDialogComponent implements OnInit {
  url!: string;

  constructor(private route: ActivatedRoute, private modalService: NgbActiveModal) {}

  ngOnInit(): void {}

  ok(): void {
    this.modalService.dismiss();
  }
}
