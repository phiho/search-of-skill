import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, ParamMap, Params, Router } from '@angular/router';

import { ITEMS_PER_PAGE } from '../../../shared/constants/pagination.constants';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDialogComponent } from '../../../shared/dialog/confirm-dialog.component';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { combineLatest } from 'rxjs';
import { EmailStatus, EmailTracking } from '../models/email-tracking.model';
import { EmailResendDialogComponent } from './email-resend-dialog.component';
import { EmailTrackingService } from '../services/email-tracking.service';
import { Page } from '../../../shared/models/page.model';
import { HandleRequestService } from '../../../core/handle-request/handle-request.service';
import { loading } from '../../../shared/rxjs/loading';
import { CommentDialogComponent } from './comment-dialog.component';
import { FormBuilder, Validators } from '@angular/forms';
import { EmailStatusConstants } from '../../../shared/util/email-status.constants';
import { UrlDialogComponent } from './url-dialog-component';
import { DialogType } from '../../../shared/dialog/dialog-type.enum';

@Component({
  selector: 'jhi-email-tracking-mgmt',
  templateUrl: './email-tracking-management.component.html',
})
export class EmailTrackingManagementComponent implements OnInit {
  emailTrackingList: EmailTracking[] = [];
  selectedEmailTrackings: EmailTracking[] = [];
  editForm = this.fb.group({
    itemPerPage: [20, [Validators.required, Validators.pattern(/^[0-9]*$/), Validators.min(1), Validators.max(1000)]],
    searchById: [null, [Validators.pattern(/^[0-9]*$/)]],
    search: [null],
    searchOption: ['searchByID'],
  });
  searchOptions = [
    { value: 'searchByID', label: 'Search by ID' },
    { value: 'freeSearch', label: 'Search by all fields' },
  ];

  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  selectAll = false;
  statusList = EmailStatusConstants;
  params: Params = [];
  recipientId = 0;
  keyword: String = '';
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private modalService: NgbModal,
    private translatePipe: TranslatePipe,
    private emailTrackingService: EmailTrackingService,
    private handleRequestService: HandleRequestService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.page = 1;
    this.ascending = true;
    this.predicate = 'id';
    this.handleNavigation();
    this.activatedRoute.queryParams.subscribe(params => {
      this.params = params;
      if (params['recipientID'] && params['recipientID'] !== '') {
        this.recipientId = Number(params['recipientID']);
        this.editForm.patchValue({
          searchById: this.recipientId,
        });
      } else if (params['page']) {
        this.page = Number(params['page']);
      } else if (params['search']) {
        this.keyword = params['search'];
        this.editForm.patchValue({
          search: this.keyword,
        });
      } else {
        this.keyword = '';
        this.recipientId = 0;
      }

      this.loadAll();
    });
  }

  private loadAll(): void {
    this.emailTrackingService
      .query(
        {
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort(),
        },
        this.recipientId,
        this.keyword
      )
      .pipe(loading())
      .subscribe(
        (res: Page<EmailTracking>) => {
          this.onSuccess(res);
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  private handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      this.page = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      this.predicate = sort[0];
      this.ascending = sort[1] === 'asc';
    }).subscribe();
  }

  delete(id: number): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.delete';
    modalRef.componentInstance.title = 'entity.action.delete';
    modalRef.componentInstance.content = 'emailTrackingManagement.delete.question';
    modalRef.componentInstance.translateValues = { param: id };

    modalRef.result.then(
      () => {
        this.emailTrackingService
          .delete(id)
          .pipe(loading())
          .subscribe(
            () => {
              this.emailTrackingList = this.emailTrackingList.filter(elm => elm.id !== id);
              this.toastrService.success(this.translatePipe.transform('emailTrackingManagement.deleted', { param: id }));
            },
            error => {
              this.handleRequestService.showMessageError('error.actionFailed', error.error);
            }
          );
      },
      () => {}
    );
  }

  getClassByStatus(status: EmailStatus): string {
    switch (status) {
      case EmailStatus.GOT_ALL:
        return 'status-green';
      case EmailStatus.RECEIVED_COST:
        return 'status-green-border-lila';
      case EmailStatus.RECEIVED_WITHOUT:
      case EmailStatus.RECEIVED_WITH:
        return 'status-green-border-orange';
      case EmailStatus.CONFIDENTIALITY_WITH:
      case EmailStatus.CONFIDENTIALITY_WITHOUT:
      case EmailStatus.BY_MAIL:
        return 'status-orange';
      case EmailStatus.APPROVED_BUT_DONT_INCLUDE_ANY_CV:
        return 'status-blue-bold';
      case EmailStatus.CANCELED:
      case EmailStatus.NOT_READY_YET:
      case EmailStatus.APPEALED:
        return 'status-red';
      case EmailStatus.SENT:
      case EmailStatus.INCOMPLETE:
        return 'status-yellow';
      case EmailStatus.PAYMENT:
      case EmailStatus.COULD_NOT_BE_SENT:
        return 'status-purple';
      case EmailStatus.UNANSWERED:
        return 'status-blue-bold';
      case EmailStatus.COMMENT:
        return 'status-blue';
      default:
        return '';
    }
  }

  public getToolTipByStatus(emailStatus: EmailStatus): string {
    const status = this.statusList.filter(elm => elm.value === emailStatus);
    if (status.length > 0) {
      return status[0].hoverTitle;
    } else return emailStatus.toString();
  }

  markAll(selectAll: boolean): void {
    this.selectAll = selectAll;
    if (selectAll) {
      this.emailTrackingList.forEach(elm => (elm.isSelected = true));
      this.selectedEmailTrackings = this.emailTrackingList;
    } else {
      this.emailTrackingList.forEach(elm => (elm.isSelected = false));
      this.selectedEmailTrackings = [];
    }
  }

  changeStatus(emailStatus: EmailStatus, emailTracking: EmailTracking): void {
    const emailTrackings: EmailTracking[] = [];
    emailTrackings.push(emailTracking);
    this.emailTrackingService
      .updateStatus(emailTrackings, emailStatus)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadAll();
          this.toastrService.success(this.translatePipe.transform('emailTrackingManagement.updated', { param: emailTracking.id }));
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  changeStatusOfSeveralEmailTrackings(emailStatus: EmailStatus): void {
    const emailTrackings = this.selectedEmailTrackings;
    this.emailTrackingService
      .updateStatus(emailTrackings, emailStatus)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadAll();
          this.selectedEmailTrackings = [];
          this.toastrService.success(
            this.translatePipe.transform('emailTrackingManagement.updatedSeveral', { param: emailTrackings.map(elm => elm.id).toString() })
          );
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
          this.selectedEmailTrackings = [];
        }
      );
  }

  showResend(id: number): void {
    const modalRef = this.modalService.open(EmailResendDialogComponent);
    modalRef.componentInstance.emailTrackingId = id;
    modalRef.result.then(
      () => {
        this.loadAll();
      },
      () => {}
    );
  }

  private onSuccess(emailTrackingPage: Page<EmailTracking>): void {
    this.totalItems = emailTrackingPage.total;
    this.emailTrackingList = emailTrackingPage.items;
  }

  transition(): void {
    this.router.navigate(['./admin/email-tracking-management'], { queryParams: { page: this.page } });
  }

  markAnEmailTracking(emailTracking: EmailTracking): void {
    const checked = emailTracking.isSelected;
    if (checked) {
      if (!this.selectedEmailTrackings.includes(emailTracking)) {
        this.selectedEmailTrackings.push(emailTracking);
      }
    } else {
      this.selectedEmailTrackings = this.selectedEmailTrackings.filter(elm => elm.id !== emailTracking.id);
    }
  }

  openCommentDialog(emailTrackingId: number): void {
    const modalRef = this.modalService.open(CommentDialogComponent, { size: 'lg' });
    modalRef.componentInstance.emailTrackingId = emailTrackingId;
    modalRef.result.then(
      () => {
        this.loadAll();
      },
      () => {}
    );
  }

  openUrlDialog(url: string): void {
    const modalRef = this.modalService.open(UrlDialogComponent, { size: 'lg' });
    modalRef.componentInstance.url = url;
    modalRef.result.then(
      () => {},
      () => {}
    );
  }

  changeItemsPerPage(): void {
    this.itemsPerPage = this.editForm.get('itemPerPage')!.value;
    this.loadAll();
  }

  searchByRecipientID(): void {
    const recipientId = this.editForm.get('searchById')!.value;
    this.router.navigate(['./admin/email-tracking-management'], { queryParams: { recipientID: recipientId } });
  }

  search(): void {
    const search = this.editForm.get('search')!.value;
    this.router.navigate(['./admin/email-tracking-management'], { queryParams: { search } });
  }

  changeOption($event: any): void {
    if ($event.value === 'searchByID') {
      this.keyword = '';
      this.editForm.patchValue({
        search: null,
      });
      this.router.navigate(['./admin/email-tracking-management'], { queryParams: { recipientID: this.recipientId } });
    } else {
      this.recipientId = 0;
      this.editForm.patchValue({
        searchById: null,
      });
      this.router.navigate(['./admin/email-tracking-management'], { queryParams: { search: this.keyword } });
    }
  }

  archive(id: number, recipientID: number): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = 'entity.action.ok';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'emailTrackingManagement.archive.question';
    modalRef.componentInstance.translateValues = { param: recipientID };

    modalRef.result.then(
      () => {
        this.emailTrackingService
          .archive(id)
          .pipe(loading())
          .subscribe(
            () => {
              this.emailTrackingList = this.emailTrackingList.filter(elm => elm.id !== id);
              this.handleRequestService.showMessageSuccess('emailTrackingManagement.archived', { param: recipientID });
            },
            error => {
              this.handleRequestService.showMessageError('error.actionFailed', error.error);
            }
          );
      },
      () => {}
    );
  }
}
