export interface ResendEmailTracking {
  id?: number;
  email?: string;
  registrar?: string;
}
