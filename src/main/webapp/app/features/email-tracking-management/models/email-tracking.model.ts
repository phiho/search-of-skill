import { CronjobDataDTO } from '../../cronjob-management/models/cronjob.model';
import { Recipient } from '../../recipient-management/models/recipient.model';

export interface EmailTracking {
  id?: number;
  cronJob?: CronjobDataDTO;
  recipient?: Recipient;
  reminderDate1?: Date;
  reminderDate2?: Date;
  sentReminder1?: boolean;
  sentReminder2?: boolean;
  status?: EmailStatus;
  deleted?: boolean;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
  isSelected?: boolean;
  comment?: string;
  registrar?: string;
}

export enum EmailStatus {
  GOT_ALL = 'GOT_ALL',
  RECEIVED_WITHOUT = 'RECEIVED_WITHOUT',
  RECEIVED_WITH = 'RECEIVED_WITH',
  RECEIVED_COST = 'RECEIVED_COST',
  CONFIDENTIALITY_WITHOUT = 'CONFIDENTIALITY_WITHOUT',
  CONFIDENTIALITY_WITH = 'CONFIDENTIALITY_WITH',
  APPROVED_BUT_DONT_INCLUDE_ANY_CV = 'APPROVED_BUT_DONT_INCLUDE_ANY_CV',
  CANCELED = 'CANCELED',
  NOT_READY_YET = 'NOT_READY_YET',
  APPEALED = 'APPEALED',
  SENT = 'SENT',
  PAYMENT = 'PAYMENT',
  COMMENT = 'COMMENT',
  UNANSWERED = 'UNANSWERED',
  COULD_NOT_BE_SENT = 'COULD_NOT_BE_SENT',
  BY_MAIL = 'BY_MAIL',
  INCOMPLETE = 'INCOMPLETE',
}
