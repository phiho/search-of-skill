import { CronjobDataDTO } from './cronjob.model';

export interface CronjobStatistic {
  cronjob?: CronjobDataDTO;
  totalEmailToSend?: number;
  emailSent?: number;
  reminderSent1?: number;
  reminderSent2?: number;
  unansweredEmail?: number;
}
