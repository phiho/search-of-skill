import { CronJobStatus, Frequency, WeekDay } from './cronjob.model';

export interface CronjobDTO {
  id?: number;
  name?: string;
  recipientListId?: number;
  templateMainId?: number;
  templateReminder1Id?: number;
  templateReminder2Id?: number;
  templateReminderTime1?: number;
  templateReminderTime2?: number;
  chunkNumber?: number;
  weekOfMonth?: number;
  weekday?: WeekDay;
  status?: CronJobStatus;
  sendHour?: number;
  startDate?: Date;
  endDate?: Date;
  deleted?: boolean;
  archived?: boolean;
  frequency?: Frequency;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}
