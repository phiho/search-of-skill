import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { SERVER_API_URL } from '../../../app.constants';
import { EmailTemplate } from '../../email-template-management/models/email-template.model';
import { Observable } from 'rxjs';
import { RecipientList } from '../../recipient-management/models/recipientList.model';
import { createRequestOption, Pagination } from '../../../shared/util/request-util';
import { Page } from '../../../shared/models/page.model';
import { map } from 'rxjs/operators';
import { CronjobDataDTO } from '../models/cronjob.model';
import { CronjobDTO } from '../models/cronjob-dto.model';
import { CronjobStatistic } from '../models/cronjob-statistic.model';

@Injectable({ providedIn: 'root' })
export class CronjobService {
  public resourceUrl = SERVER_API_URL + 'api';

  constructor(private http: HttpClient) {}

  create(cronjobDTO: CronjobDTO): Observable<EmailTemplate> {
    return this.http.post<RecipientList>(this.resourceUrl + '/cronjob', cronjobDTO);
  }

  query(req?: Pagination, isArchived?: boolean): Observable<Page<CronjobDataDTO>> {
    const options = createRequestOption(req).append('isArchived', isArchived + '');
    return this.http
      .get<CronjobDataDTO[]>(this.resourceUrl + '/cronjobs', { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<CronjobDataDTO> = {
            items: res.body!,
            total: Number(res.headers.get('X-Total-Count')),
          };
          return page;
        })
      );
  }

  delete(id: number): Observable<{}> {
    const options: HttpParams = new HttpParams().append('id', id + '');
    return this.http.delete(this.resourceUrl + '/cronjob', { params: options });
  }

  archive(id: number): Observable<void> {
    const options: HttpParams = new HttpParams().append('id', id + '');
    return this.http.put<void>(this.resourceUrl + '/cronjob/archive', null, { params: options });
  }

  restore(id: number): Observable<void> {
    const options: HttpParams = new HttpParams().append('id', id + '');
    return this.http.put<void>(this.resourceUrl + '/cronjob/restore', null, { params: options });
  }

  cancel(id: number): Observable<void> {
    const options: HttpParams = new HttpParams().append('id', id + '');
    return this.http.put<void>(this.resourceUrl + '/cronjob/cancel', null, { params: options });
  }

  resume(id: number): Observable<void> {
    const options: HttpParams = new HttpParams().append('id', id + '');
    return this.http.put<void>(this.resourceUrl + '/cronjob/resume', null, { params: options });
  }

  getEndDateOverlap(endDate: Date): Observable<Date> {
    const options: HttpParams = new HttpParams().append('endDate', endDate.toISOString());
    return this.http.get<Date>(this.resourceUrl + '/cronjob/getEndDateOverlap', { params: options });
  }

  findStatistic(id: number): Observable<CronjobStatistic> {
    const options: HttpParams = new HttpParams().append('cronJobId', id + '');
    return this.http.get<CronjobStatistic>(this.resourceUrl + '/cronjob/statistic', { params: options });
  }

  getCronjobByName(req?: Pagination, name?: string): Observable<Page<CronjobDataDTO>> {
    let options;
    if (name) {
      options = createRequestOption(req).append('name', name);
    } else {
      options = createRequestOption(req);
    }
    return this.http
      .get<CronjobDataDTO[]>(this.resourceUrl + '/cronjob/all', { params: options, observe: 'response' })
      .pipe(
        map(res => {
          console.error(res);
          const page: Page<CronjobDataDTO> = {
            items: res.body!,
            total: Number(res.headers.get('X-Total-Count')),
          };
          return page;
        })
      );
  }
}
