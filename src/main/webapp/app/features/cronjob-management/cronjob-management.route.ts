import { Routes } from '@angular/router';

import { CronjobManagementComponent } from './components/cronjob-management.component';
import { CronjobManagementUpdateComponent } from './components/cronjob-management-update.component';
import { CronjobStatisticComponent } from './components/cronjob-statistic.component';
import { ArchiveCronjobManagementComponent } from './components/archive-cronjob-management.component';

export const cronjobManagementRoute: Routes = [
  {
    path: '',
    component: CronjobManagementComponent,
    data: {
      defaultSort: 'id,asc',
    },
  },
  {
    path: 'archive',
    component: ArchiveCronjobManagementComponent,
    data: {
      defaultSort: 'id,asc',
    },
  },
  {
    path: 'new',
    component: CronjobManagementUpdateComponent,
  },
  {
    path: ':id/statistic',
    component: CronjobStatisticComponent,
  },
];
