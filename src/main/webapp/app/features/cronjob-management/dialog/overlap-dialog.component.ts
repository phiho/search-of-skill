import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-overlap-dialog-modal',
  templateUrl: './overlap-dialog.component.html',
})
export class OverlapDialogComponent {
  title = 'Notification';
  content = '';
  translateValues = {};
  rightButton = 'entity.action.save';
  type = 'DELETE';
  leftButton = 'entity.action.cancel';
  startDate!: Date;
  isSelectedNewStartDate = false;

  constructor(public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirm(): void {
    this.activeModal.close(this.isSelectedNewStartDate);
  }

  changeSelectStartDate(checked: boolean): void {
    this.isSelectedNewStartDate = checked;
  }
}
