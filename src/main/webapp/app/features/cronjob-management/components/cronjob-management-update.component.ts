import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { CronjobService } from '../services/cronjob.service';
import { ToastrService } from 'ngx-toastr';
import { TranslatePipe } from '@ngx-translate/core';
import { CronjobDataDTO, Frequency, WeekDay } from '../models/cronjob.model';
import { RecipientList } from '../../recipient-management/models/recipientList.model';
import { EmailTemplate, EmailTemplateType } from '../../email-template-management/models/email-template.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RecipientDetailDialogComponent } from '../../recipient-management/components/recipient-detail-dialog.component';
import { EmailTemplateDetailDialogComponent } from '../../email-template-management/components/email-template-detail-dialog.component';
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons';
import { RecipientService } from '../../recipient-management/services/recipient.service';
import { loading } from '../../../shared/rxjs/loading';
import { Page } from '../../../shared/models/page.model';
import { HandleRequestService } from '../../../core/handle-request/handle-request.service';
import { EmailTemplateService } from '../../email-template-management/services/email-template.service';
import { Recipient } from '../../recipient-management/models/recipient.model';
import * as moment from 'moment';
import { Moment } from 'moment';
import { CronjobDTO } from '../models/cronjob-dto.model';
import { DialogType } from '../../../shared/dialog/dialog-type.enum';
import { DatePipe } from '@angular/common';
import { getCurrentDay, getCurrentDayString, getCurrentDayValue } from 'app/shared/util/day-util';
import { OverlapDialogComponent } from '../dialog/overlap-dialog.component';

@Component({
  selector: 'jhi-cronjob-mgmt-update',
  templateUrl: './cronjob-management-update.component.html',
})
export class CronjobManagementUpdateComponent implements OnInit {
  cronjob!: CronjobDTO;
  faCalendarAlt = faCalendarAlt;
  recipientLists: RecipientList[] = [];
  cronjobList: CronjobDataDTO[] = [];
  mainTemplates: EmailTemplate[] = [];
  reminderTemplates1: EmailTemplate[] = [];
  reminderTemplates2: EmailTemplate[] = [];
  dates = [
    { value: 'MON', content: 'Monday' },
    { value: 'TUE', content: 'Tuesday' },
    { value: 'WED', content: 'Wednesday' },
    { value: 'THU', content: 'Thursday' },
    { value: 'FRI', content: 'Friday' },
    { value: 'SAT', content: 'Saturday' },
    { value: 'SUN', content: 'Sunday' },
  ];

  times = [
    { value: 0, content: '00:00' },
    { value: 1, content: '01:00' },
    { value: 2, content: '02:00' },
    { value: 3, content: '03:00' },
    { value: 4, content: '04:00' },
    { value: 5, content: '05:00' },
    { value: 6, content: '06:00' },
    { value: 7, content: '07:00' },
    { value: 8, content: '08:00' },
    { value: 9, content: '09:00' },
    { value: 10, content: '10:00' },
    { value: 11, content: '11:00' },
    { value: 12, content: '12:00' },
    { value: 13, content: '13:00' },
    { value: 14, content: '14:00' },
    { value: 15, content: '15:00' },
    { value: 16, content: '16:00' },
    { value: 17, content: '17:00' },
    { value: 18, content: '18:00' },
    { value: 19, content: '19:00' },
    { value: 20, content: '20:00' },
    { value: 21, content: '21:00' },
    { value: 22, content: '22:00' },
    { value: 23, content: '23:00' },
    { value: 24, content: '00:00' },
  ];

  editForm = this.fb.group({
    name: [undefined, [Validators.required, Validators.maxLength(255)]],
    recipientList: [undefined, Validators.required],
    frequency: ['ONCE_PER_WEEK', Validators.required],
    template: [undefined, Validators.required],
    templateReminder1: [undefined, Validators.required],
    templateReminderTime1: [7, [Validators.required, Validators.min(1)]],
    templateReminder2: [undefined, Validators.required],
    templateReminderTime2: [14, [Validators.required, Validators.min(1)]],
    chunkNumber: [undefined, [Validators.required, Validators.min(1)]],
    weekday: [getCurrentDay(), Validators.required],
    endDate: [undefined, Validators.required],
    startDate: [undefined, Validators.required],
    sendHour: [new Date().getHours() + 1, Validators.required],
    weekOfMonth: [1],
  });
  lessThanTemplateReminder1 = false;
  greaterThanTemplateReminder2 = false;
  minDate!: any;
  totalRecipients = 0;
  weekNumbers = [
    { value: 1, content: '1st week' },
    { value: 2, content: '2nd week' },
    { value: 3, content: '3rd week' },
    { value: 4, content: '4th week' },
  ];
  endDate!: Moment;
  startDate!: Moment;
  isSaving = false;
  invalidStartDate = false;

  constructor(
    private translatePipe: TranslatePipe,
    private toastrService: ToastrService,
    private cronjobService: CronjobService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private recipientService: RecipientService,
    private handleRequestService: HandleRequestService,
    private emailTemplateService: EmailTemplateService,
    private datePipe: DatePipe
  ) {}

  ngOnInit(): void {
    const currentDate = new Date();
    this.minDate = { year: currentDate.getFullYear(), month: currentDate.getMonth() + 1, day: currentDate.getDate() };
    this.loadRecipientListByName('');
    this.loadCronjobByName('');
    this.loadTemplateByTypeAndName(EmailTemplateType.MAIN.toString(), '');
    this.loadTemplateByTypeAndName(EmailTemplateType.REMINDER_1.toString(), '');
    this.loadTemplateByTypeAndName(EmailTemplateType.REMINDER_2.toString(), '');

    this.cronjob = {};
    this.updateForm(this.cronjob);
  }

  loadRecipientListByName(name: string): void {
    this.recipientService
      .query(
        {
          page: 0,
          size: 20,
          sort: ['name,asc'],
        },
        name
      )
      .pipe(loading())
      .subscribe(
        (recipientListPage: Page<RecipientList>) => {
          this.recipientLists = recipientListPage.items;
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  loadCronjobByName(name: string): void {
    this.cronjobService
      .getCronjobByName(
        {
          page: 0,
          size: 20,
          sort: ['name,asc'],
        },
        name
      )
      .pipe(loading())
      .subscribe(
        (cronjobList: Page<CronjobDataDTO>) => {
          this.cronjobList = cronjobList.items;
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.updateCronjob(this.cronjob);
    this.cronjobService.getEndDateOverlap(this.cronjob.endDate!).subscribe(
      (endDate: Date) => {
        if (endDate) {
          const modalRef = this.modalService.open(OverlapDialogComponent, { size: 'lg' });
          modalRef.componentInstance.rightButton = 'entity.action.save';
          modalRef.componentInstance.content = 'cronjobManagement.warning.question';
          modalRef.componentInstance.type = DialogType.WARNING;

          // Calculate new suggest start date base on overlap end date
          const weekday = this.editForm.get('weekday')?.value;
          const time = this.editForm.get('sendHour')?.value;
          const frequency = this.editForm.get('frequency')?.value;
          const weekOfMonth = this.editForm.get('weekOfMonth')?.value;
          const chunkNumber = this.editForm.get('chunkNumber')?.value;
          // const newStartDate = this.getFirstSendDateFromSuggestEndDate(time, weekday, frequency, weekOfMonth, moment(endDate));
          const newStartDate = this.getFirstSendDate(time, weekday, frequency, weekOfMonth, moment(endDate));
          const end = this.getEndSendDate(time, weekday, newStartDate, frequency, weekOfMonth, chunkNumber, this.totalRecipients);
          modalRef.componentInstance.startDate = newStartDate.toDate();
          modalRef.componentInstance.translateValues = { date: this.datePipe.transform(endDate, 'dd/MM/yyyy') };

          modalRef.result.then(
            (isSelectedNewStartDate: boolean) => {
              if (isSelectedNewStartDate) {
                this.cronjob.startDate = newStartDate.toDate();
                this.cronjob.endDate = end.toDate();
              }
              this.saveCronjob();
            },
            () => {}
          );
        } else {
          this.saveCronjob();
        }
      },
      error => {
        this.handleRequestService.showMessageError('error.actionFailed', error.error);
      }
    );
  }

  private updateForm(cronjob: CronjobDataDTO): void {
    this.editForm.patchValue({
      id: cronjob.id,
    });
  }

  viewDetail(event: MouseEvent, recipientList: RecipientList): void {
    event.stopPropagation();
    const modalRef = this.modalService.open(RecipientDetailDialogComponent, { size: 'xl', windowClass: 'modal-detail-custom' });
    modalRef.componentInstance.recipientList = recipientList;
  }

  viewDetailTemplate(event: MouseEvent, emailTemplateId: number): void {
    event.stopPropagation();
    const modalRef = this.modalService.open(EmailTemplateDetailDialogComponent, { size: 'lg', windowClass: 'modal-detail-custom' });
    modalRef.componentInstance.emailTemplateId = emailTemplateId;
  }

  selectSchedule(): boolean {
    const value = this.editForm.get('frequency')!.value;
    return value !== 'ONCE_PER_MONTH';
  }

  loadTemplateByTypeAndName(type: string, name: string): void {
    this.emailTemplateService
      .query(
        {
          page: 0,
          size: 5,
          sort: ['name,asc'],
        },
        name,
        type
      )
      .pipe(loading())
      .subscribe((emailTemplatePage: Page<EmailTemplate>) => {
        const emailTemplates = emailTemplatePage.items;
        if (type === EmailTemplateType.MAIN) {
          this.mainTemplates = emailTemplates.filter(elm => elm.type === EmailTemplateType.MAIN);
        } else if (type === EmailTemplateType.REMINDER_1) {
          this.reminderTemplates1 = emailTemplates.filter(elm => elm.type === EmailTemplateType.REMINDER_1);
        } else if (type === EmailTemplateType.REMINDER_2) {
          this.reminderTemplates2 = emailTemplates.filter(elm => elm.type === EmailTemplateType.REMINDER_2);
        }
      });
  }

  selectRecipientList(emailTemplate: EmailTemplate): void {
    // call this service to get the total of recipients in list
    if (!emailTemplate) return;
    this.recipientService
      .findAllRecipientsByRecipientListId(
        {
          page: 0,
          size: 1,
          sort: ['id,asc'],
        },
        emailTemplate.id
      )
      .subscribe((recipientPage: Page<Recipient>) => {
        this.totalRecipients = recipientPage.total;
        this.startDate = moment();
        this.editForm.patchValue({
          chunkNumber: this.totalRecipients,
          startDate: moment(),
        });
      });
  }

  public calculateEndDate(): void {
    const weekday = this.editForm.get('weekday')?.value;
    const time = this.editForm.get('sendHour')?.value;
    const frequency = this.editForm.get('frequency')?.value;
    const weekOfMonth = this.editForm.get('weekOfMonth')?.value;
    const chunkNumber = this.editForm.get('chunkNumber')?.value;
    const start: Moment = this.editForm.get('startDate')?.value;
    if (chunkNumber && chunkNumber <= this.totalRecipients && time && weekday && frequency) {
      const firstSendDate = this.getFirstSendDate(time, weekday, frequency, weekOfMonth, start);
      this.startDate = firstSendDate;
      // if calculated start date is not same start date in form will notice
      if (frequency === Frequency.ONCE_PER_MONTH && !firstSendDate.isSame(start, 'day')) {
        this.invalidStartDate = true;
      } else {
        this.invalidStartDate = false;
      }
      const end = this.getEndSendDate(time, weekday, start, frequency, weekOfMonth, chunkNumber, this.totalRecipients);
      this.endDate = end;
      this.editForm.patchValue({
        endDate: end,
      });
    }
  }

  public calculateEndDateAfterChangeChunkNumber(): void {
    const time = this.editForm.get('sendHour')?.value;
    const weekday = this.editForm.get('weekday')?.value;
    const frequency = this.editForm.get('frequency')?.value;
    const weekOfMonth = this.editForm.get('weekOfMonth')?.value;
    const chunkNumber = this.editForm.get('chunkNumber')?.value;
    if (chunkNumber && chunkNumber <= this.totalRecipients && time && weekday && frequency) {
      const end = this.getEndSendDate(time, weekday, this.startDate, frequency, weekOfMonth, chunkNumber, this.totalRecipients);
      this.endDate = end;
      this.editForm.patchValue({
        endDate: end,
      });
    }
  }

  public selectStartDate(): void {
    this.invalidStartDate = false;
    let startDate: Moment = this.editForm.get('startDate')?.value;
    const day = getCurrentDayString(startDate.isoWeekday());
    let time = this.editForm.get('sendHour')?.value;
    // If hours of start date less than current hours and is current day we need to set the default time is next hours of current hour
    if (startDate && startDate.isSame(moment(), 'day') && time <= moment().hours()) {
      time = moment().hours() + 1;
      this.editForm.patchValue({
        sendHour: time,
      });
    }
    const frequency = this.editForm.get('frequency')?.value;
    const weekOfMonth = this.editForm.get('weekOfMonth')?.value;
    const chunkNumber = this.editForm.get('chunkNumber')?.value;
    if (chunkNumber && chunkNumber <= this.totalRecipients && time && day && frequency) {
      startDate = startDate.hours(time);
      this.startDate = startDate;
      // if is ONCE_PER_MONTH we need to check it must send in the correct week of month
      if (frequency === Frequency.ONCE_PER_MONTH && !this.isSentInWeekOfMonth(startDate, weekOfMonth)) {
        this.invalidStartDate = true;
      } else {
        this.invalidStartDate = false;
      }
      const end = this.getEndSendDate(time, day, startDate, frequency, weekOfMonth, chunkNumber, this.totalRecipients);
      this.endDate = end;
      this.editForm.patchValue({
        weekday: day,
        endDate: end,
      });
    }
  }

  private isSentInWeekOfMonth(date: Moment, weekOfMonth: number): boolean {
    let currentWeek = Math.ceil(date.date() / 7);
    if (currentWeek > 4) currentWeek = 4;
    return weekOfMonth === currentWeek;
  }

  public calculateChunkNumber(): void {
    const weekday = this.editForm.get('weekday')?.value;
    const time = this.editForm.get('sendHour')?.value;
    const frequency = this.editForm.get('frequency')?.value;
    const weekOfMonth = this.editForm.get('weekOfMonth')?.value;
    const end = this.editForm.get('endDate')?.value;
    const number = this.getChunkNumber(time, weekday, frequency, weekOfMonth, end, this.totalRecipients);
    this.editForm.patchValue({
      chunkNumber: number,
      endDate: end,
    });
  }

  public isSendInCurrentDay(weekDay: WeekDay, time: number): boolean {
    const firstSendDate = moment().isoWeekday(weekDay);
    return moment().date() === firstSendDate.date() && time > moment().hours();
  }

  public isSendInCurrentWeek(weekDay: WeekDay, time: number): boolean {
    const firstSendDate = moment().isoWeekday(weekDay);
    return moment().date() < firstSendDate.date() || (moment().date() === firstSendDate.date() && time > moment().hours());
  }

  public isSendInThisWeek(firstSendDate: Moment): boolean {
    return moment().week() === firstSendDate.week();
  }

  // check if it will be sent in this current month or not
  public isSendInCurrentMonth(weekDay: WeekDay, time: number, weekOfMonth: number): boolean {
    let currentWeek = Math.ceil(moment().date() / 7);
    if (currentWeek > 4) currentWeek = 4;
    if (weekOfMonth > currentWeek) {
      return true;
    } else if (currentWeek === weekOfMonth) {
      // if in the same month we will check if it sends in this current week or not
      return this.isSendInCurrentWeek(weekDay, time);
    }
    return false;
  }

  // check if it will be sent in this current month or not
  public isSendInSpecificMonth(weekDay: WeekDay, time: number, weekOfMonth: number, date: Moment): boolean {
    let currentWeek = Math.ceil(date.date() / 7);
    if (currentWeek > 4) currentWeek = 4;
    if (weekOfMonth > currentWeek) {
      return true;
    } else if (currentWeek === weekOfMonth) {
      // if in the same month we will check if it sends in this current week or not
      return this.isSendInCurrentWeek(weekDay, time);
    }
    return false;
  }

  public getFirstSendDate(time: number, weekDay: WeekDay, frequency: Frequency, weekOfMonth: number, startDate: Moment): Moment {
    let firstSendDate = moment(startDate);
    firstSendDate = firstSendDate.isoWeekday(weekDay).startOf('days');

    if (frequency !== Frequency.ONCE_PER_MONTH) {
      // if it doesn't send in current week it will send in the next week
      if (!this.isSendInCurrentWeek(weekDay, time)) {
        firstSendDate = firstSendDate.add(1, 'weeks');
      }
    } else {
      if (!this.isSendInSpecificMonth(weekDay, time, weekOfMonth, firstSendDate)) {
        // if it doesn't send in current month it will send in the next month
        firstSendDate = firstSendDate.add(1, 'months').startOf('month');
        const startDay = firstSendDate.weekday();
        // if it doesn't send in current week it will added more one week
        if (startDay > getCurrentDayValue(weekDay)) {
          weekOfMonth = weekOfMonth + 1;
        }
      } else {
        firstSendDate = firstSendDate.startOf('month');
        // if it sends in current month we will set the weekOfMonth for End Date in this current month
        const startDay = firstSendDate.weekday();
        // if the start day of month (of end date) is less than current day it will be subtract one week

        if (startDay > getCurrentDayValue(weekDay)) {
          weekOfMonth = weekOfMonth + 1;
        }
      }
      firstSendDate = firstSendDate.add(weekOfMonth - 1, 'weeks').isoWeekday(weekDay);
    }
    return firstSendDate.hours(time);
  }

  public getEndSendDate(
    time: number,
    weekday: WeekDay,
    firstSendDate: Moment,
    frequency: Frequency,
    weekOfMonth: number,
    chunkNumber: number,
    totalRecipients: number
  ): Moment {
    let endDate = firstSendDate ? moment(firstSendDate) : moment();
    // get the times to finish sending all emails
    let timeToSendAll = Math.ceil(totalRecipients / chunkNumber);
    if (timeToSendAll > 1) {
      timeToSendAll = timeToSendAll - 1;
      let addedWeek = timeToSendAll;
      if (frequency === Frequency.ONCE_PER_WEEK) {
        endDate = endDate.add(addedWeek, 'weeks');
      } else if (frequency === Frequency.ONCE_PER_TWO_WEEK) {
        addedWeek = timeToSendAll * 2;
        endDate = endDate.add(addedWeek, 'weeks');
      } else if (frequency === Frequency.ONCE_PER_MONTH) {
        endDate = endDate.add(timeToSendAll, 'months').startOf('months');
        const startDay = endDate.weekday();
        // if the start day of month (of end date) is less than current day it will be subtract one week
        if (startDay <= getCurrentDayValue(weekday)) {
          weekOfMonth = weekOfMonth - 1;
        }
        endDate = endDate.add(weekOfMonth, 'weeks').isoWeekday(weekday);
      }
    }
    return endDate.hours(time);
  }

  public getChunkNumber(
    time: number,
    weekday: WeekDay,
    frequency: Frequency,
    weekOfMonth: number,
    endDate: Moment,
    totalRecipients: number
  ): number {
    if (this.endDate.dayOfYear() === endDate.dayOfYear() && endDate.year() === this.endDate.year()) {
      return this.editForm.get('chunkNumber')?.value;
    }
    const start: Moment = this.editForm.get('startDate')?.value;
    const firstSendDate = this.getFirstSendDate(time, weekday, frequency, weekOfMonth, start);
    const numberOfWeeks = Math.ceil(endDate.diff(firstSendDate, 'day') / 7);
    const frequencyValue = frequency === Frequency.ONCE_PER_TWO_WEEK ? 2 : frequency === Frequency.ONCE_PER_MONTH ? 4 : 1;
    const remainOfTimeSend = Math.floor(numberOfWeeks / frequencyValue);
    let numberOfTimeSend = Math.ceil(numberOfWeeks / frequencyValue);
    const isSentInCurrentWeek = this.isSendInThisWeek(firstSendDate);
    let chunkNumber = 0;
    if (remainOfTimeSend > 0) {
      // if cronjob doesn't send in current week it needs more a time to finish to send
      if (!isSentInCurrentWeek) {
        numberOfTimeSend++;
      }
      if (frequencyValue === 4) {
        numberOfTimeSend--;
        this.endDate = firstSendDate
          .add(numberOfTimeSend, 'months')
          .startOf('month')
          .add(weekOfMonth, 'weeks')
          .isoWeekday(weekday)
          .hours(time);
      } else {
        this.endDate = firstSendDate.add(numberOfTimeSend * frequencyValue, 'weeks');
      }

      // if end date is greater than deadline the chunk number will be recalculated by decreasing the number of time send 1 unit
      if (this.endDate.isAfter(endDate, 'day')) {
        chunkNumber = Math.ceil(totalRecipients / numberOfTimeSend);
      } else {
        numberOfTimeSend++;
        chunkNumber = Math.ceil(totalRecipients / numberOfTimeSend);
      }
    } else {
      this.endDate = firstSendDate;
      chunkNumber = totalRecipients;
    }
    return chunkNumber;
  }

  private updateCronjob(cronjob: CronjobDTO): void {
    cronjob.name = this.editForm.get('name')?.value;
    cronjob.templateMainId = this.editForm.get('template')?.value;
    cronjob.recipientListId = this.editForm.get('recipientList')?.value;
    cronjob.templateReminder1Id = this.editForm.get('templateReminder1')?.value;
    cronjob.templateReminder2Id = this.editForm.get('templateReminder2')?.value;
    cronjob.chunkNumber = this.editForm.get('chunkNumber')?.value;
    cronjob.frequency = this.editForm.get('frequency')?.value;
    cronjob.weekday = this.editForm.get('weekday')?.value;
    // get timeZone
    const timeZone = this.endDate?.toDate().getTimezoneOffset() / 60;
    let sendHour = this.editForm.get('sendHour')?.value + timeZone;
    // convert to UTC time zone
    if (sendHour < 0) {
      sendHour = 24 + sendHour;
      // get last day
      cronjob.weekday = getCurrentDayString(getCurrentDayValue(cronjob.weekday!.toString()) - 1);
    } else if (sendHour > 23) {
      sendHour = sendHour - 24;
      // get next day
      cronjob.weekday = getCurrentDayString(getCurrentDayValue(cronjob.weekday!.toString()) + 1);
    }
    cronjob.sendHour = sendHour;
    cronjob.endDate = this.endDate.toDate();
    cronjob.startDate = this.startDate.toDate();
    cronjob.weekOfMonth = this.editForm.get('weekOfMonth')?.value;
    cronjob.templateReminderTime1 = this.editForm.get('templateReminderTime1')?.value;
    cronjob.templateReminderTime2 = this.editForm.get('templateReminderTime2')?.value;
  }

  private onSaveSuccess(cronjobDTO: CronjobDTO): void {
    this.isSaving = true;
    this.previousState();
    this.handleRequestService.showMessageSuccess('cronjobManagement.created', { param: cronjobDTO.id });
  }

  private onSaveError(error: any): void {
    this.isSaving = true;
    this.handleRequestService.showMessageError('error.actionFailed', error.error);
  }

  private saveCronjob(): void {
    this.isSaving = true;
    this.cronjobService
      .create(this.cronjob)
      .pipe(loading())
      .subscribe(
        (cronjobDTO: CronjobDTO) => this.onSaveSuccess(cronjobDTO),
        error => this.onSaveError(error)
      );
  }

  getCurrentWeekOfMonth(): string {
    const currentWeekOfMonth = this.editForm.get('weekOfMonth')?.value;
    const weekOfMonth = this.weekNumbers.filter(elm => elm.value === currentWeekOfMonth);
    return weekOfMonth.length > 0 ? weekOfMonth[0].content : '';
  }
}
