import { NgModule } from '@angular/core';
import { SearchOfSkillsSharedLibsModule } from './shared-libs.module';
import { FindLanguageFromKeyPipe } from './language/find-language-from-key.pipe';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { LoginModalComponent } from './login/login.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { LOADING_STORE_PROVIDER } from 'app/shared/store/loading.store';
import { LoadingComponent } from 'app/shared/loading/loading.component';
import { ConfirmDialogComponent } from 'app/shared/dialog/confirm-dialog.component';
import { TranslatePipe } from '@ngx-translate/core';
import { WeekDayPipe } from 'app/shared/util/week-day.pipe';
import { NgSelectModule } from '@ng-select/ng-select';
import { HourPipe } from 'app/shared/util/hour.pipe';
import { EmailStatusPipe } from 'app/shared/util/email-status.pipe';
import { NoWhitespaceDirective } from 'app/shared/directives/no-white-space/no-whitespace.directive';

@NgModule({
  imports: [SearchOfSkillsSharedLibsModule],
  declarations: [
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective,
    LoadingComponent,
    ConfirmDialogComponent,
    WeekDayPipe,
    HourPipe,
    EmailStatusPipe,
    NoWhitespaceDirective,
  ],
  entryComponents: [LoginModalComponent, ConfirmDialogComponent],
  exports: [
    SearchOfSkillsSharedLibsModule,
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective,
    LoadingComponent,
    ConfirmDialogComponent,
    WeekDayPipe,
    NgSelectModule,
    HourPipe,
    EmailStatusPipe,
    NoWhitespaceDirective,
  ],
  providers: [...LOADING_STORE_PROVIDER, TranslatePipe],
})
export class SearchOfSkillsSharedModule {}
