import { finalize } from 'rxjs/operators';
import { Observable, OperatorFunction } from 'rxjs';
import { loadingStore } from '../store/loading.store';

export function loading<T>(): OperatorFunction<T, T> {
  return (source: Observable<T>) => {
    return new Observable((subscriber: any) => {
      loadingStore.showLoading();
      return source.pipe(finalize(() => loadingStore.hideLoading())).subscribe(subscriber);
    });
  };
}
