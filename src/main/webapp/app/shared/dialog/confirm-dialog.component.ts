import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-confirm-dialog-modal',
  templateUrl: './confirm-dialog.component.html',
})
export class ConfirmDialogComponent {
  title = 'Notification';
  content = '';
  translateValues = {};
  rightButton = 'entity.action.save';
  type = 'DELETE';
  leftButton = 'entity.action.cancel';

  constructor(public activeModal: NgbActiveModal) {}

  fnCallBackCancel = () => {
    console.error('fnCallBackCancel');
  };

  fnCallBackConfirm = () => {
    console.error('fnCallBackConfirm');
  };

  cancel(): void {
    this.activeModal.dismiss();
    this.fnCallBackCancel();
  }

  confirm(): void {
    this.activeModal.close();
    this.fnCallBackConfirm();
  }
}
